connection: "lp_app_qa"

include: "/views/*.view.lkml"                # include all views in the views/ folder in this project
# include: "/**/*.view.lkml"                 # include all views in this project
# include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard

# Select the views that should be a part of this model,
# and define the joins that connect them together.

explore: companies {
  join: accounts{
  relationship: one_to_one
  sql_on: ${companies.id} = ${accounts.company_id} ;;
  type:  inner
  }

  join: jobs{
    relationship: one_to_one
    sql_on: ${accounts.id} = ${jobs.account_id} ;;
    type:  inner
  }

  join: candidate_records{
    relationship: many_to_one
    sql_on: ${jobs.id} = ${candidate_records.job_id} ;;
    type:  inner
  }

  join: conductors {
    relationship: one_to_one
    sql_on: ${candidate_records.id} = ${conductors.candidate_record_id} ;;
    type: inner
  }

  join: conductor_instruments {
    relationship: one_to_one
    sql_on: ${conductors.id} = ${conductor_instruments.conductor_id} ;;
    type:  inner
  }

  join: instruments{
    relationship:  one_to_one
    sql_on: ${instruments.id} = ${conductor_instruments.instrument_id} ;;
    type: inner
  }

}
