connection: "lp_app_qa"

# include all the views
include: "/views/**/*.view"

datagroup: lp_app_qa_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: lp_app_qa_default_datagroup

explore: acc_can_trackers {
  join: account_candidates {
    type: left_outer
    sql_on: ${acc_can_trackers.account_candidate_id} = ${account_candidates.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${acc_can_trackers.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${account_candidates.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: account_available_stages {
  join: accounts {
    type: left_outer
    sql_on: ${account_available_stages.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: account_candidate_feedbacks {
  join: accounts {
    type: left_outer
    sql_on: ${account_candidate_feedbacks.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: account_candidates {
  join: accounts {
    type: left_outer
    sql_on: ${account_candidates.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: account_colors {
  join: accounts {
    type: left_outer
    sql_on: ${account_colors.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: account_cronofy_settings {
  join: account_scheduling_providers {
    type: left_outer
    sql_on: ${account_cronofy_settings.account_scheduling_provider_id} = ${account_scheduling_providers.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${account_scheduling_providers.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: scheduling_providers {
    type: left_outer
    sql_on: ${account_scheduling_providers.scheduling_provider_id} = ${scheduling_providers.id} ;;
    relationship: many_to_one
  }
}

explore: account_emails {
  join: accounts {
    type: left_outer
    sql_on: ${account_emails.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: account_images {
  join: accounts {
    type: left_outer
    sql_on: ${account_images.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${account_images.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: account_integrations {
  join: accounts {
    type: left_outer
    sql_on: ${account_integrations.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }
}

explore: account_interview_contents {
  join: accounts {
    type: left_outer
    sql_on: ${account_interview_contents.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: account_live_providers {
  join: accounts {
    type: left_outer
    sql_on: ${account_live_providers.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: live_providers {
    type: left_outer
    sql_on: ${account_live_providers.live_provider_id} = ${live_providers.id} ;;
    relationship: many_to_one
  }
}

explore: account_mobiles {
  join: accounts {
    type: left_outer
    sql_on: ${account_mobiles.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: account_notes {
  join: accounts {
    type: left_outer
    sql_on: ${account_notes.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${account_notes.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: account_plans {
  join: accounts {
    type: left_outer
    sql_on: ${account_plans.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: billing_periods {
    type: left_outer
    sql_on: ${account_plans.billing_period_id} = ${billing_periods.id} ;;
    relationship: many_to_one
  }

  join: plans {
    type: left_outer
    sql_on: ${account_plans.plan_id} = ${plans.id} ;;
    relationship: many_to_one
  }
}

explore: account_roles {
  join: roles {
    type: left_outer
    sql_on: ${account_roles.role_id} = ${roles.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${account_roles.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: account_scheduling_providers {
  join: accounts {
    type: left_outer
    sql_on: ${account_scheduling_providers.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: scheduling_providers {
    type: left_outer
    sql_on: ${account_scheduling_providers.scheduling_provider_id} = ${scheduling_providers.id} ;;
    relationship: many_to_one
  }
}

explore: account_settings {
  join: accounts {
    type: left_outer
    sql_on: ${account_settings.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: account_tags {
  join: accounts {
    type: left_outer
    sql_on: ${account_tags.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: account_users {
  join: accounts {
    type: left_outer
    sql_on: ${account_users.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${account_users.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: account_users_invites {
  join: accounts {
    type: left_outer
    sql_on: ${account_users_invites.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: account_uuids {}

explore: account_videos {
  join: accounts {
    type: left_outer
    sql_on: ${account_videos.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: account_whitelabels {
  join: accounts {
    type: left_outer
    sql_on: ${account_whitelabels.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: accounts {}

explore: activity_logs {
  join: accounts {
    type: left_outer
    sql_on: ${activity_logs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${activity_logs.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${activity_logs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: admin_announcements {}

explore: admin_reports {
  join: accounts {
    type: left_outer
    sql_on: ${admin_reports.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${admin_reports.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${admin_reports.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: admin_role_permissions {
  join: admin_roles {
    type: left_outer
    sql_on: ${admin_role_permissions.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: admin_roles {}

explore: admin_settings {}

explore: answer_attempts {}

explore: answer_time_settings {}

explore: answer_types {}

explore: answers {
  join: answer_types {
    type: left_outer
    sql_on: ${answers.answer_type_id} = ${answer_types.id} ;;
    relationship: many_to_one
  }
}

explore: api_logs {
  join: accounts {
    type: left_outer
    sql_on: ${api_logs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${api_logs.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${api_logs.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }
}

explore: api_review_interview_links {
  join: candidate_records {
    type: left_outer
    sql_on: ${api_review_interview_links.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${api_review_interview_links.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${api_review_interview_links.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }
}

explore: applicant_invite_custom_ids {
  join: accounts {
    type: left_outer
    sql_on: ${applicant_invite_custom_ids.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${applicant_invite_custom_ids.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${applicant_invite_custom_ids.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: live_assessment_candidates {
    type: left_outer
    sql_on: ${applicant_invite_custom_ids.live_assessment_candidate_id} = ${live_assessment_candidates.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${applicant_invites.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${applicant_invites.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_applications.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: live_meetings {
    type: left_outer
    sql_on: ${live_assessment_candidates.live_meeting_id} = ${live_meetings.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${live_meetings.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }
}

explore: applicant_invite_reminder_histories {
  join: accounts {
    type: left_outer
    sql_on: ${applicant_invite_reminder_histories.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${applicant_invite_reminder_histories.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${applicant_invite_reminder_histories.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: applicant_invite_system_reminder_histories {
  join: jobs {
    type: left_outer
    sql_on: ${applicant_invite_system_reminder_histories.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: applicant_invites {
  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${applicant_invites.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${applicant_invites.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${account_integrations.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_applications.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }
}

explore: applicant_subscriptions {
  join: jobs {
    type: left_outer
    sql_on: ${applicant_subscriptions.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${applicant_subscriptions.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: application_retakes {
  join: candidate_records {
    type: left_outer
    sql_on: ${application_retakes.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${application_retakes.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${application_retakes.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }
}

explore: application_statuses {
  join: jobs {
    type: left_outer
    sql_on: ${application_statuses.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${application_statuses.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: archived_candidate_fields {
  join: archived_candidates {
    type: left_outer
    sql_on: ${archived_candidate_fields.archived_candidate_id} = ${archived_candidates.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${archived_candidates.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${archived_candidates.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${archived_candidates.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: live_assessment_candidates {
    type: left_outer
    sql_on: ${archived_candidates.live_assessment_candidate_id} = ${live_assessment_candidates.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: live_meetings {
    type: left_outer
    sql_on: ${live_assessment_candidates.live_meeting_id} = ${live_meetings.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${live_meetings.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }
}

explore: archived_candidates {
  join: candidate_records {
    type: left_outer
    sql_on: ${archived_candidates.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${archived_candidates.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${archived_candidates.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: live_assessment_candidates {
    type: left_outer
    sql_on: ${archived_candidates.live_assessment_candidate_id} = ${live_assessment_candidates.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: live_meetings {
    type: left_outer
    sql_on: ${live_assessment_candidates.live_meeting_id} = ${live_meetings.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${live_meetings.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }
}

explore: assessment_invites {
  join: users {
    type: left_outer
    sql_on: ${assessment_invites.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: assessment_platforms {
  join: platforms {
    type: left_outer
    sql_on: ${assessment_platforms.platform_id} = ${platforms.id} ;;
    relationship: many_to_one
  }
}

explore: assessment_session_consents {}

explore: assessment_session_statuses {}

explore: assessment_sessions {}

explore: authentications {
  join: users {
    type: left_outer
    sql_on: ${authentications.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: auto_exit_strategies {}

explore: availabilities {}

explore: azure_ad_credentials {
  join: account_live_providers {
    type: left_outer
    sql_on: ${azure_ad_credentials.account_live_provider_id} = ${account_live_providers.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${account_live_providers.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: live_providers {
    type: left_outer
    sql_on: ${account_live_providers.live_provider_id} = ${live_providers.id} ;;
    relationship: many_to_one
  }
}

explore: billing_periods {}

explore: bracket_feedback_reports {}

explore: candidate_answer_statuses {
  join: candidate_answers {
    type: left_outer
    sql_on: ${candidate_answer_statuses.candidate_answer_id} = ${candidate_answers.id} ;;
    relationship: many_to_one
  }

  join: answer_attempts {
    type: left_outer
    sql_on: ${candidate_answers.answer_attempt_id} = ${answer_attempts.id} ;;
    relationship: many_to_one
  }

  join: answer_types {
    type: left_outer
    sql_on: ${candidate_answers.answer_type_id} = ${answer_types.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_answers {
  join: answer_attempts {
    type: left_outer
    sql_on: ${candidate_answers.answer_attempt_id} = ${answer_attempts.id} ;;
    relationship: many_to_one
  }

  join: answer_types {
    type: left_outer
    sql_on: ${candidate_answers.answer_type_id} = ${answer_types.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_average_scores {
  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_average_scores.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: criteria_reviews {
    type: left_outer
    sql_on: ${candidate_average_scores.criteria_review_id} = ${criteria_reviews.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${candidate_average_scores.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: questions {
    type: left_outer
    sql_on: ${candidate_average_scores.question_id} = ${questions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: ds_questions {
    type: left_outer
    sql_on: ${criteria_reviews.ds_question_id} = ${ds_questions.id} ;;
    relationship: many_to_one
  }

  join: ds_review_groups {
    type: left_outer
    sql_on: ${criteria_reviews.ds_review_group_id} = ${ds_review_groups.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${criteria_reviews.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: screening_questions {
    type: left_outer
    sql_on: ${criteria_reviews.screening_question_id} = ${screening_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_library_questions {
    type: left_outer
    sql_on: ${screening_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_question_images {
    type: left_outer
    sql_on: ${screening_questions.screening_question_image_id} = ${screening_question_images.id} ;;
    relationship: many_to_one
  }

  join: question_groups {
    type: left_outer
    sql_on: ${questions.question_group_id} = ${question_groups.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_dual_choice_answers {
  join: candidate_answers {
    type: left_outer
    sql_on: ${candidate_dual_choice_answers.candidate_answer_id} = ${candidate_answers.id} ;;
    relationship: many_to_one
  }

  join: options {
    type: left_outer
    sql_on: ${candidate_dual_choice_answers.option_id} = ${options.id} ;;
    relationship: many_to_one
  }

  join: answer_attempts {
    type: left_outer
    sql_on: ${candidate_answers.answer_attempt_id} = ${answer_attempts.id} ;;
    relationship: many_to_one
  }

  join: answer_types {
    type: left_outer
    sql_on: ${candidate_answers.answer_type_id} = ${answer_types.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_feedback_scoring_results {
  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_feedback_scoring_results.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${candidate_feedback_scoring_results.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_feedback_settings {
  join: job_candidate_feedbacks {
    type: left_outer
    sql_on: ${candidate_feedback_settings.job_candidate_feedback_id} = ${job_candidate_feedbacks.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${job_candidate_feedbacks.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${interviews.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_message_notes {
  join: candidate_message_threads {
    type: left_outer
    sql_on: ${candidate_message_notes.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_message_threads.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_message_threads.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_message_threads.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_message_threads {
  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_message_threads.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_message_threads.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_message_threads.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_message_users {
  join: candidate_message_threads {
    type: left_outer
    sql_on: ${candidate_message_users.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_message_users.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_message_threads.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_message_threads.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_message_threads.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_messages {
  join: candidate_message_threads {
    type: left_outer
    sql_on: ${candidate_messages.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_message_threads.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_message_threads.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_message_threads.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_multiple_choice_answers {
  join: candidate_answers {
    type: left_outer
    sql_on: ${candidate_multiple_choice_answers.candidate_answer_id} = ${candidate_answers.id} ;;
    relationship: many_to_one
  }

  join: options {
    type: left_outer
    sql_on: ${candidate_multiple_choice_answers.option_id} = ${options.id} ;;
    relationship: many_to_one
  }

  join: answer_attempts {
    type: left_outer
    sql_on: ${candidate_answers.answer_attempt_id} = ${answer_attempts.id} ;;
    relationship: many_to_one
  }

  join: answer_types {
    type: left_outer
    sql_on: ${candidate_answers.answer_type_id} = ${answer_types.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_notifications {}

explore: candidate_question_retakes {
  join: answer_attempts {
    type: left_outer
    sql_on: ${candidate_question_retakes.answer_attempt_id} = ${answer_attempts.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_questions {}

explore: candidate_record_deadlines {
  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_record_deadlines.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_record_infos {
  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_record_infos.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_record_ips {
  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_record_ips.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_record_metadata {
  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_record_metadata.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_record_status_logs {
  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_record_status_logs.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_record_tags {
  join: account_tags {
    type: left_outer
    sql_on: ${candidate_record_tags.account_tag_id} = ${account_tags.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_record_tags.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${account_tags.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_record_timestamps {
  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_record_timestamps.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_record_uuids {}

explore: candidate_records {
  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_review_criteria {
  join: users {
    type: left_outer
    sql_on: ${candidate_review_criteria.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_screening_option_answers {
  join: candidate_screening_questions {
    type: left_outer
    sql_on: ${candidate_screening_option_answers.candidate_screening_question_id} = ${candidate_screening_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_options {
    type: left_outer
    sql_on: ${candidate_screening_option_answers.screening_option_id} = ${screening_options.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_screening_questions.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: screening_library_questions {
    type: left_outer
    sql_on: ${candidate_screening_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_questions {
    type: left_outer
    sql_on: ${candidate_screening_questions.screening_question_id} = ${screening_questions.id} ;;
    relationship: many_to_one
  }

  join: versions {
    type: left_outer
    sql_on: ${candidate_screening_questions.version_id} = ${versions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_question_images {
    type: left_outer
    sql_on: ${screening_questions.screening_question_image_id} = ${screening_question_images.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_screening_options {
  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_screening_options.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: screening_options {
    type: left_outer
    sql_on: ${candidate_screening_options.screening_option_id} = ${screening_options.id} ;;
    relationship: many_to_one
  }

  join: screening_questions {
    type: left_outer
    sql_on: ${candidate_screening_options.screening_question_id} = ${screening_questions.id} ;;
    relationship: many_to_one
  }

  join: versions {
    type: left_outer
    sql_on: ${candidate_screening_options.version_id} = ${versions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_library_questions {
    type: left_outer
    sql_on: ${screening_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_question_images {
    type: left_outer
    sql_on: ${screening_questions.screening_question_image_id} = ${screening_question_images.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_screening_questions {
  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_screening_questions.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: screening_library_questions {
    type: left_outer
    sql_on: ${candidate_screening_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_questions {
    type: left_outer
    sql_on: ${candidate_screening_questions.screening_question_id} = ${screening_questions.id} ;;
    relationship: many_to_one
  }

  join: versions {
    type: left_outer
    sql_on: ${candidate_screening_questions.version_id} = ${versions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_question_images {
    type: left_outer
    sql_on: ${screening_questions.screening_question_image_id} = ${screening_question_images.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_screening_view_statuses {
  join: jobs {
    type: left_outer
    sql_on: ${candidate_screening_view_statuses.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_screening_view_statuses.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_sessions {
  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_sessions.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: screening_sessions {
    type: left_outer
    sql_on: ${candidate_sessions.screening_session_id} = ${screening_sessions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: integration_transactions {
    type: left_outer
    sql_on: ${screening_sessions.integration_transaction_id} = ${integration_transactions.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${integration_transactions.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_stage_statuses {}

explore: candidate_status_reports {
  join: accounts {
    type: left_outer
    sql_on: ${candidate_status_reports.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_status_reports.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_text_answers {
  join: candidate_answers {
    type: left_outer
    sql_on: ${candidate_text_answers.candidate_answer_id} = ${candidate_answers.id} ;;
    relationship: many_to_one
  }

  join: answer_attempts {
    type: left_outer
    sql_on: ${candidate_answers.answer_attempt_id} = ${answer_attempts.id} ;;
    relationship: many_to_one
  }

  join: answer_types {
    type: left_outer
    sql_on: ${candidate_answers.answer_type_id} = ${answer_types.id} ;;
    relationship: many_to_one
  }
}

explore: candidate_video_answers {
  join: candidate_answers {
    type: left_outer
    sql_on: ${candidate_video_answers.candidate_answer_id} = ${candidate_answers.id} ;;
    relationship: many_to_one
  }

  join: answer_attempts {
    type: left_outer
    sql_on: ${candidate_answers.answer_attempt_id} = ${answer_attempts.id} ;;
    relationship: many_to_one
  }

  join: answer_types {
    type: left_outer
    sql_on: ${candidate_answers.answer_type_id} = ${answer_types.id} ;;
    relationship: many_to_one
  }
}

explore: client_role_permissions {
  join: client_roles {
    type: left_outer
    sql_on: ${client_role_permissions.client_role_id} = ${client_roles.id} ;;
    relationship: many_to_one
  }

  join: permissions {
    type: left_outer
    sql_on: ${client_role_permissions.permission_id} = ${permissions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${client_roles.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: roles {
    type: left_outer
    sql_on: ${client_roles.role_id} = ${roles.id} ;;
    relationship: many_to_one
  }
}

explore: client_roles {
  join: accounts {
    type: left_outer
    sql_on: ${client_roles.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: roles {
    type: left_outer
    sql_on: ${client_roles.role_id} = ${roles.id} ;;
    relationship: many_to_one
  }
}

explore: collaborator_invites {
  join: accounts {
    type: left_outer
    sql_on: ${collaborator_invites.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${collaborator_invites.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: companies {
  join: company_sizes {
    type: left_outer
    sql_on: ${companies.company_size_id} = ${company_sizes.id} ;;
    relationship: many_to_one
  }

  join: ds_company_sizes {
    type: left_outer
    sql_on: ${companies.ds_company_size_id} = ${ds_company_sizes.id} ;;
    relationship: many_to_one
  }

  join: ds_company_types {
    type: left_outer
    sql_on: ${companies.ds_company_type_id} = ${ds_company_types.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${companies.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${companies.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: company_questions {}

explore: company_sizes {}

explore: competency_descriptions {}

explore: conductor_instruments {
  join: conductors {
    type: left_outer
    sql_on: ${conductor_instruments.conductor_id} = ${conductors.id} ;;
    relationship: many_to_one
  }

  join: instruments {
    type: left_outer
    sql_on: ${conductor_instruments.instrument_id} = ${instruments.id} ;;
    relationship: many_to_one
  }

  join: modular_assessments {
    type: left_outer
    sql_on: ${conductor_instruments.modular_assessment_id} = ${modular_assessments.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${conductors.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${conductors.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${modular_assessments.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: conductors {
  join: candidate_records {
    type: left_outer
    sql_on: ${conductors.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${conductors.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }
}

explore: criteria_review_answers {
  join: criteria_reviews {
    type: left_outer
    sql_on: ${criteria_review_answers.criteria_review_id} = ${criteria_reviews.id} ;;
    relationship: many_to_one
  }

  join: job_application_reviews {
    type: left_outer
    sql_on: ${criteria_review_answers.job_application_review_id} = ${job_application_reviews.id} ;;
    relationship: many_to_one
  }

  join: live_assessment_reviews {
    type: left_outer
    sql_on: ${criteria_review_answers.live_assessment_review_id} = ${live_assessment_reviews.id} ;;
    relationship: many_to_one
  }

  join: screening_reviews {
    type: left_outer
    sql_on: ${criteria_review_answers.screening_review_id} = ${screening_reviews.id} ;;
    relationship: many_to_one
  }

  join: ds_questions {
    type: left_outer
    sql_on: ${criteria_reviews.ds_question_id} = ${ds_questions.id} ;;
    relationship: many_to_one
  }

  join: ds_review_groups {
    type: left_outer
    sql_on: ${criteria_reviews.ds_review_group_id} = ${ds_review_groups.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${criteria_reviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${criteria_reviews.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: questions {
    type: left_outer
    sql_on: ${criteria_reviews.question_id} = ${questions.id} ;;
    relationship: many_to_one
  }

  join: screening_questions {
    type: left_outer
    sql_on: ${criteria_reviews.screening_question_id} = ${screening_questions.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${ds_review_groups.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: question_groups {
    type: left_outer
    sql_on: ${questions.question_group_id} = ${question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_library_questions {
    type: left_outer
    sql_on: ${screening_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_question_images {
    type: left_outer
    sql_on: ${screening_questions.screening_question_image_id} = ${screening_question_images.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${job_application_reviews.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_application_reviews.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: reviewers {
    type: left_outer
    sql_on: ${job_application_reviews.reviewer_id} = ${reviewers.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: live_assessment_reviewers {
    type: left_outer
    sql_on: ${live_assessment_reviews.live_assessment_reviewer_id} = ${live_assessment_reviewers.id} ;;
    relationship: many_to_one
  }

  join: live_meetings {
    type: left_outer
    sql_on: ${live_assessment_reviews.live_meeting_id} = ${live_meetings.id} ;;
    relationship: many_to_one
  }
}

explore: criteria_review_uuids {}

explore: criteria_reviews {
  join: ds_questions {
    type: left_outer
    sql_on: ${criteria_reviews.ds_question_id} = ${ds_questions.id} ;;
    relationship: many_to_one
  }

  join: ds_review_groups {
    type: left_outer
    sql_on: ${criteria_reviews.ds_review_group_id} = ${ds_review_groups.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${criteria_reviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${criteria_reviews.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: questions {
    type: left_outer
    sql_on: ${criteria_reviews.question_id} = ${questions.id} ;;
    relationship: many_to_one
  }

  join: screening_questions {
    type: left_outer
    sql_on: ${criteria_reviews.screening_question_id} = ${screening_questions.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${ds_review_groups.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: question_groups {
    type: left_outer
    sql_on: ${questions.question_group_id} = ${question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_library_questions {
    type: left_outer
    sql_on: ${screening_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_question_images {
    type: left_outer
    sql_on: ${screening_questions.screening_question_image_id} = ${screening_question_images.id} ;;
    relationship: many_to_one
  }
}

explore: criterion_answer_formats {}

explore: cronofy_credentials {
  join: account_scheduling_providers {
    type: left_outer
    sql_on: ${cronofy_credentials.account_scheduling_provider_id} = ${account_scheduling_providers.id} ;;
    relationship: many_to_one
  }

  join: live_assessment_reviewers {
    type: left_outer
    sql_on: ${cronofy_credentials.live_assessment_reviewer_id} = ${live_assessment_reviewers.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${account_scheduling_providers.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: scheduling_providers {
    type: left_outer
    sql_on: ${account_scheduling_providers.scheduling_provider_id} = ${scheduling_providers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${live_assessment_reviewers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: custom_candidate_ids {
  join: accounts {
    type: left_outer
    sql_on: ${custom_candidate_ids.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: data_fields {
  join: conductor_instruments {
    type: left_outer
    sql_on: ${data_fields.conductor_instrument_id} = ${conductor_instruments.id} ;;
    relationship: many_to_one
  }

  join: conductors {
    type: left_outer
    sql_on: ${conductor_instruments.conductor_id} = ${conductors.id} ;;
    relationship: many_to_one
  }

  join: instruments {
    type: left_outer
    sql_on: ${conductor_instruments.instrument_id} = ${instruments.id} ;;
    relationship: many_to_one
  }

  join: modular_assessments {
    type: left_outer
    sql_on: ${conductor_instruments.modular_assessment_id} = ${modular_assessments.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${conductors.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${conductors.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${modular_assessments.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: data_policy_logs {
  join: accounts {
    type: left_outer
    sql_on: ${data_policy_logs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: archived_candidates {
    type: left_outer
    sql_on: ${data_policy_logs.archived_candidate_id} = ${archived_candidates.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${data_policy_logs.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${data_policy_logs.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: live_assessment_candidates {
    type: left_outer
    sql_on: ${data_policy_logs.live_assessment_candidate_id} = ${live_assessment_candidates.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${data_policy_logs.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${archived_candidates.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_applications.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: live_meetings {
    type: left_outer
    sql_on: ${live_assessment_candidates.live_meeting_id} = ${live_meetings.id} ;;
    relationship: many_to_one
  }
}

explore: degrees {}

explore: delayed_jobs {}

explore: did_not_complete_trackers {
  join: candidate_records {
    type: left_outer
    sql_on: ${did_not_complete_trackers.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: documents {
  join: candidate_records {
    type: left_outer
    sql_on: ${documents.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${documents.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${documents.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: ds_benchmark_scores {
  join: candidate_records {
    type: left_outer
    sql_on: ${ds_benchmark_scores.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: criteria_reviews {
    type: left_outer
    sql_on: ${ds_benchmark_scores.criteria_review_id} = ${criteria_reviews.id} ;;
    relationship: many_to_one
  }

  join: ds_review_groups {
    type: left_outer
    sql_on: ${ds_benchmark_scores.ds_review_group_id} = ${ds_review_groups.id} ;;
    relationship: many_to_one
  }

  join: ds_reviewers {
    type: left_outer
    sql_on: ${ds_benchmark_scores.ds_reviewer_id} = ${ds_reviewers.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: ds_questions {
    type: left_outer
    sql_on: ${criteria_reviews.ds_question_id} = ${ds_questions.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${criteria_reviews.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: questions {
    type: left_outer
    sql_on: ${criteria_reviews.question_id} = ${questions.id} ;;
    relationship: many_to_one
  }

  join: screening_questions {
    type: left_outer
    sql_on: ${criteria_reviews.screening_question_id} = ${screening_questions.id} ;;
    relationship: many_to_one
  }

  join: question_groups {
    type: left_outer
    sql_on: ${questions.question_group_id} = ${question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_library_questions {
    type: left_outer
    sql_on: ${screening_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_question_images {
    type: left_outer
    sql_on: ${screening_questions.screening_question_image_id} = ${screening_question_images.id} ;;
    relationship: many_to_one
  }

  join: ds_reviewer_positions {
    type: left_outer
    sql_on: ${ds_reviewers.ds_reviewer_position_id} = ${ds_reviewer_positions.id} ;;
    relationship: many_to_one
  }
}

explore: ds_candidate_question_scores {
  join: candidate_records {
    type: left_outer
    sql_on: ${ds_candidate_question_scores.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: questions {
    type: left_outer
    sql_on: ${ds_candidate_question_scores.question_id} = ${questions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: question_groups {
    type: left_outer
    sql_on: ${questions.question_group_id} = ${question_groups.id} ;;
    relationship: many_to_one
  }
}

explore: ds_candidate_scores {
  join: candidate_records {
    type: left_outer
    sql_on: ${ds_candidate_scores.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: ds_cities {}

explore: ds_company_sizes {}

explore: ds_company_types {}

explore: ds_countries {}

explore: ds_currencies {}

explore: ds_ethnicities {}

explore: ds_industries {}

explore: ds_job_infos {
  join: ds_position_types {
    type: left_outer
    sql_on: ${ds_job_infos.ds_position_type_id} = ${ds_position_types.id} ;;
    relationship: many_to_one
  }

  join: ds_reviewers {
    type: left_outer
    sql_on: ${ds_job_infos.ds_reviewer_id} = ${ds_reviewers.id} ;;
    relationship: many_to_one
  }

  join: ds_team_sizes {
    type: left_outer
    sql_on: ${ds_job_infos.ds_team_size_id} = ${ds_team_sizes.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${ds_job_infos.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${ds_reviewers.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: ds_reviewer_positions {
    type: left_outer
    sql_on: ${ds_reviewers.ds_reviewer_position_id} = ${ds_reviewer_positions.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${ds_reviewers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }
}

explore: ds_nationalities {}

explore: ds_position_types {}

explore: ds_prioritise_candidates {
  join: candidate_records {
    type: left_outer
    sql_on: ${ds_prioritise_candidates.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: ds_review_groups {
    type: left_outer
    sql_on: ${ds_prioritise_candidates.ds_review_group_id} = ${ds_review_groups.id} ;;
    relationship: many_to_one
  }

  join: ds_reviewers {
    type: left_outer
    sql_on: ${ds_prioritise_candidates.ds_reviewer_id} = ${ds_reviewers.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: ds_reviewer_positions {
    type: left_outer
    sql_on: ${ds_reviewers.ds_reviewer_position_id} = ${ds_reviewer_positions.id} ;;
    relationship: many_to_one
  }
}

explore: ds_question_answers {
  join: ds_job_infos {
    type: left_outer
    sql_on: ${ds_question_answers.ds_job_info_id} = ${ds_job_infos.id} ;;
    relationship: many_to_one
  }

  join: ds_questions {
    type: left_outer
    sql_on: ${ds_question_answers.ds_question_id} = ${ds_questions.id} ;;
    relationship: many_to_one
  }

  join: ds_question_options {
    type: left_outer
    sql_on: ${ds_question_answers.ds_question_option_id} = ${ds_question_options.id} ;;
    relationship: many_to_one
  }

  join: ds_reviewers {
    type: left_outer
    sql_on: ${ds_question_answers.ds_reviewer_id} = ${ds_reviewers.id} ;;
    relationship: many_to_one
  }

  join: ds_position_types {
    type: left_outer
    sql_on: ${ds_job_infos.ds_position_type_id} = ${ds_position_types.id} ;;
    relationship: many_to_one
  }

  join: ds_team_sizes {
    type: left_outer
    sql_on: ${ds_job_infos.ds_team_size_id} = ${ds_team_sizes.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${ds_job_infos.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: ds_reviewer_positions {
    type: left_outer
    sql_on: ${ds_reviewers.ds_reviewer_position_id} = ${ds_reviewer_positions.id} ;;
    relationship: many_to_one
  }
}

explore: ds_question_options {
  join: ds_questions {
    type: left_outer
    sql_on: ${ds_question_options.ds_question_id} = ${ds_questions.id} ;;
    relationship: many_to_one
  }
}

explore: ds_questions {}

explore: ds_rereview_candidates {
  join: candidate_records {
    type: left_outer
    sql_on: ${ds_rereview_candidates.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: ds_review_groups {
    type: left_outer
    sql_on: ${ds_rereview_candidates.ds_review_group_id} = ${ds_review_groups.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: ds_review_applicants {
  join: ds_review_groups {
    type: left_outer
    sql_on: ${ds_review_applicants.ds_review_group_id} = ${ds_review_groups.id} ;;
    relationship: many_to_one
  }

  join: job_application_question_versions {
    type: left_outer
    sql_on: ${ds_review_applicants.job_application_question_version_id} = ${job_application_question_versions.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${ds_review_applicants.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${ds_review_groups.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_application_question_versions.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: questions {
    type: left_outer
    sql_on: ${job_application_question_versions.question_id} = ${questions.id} ;;
    relationship: many_to_one
  }

  join: versions {
    type: left_outer
    sql_on: ${job_application_question_versions.version_id} = ${versions.id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: question_groups {
    type: left_outer
    sql_on: ${questions.question_group_id} = ${question_groups.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: ds_review_group_memberships {
  join: ds_review_groups {
    type: left_outer
    sql_on: ${ds_review_group_memberships.ds_review_group_id} = ${ds_review_groups.id} ;;
    relationship: many_to_one
  }

  join: ds_reviewers {
    type: left_outer
    sql_on: ${ds_review_group_memberships.ds_reviewer_id} = ${ds_reviewers.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${ds_review_groups.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: ds_reviewer_positions {
    type: left_outer
    sql_on: ${ds_reviewers.ds_reviewer_position_id} = ${ds_reviewer_positions.id} ;;
    relationship: many_to_one
  }
}

explore: ds_review_group_uuids {}

explore: ds_review_groups {
  join: jobs {
    type: left_outer
    sql_on: ${ds_review_groups.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: ds_reviewer_positions {}

explore: ds_reviewer_uuids {}

explore: ds_reviewers {
  join: accounts {
    type: left_outer
    sql_on: ${ds_reviewers.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: ds_reviewer_positions {
    type: left_outer
    sql_on: ${ds_reviewers.ds_reviewer_position_id} = ${ds_reviewer_positions.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${ds_reviewers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: ds_seniorities {}

explore: ds_team_sizes {}

explore: ea_message_types {}

explore: educations {
  join: candidate_records {
    type: left_outer
    sql_on: ${educations.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${educations.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: grades {
    type: left_outer
    sql_on: ${educations.grade_id} = ${grades.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${educations.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: university_courses {
    type: left_outer
    sql_on: ${educations.university_course_id} = ${university_courses.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: email_addresses {}

explore: email_records {}

explore: email_templates {}

explore: email_types {}

explore: employer_video_questions {}

explore: external_assessments {
  join: modular_assessments {
    type: left_outer
    sql_on: ${external_assessments.modular_assessment_id} = ${modular_assessments.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${modular_assessments.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: feedback_categories {}

explore: feedback_category_frameworks {
  join: candidate_feedback_settings {
    type: left_outer
    sql_on: ${feedback_category_frameworks.candidate_feedback_setting_id} = ${candidate_feedback_settings.id} ;;
    relationship: many_to_one
  }

  join: job_candidate_feedbacks {
    type: left_outer
    sql_on: ${candidate_feedback_settings.job_candidate_feedback_id} = ${job_candidate_feedbacks.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${job_candidate_feedbacks.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${interviews.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: feedback_competencies {
  join: candidate_feedback_settings {
    type: left_outer
    sql_on: ${feedback_competencies.candidate_feedback_setting_id} = ${candidate_feedback_settings.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${feedback_competencies.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: job_candidate_feedbacks {
    type: left_outer
    sql_on: ${candidate_feedback_settings.job_candidate_feedback_id} = ${job_candidate_feedbacks.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${interviews.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: glacier_archives {
  join: job_applications {
    type: left_outer
    sql_on: ${glacier_archives.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${glacier_archives.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: video_encodings {
    type: left_outer
    sql_on: ${glacier_archives.video_encoding_id} = ${video_encodings.id} ;;
    relationship: many_to_one
  }

  join: videos {
    type: left_outer
    sql_on: ${glacier_archives.video_id} = ${videos.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_applications.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: job_application_question_versions {
    type: left_outer
    sql_on: ${videos.job_application_question_version_id} = ${job_application_question_versions.id} ;;
    relationship: many_to_one
  }

  join: questions {
    type: left_outer
    sql_on: ${videos.question_id} = ${questions.id} ;;
    relationship: many_to_one
  }

  join: versions {
    type: left_outer
    sql_on: ${job_application_question_versions.version_id} = ${versions.id} ;;
    relationship: many_to_one
  }

  join: question_groups {
    type: left_outer
    sql_on: ${questions.question_group_id} = ${question_groups.id} ;;
    relationship: many_to_one
  }
}

explore: glacier_restores {
  join: job_applications {
    type: left_outer
    sql_on: ${glacier_restores.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${glacier_restores.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_applications.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }
}

explore: grades {}

explore: hiring_team_members {
  join: hiring_teams {
    type: left_outer
    sql_on: ${hiring_team_members.hiring_team_id} = ${hiring_teams.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${hiring_team_members.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${hiring_teams.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: hiring_teams {
  join: accounts {
    type: left_outer
    sql_on: ${hiring_teams.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: industries {}

explore: industry_roles {}

explore: instrument_events {
  join: conductor_instruments {
    type: left_outer
    sql_on: ${instrument_events.conductor_instrument_id} = ${conductor_instruments.id} ;;
    relationship: many_to_one
  }

  join: conductors {
    type: left_outer
    sql_on: ${conductor_instruments.conductor_id} = ${conductors.id} ;;
    relationship: many_to_one
  }

  join: instruments {
    type: left_outer
    sql_on: ${conductor_instruments.instrument_id} = ${instruments.id} ;;
    relationship: many_to_one
  }

  join: modular_assessments {
    type: left_outer
    sql_on: ${conductor_instruments.modular_assessment_id} = ${modular_assessments.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${conductors.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${conductors.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${modular_assessments.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: instruments {}

explore: integration_interviews {
  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${account_integrations.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${interviews.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: integration_reviews {
  join: job_applications {
    type: left_outer
    sql_on: ${integration_reviews.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${integration_reviews.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_applications.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }
}

explore: integration_transaction_fields {
  join: integration_transactions {
    type: left_outer
    sql_on: ${integration_transaction_fields.integration_transaction_id} = ${integration_transactions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${integration_transactions.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_transactions.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${integration_transactions.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${integration_transactions.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: integration_transaction_tokens {
  join: integration_transactions {
    type: left_outer
    sql_on: ${integration_transaction_tokens.integration_transaction_id} = ${integration_transactions.id} ;;
    relationship: many_to_one
  }

  join: interview_setup_requests {
    type: left_outer
    sql_on: ${integration_transaction_tokens.interview_setup_request_id} = ${interview_setup_requests.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${integration_transactions.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_transactions.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${integration_transactions.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${integration_transactions.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: integration_transactions {
  join: accounts {
    type: left_outer
    sql_on: ${integration_transactions.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_transactions.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${integration_transactions.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${integration_transactions.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: integrations {}

explore: interview_requirements {
  join: jobs {
    type: left_outer
    sql_on: ${interview_requirements.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: interview_setup_requests {
  join: accounts {
    type: left_outer
    sql_on: ${interview_setup_requests.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${interview_setup_requests.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: interview_share_invite_logs {
  join: interview_share_invites {
    type: left_outer
    sql_on: ${interview_share_invite_logs.interview_share_invite_id} = ${interview_share_invites.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${interview_share_invites.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${interview_share_invites.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${interview_share_invites.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }
}

explore: interview_share_invites {
  join: accounts {
    type: left_outer
    sql_on: ${interview_share_invites.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${interview_share_invites.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${interview_share_invites.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }
}

explore: interviews {
  join: jobs {
    type: left_outer
    sql_on: ${interviews.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_application_comments {
  join: job_applications {
    type: left_outer
    sql_on: ${job_application_comments.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_application_comments.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${job_applications.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_application_criteria_scores {
  join: criteria_reviews {
    type: left_outer
    sql_on: ${job_application_criteria_scores.criteria_review_id} = ${criteria_reviews.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${job_application_criteria_scores.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: ds_questions {
    type: left_outer
    sql_on: ${criteria_reviews.ds_question_id} = ${ds_questions.id} ;;
    relationship: many_to_one
  }

  join: ds_review_groups {
    type: left_outer
    sql_on: ${criteria_reviews.ds_review_group_id} = ${ds_review_groups.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${criteria_reviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${criteria_reviews.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: questions {
    type: left_outer
    sql_on: ${criteria_reviews.question_id} = ${questions.id} ;;
    relationship: many_to_one
  }

  join: screening_questions {
    type: left_outer
    sql_on: ${criteria_reviews.screening_question_id} = ${screening_questions.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${ds_review_groups.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: question_groups {
    type: left_outer
    sql_on: ${questions.question_group_id} = ${question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_library_questions {
    type: left_outer
    sql_on: ${screening_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_question_images {
    type: left_outer
    sql_on: ${screening_questions.screening_question_image_id} = ${screening_question_images.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }
}

explore: job_application_histories {
  join: job_applications {
    type: left_outer
    sql_on: ${job_application_histories.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${job_applications.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_applications.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_application_lists {
  join: job_applications {
    type: left_outer
    sql_on: ${job_application_lists.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_application_lists.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${job_applications.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_applications.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_application_question_versions {
  join: candidate_records {
    type: left_outer
    sql_on: ${job_application_question_versions.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${job_application_question_versions.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: questions {
    type: left_outer
    sql_on: ${job_application_question_versions.question_id} = ${questions.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_application_question_versions.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: versions {
    type: left_outer
    sql_on: ${job_application_question_versions.version_id} = ${versions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: question_groups {
    type: left_outer
    sql_on: ${questions.question_group_id} = ${question_groups.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_application_ratings {
  join: job_applications {
    type: left_outer
    sql_on: ${job_application_ratings.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_application_ratings.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${job_applications.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_application_reviews {
  join: ds_review_groups {
    type: left_outer
    sql_on: ${job_application_reviews.ds_review_group_id} = ${ds_review_groups.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${job_application_reviews.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_application_reviews.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: reviewers {
    type: left_outer
    sql_on: ${job_application_reviews.reviewer_id} = ${reviewers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_application_reviews.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${ds_review_groups.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_application_shares {
  join: job_applications {
    type: left_outer
    sql_on: ${job_application_shares.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${job_applications.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_applications.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_application_total_scores {
  join: job_applications {
    type: left_outer
    sql_on: ${job_application_total_scores.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${job_applications.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_applications.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_application_view_statuses {
  join: job_applications {
    type: left_outer
    sql_on: ${job_application_view_statuses.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_application_view_statuses.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${job_applications.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_applications {
  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${job_applications.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_applications.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_availabilities {
  join: jobs {
    type: left_outer
    sql_on: ${job_availabilities.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_batch_cv_uploads {
  join: jobs {
    type: left_outer
    sql_on: ${job_batch_cv_uploads.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_candidate_feedbacks {
  join: interviews {
    type: left_outer
    sql_on: ${job_candidate_feedbacks.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${interviews.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_clicks {
  join: jobs {
    type: left_outer
    sql_on: ${job_clicks.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_clicks.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_collaborators {
  join: accounts {
    type: left_outer
    sql_on: ${job_collaborators.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${job_collaborators.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_collaborators.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_favorites {
  join: jobs {
    type: left_outer
    sql_on: ${job_favorites.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_favorites.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_integration_mapping_notifications {
  join: integration_transactions {
    type: left_outer
    sql_on: ${job_integration_mapping_notifications.integration_transaction_id} = ${integration_transactions.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${job_integration_mapping_notifications.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${integration_transactions.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_transactions.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${integration_transactions.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_integration_mappings {
  join: account_integrations {
    type: left_outer
    sql_on: ${job_integration_mappings.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${job_integration_mappings.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${job_integration_mappings.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${job_integration_mappings.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${account_integrations.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_review_lists {
  join: jobs {
    type: left_outer
    sql_on: ${job_review_lists.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_review_lists.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_roles {
  join: industry_roles {
    type: left_outer
    sql_on: ${job_roles.industry_role_id} = ${industry_roles.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${job_roles.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_seeker_favorite_jobs {
  join: jobs {
    type: left_outer
    sql_on: ${job_seeker_favorite_jobs.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_seeker_favorite_jobs.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_seeker_industries {
  join: job_seekers {
    type: left_outer
    sql_on: ${job_seeker_industries.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_seekers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_seeker_roles {
  join: industry_roles {
    type: left_outer
    sql_on: ${job_seeker_roles.industry_role_id} = ${industry_roles.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_seeker_roles.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_seekers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_seeker_schedule_types {
  join: job_seekers {
    type: left_outer
    sql_on: ${job_seeker_schedule_types.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${job_seeker_schedule_types.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_seekers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_seeker_video_recordings {
  join: job_seeker_video_sessions {
    type: left_outer
    sql_on: ${job_seeker_video_recordings.job_seeker_video_session_id} = ${job_seeker_video_sessions.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_seeker_video_sessions.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_seeker_video_sessions {
  join: users {
    type: left_outer
    sql_on: ${job_seeker_video_sessions.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_seeker_videos {
  join: users {
    type: left_outer
    sql_on: ${job_seeker_videos.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_seekers {
  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_seekers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_settings_stores {
  join: jobs {
    type: left_outer
    sql_on: ${job_settings_stores.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_team_collaborators {
  join: hiring_teams {
    type: left_outer
    sql_on: ${job_team_collaborators.hiring_team_id} = ${hiring_teams.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${job_team_collaborators.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${hiring_teams.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: job_uuids {}

explore: jobs {
  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_assessment_candidates {
  join: live_meetings {
    type: left_outer
    sql_on: ${live_assessment_candidates.live_meeting_id} = ${live_meetings.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${live_meetings.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${live_meetings.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_assessment_locations {
  join: live_assessments {
    type: left_outer
    sql_on: ${live_assessment_locations.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${live_assessments.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${live_assessments.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_assessment_reviewers {
  join: users {
    type: left_outer
    sql_on: ${live_assessment_reviewers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_assessment_reviews {
  join: live_assessment_reviewers {
    type: left_outer
    sql_on: ${live_assessment_reviews.live_assessment_reviewer_id} = ${live_assessment_reviewers.id} ;;
    relationship: many_to_one
  }

  join: live_meetings {
    type: left_outer
    sql_on: ${live_assessment_reviews.live_meeting_id} = ${live_meetings.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${live_assessment_reviewers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${live_meetings.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${live_meetings.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }
}

explore: live_assessments {
  join: accounts {
    type: left_outer
    sql_on: ${live_assessments.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${live_assessments.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_assigned_reviewers {
  join: live_assessments {
    type: left_outer
    sql_on: ${live_assigned_reviewers.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: live_assessment_reviewers {
    type: left_outer
    sql_on: ${live_assigned_reviewers.live_assessment_reviewer_id} = ${live_assessment_reviewers.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${live_assessments.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${live_assessments.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_available_schedules {
  join: live_interviews {
    type: left_outer
    sql_on: ${live_available_schedules.live_interview_id} = ${live_interviews.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${live_interviews.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: live_candidate_recordings {
  join: live_candidates {
    type: left_outer
    sql_on: ${live_candidate_recordings.live_candidate_id} = ${live_candidates.id} ;;
    relationship: many_to_one
  }

  join: live_interviews {
    type: left_outer
    sql_on: ${live_candidate_recordings.live_interview_id} = ${live_interviews.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${live_interviews.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: live_candidates {
  join: live_interviews {
    type: left_outer
    sql_on: ${live_candidates.live_interview_id} = ${live_interviews.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${live_interviews.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: live_colleagues {
  join: accounts {
    type: left_outer
    sql_on: ${live_colleagues.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: live_interviews {
    type: left_outer
    sql_on: ${live_colleagues.live_interview_id} = ${live_interviews.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${live_colleagues.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_criteria_review_answers {
  join: live_criteria_reviews {
    type: left_outer
    sql_on: ${live_criteria_review_answers.live_criteria_review_id} = ${live_criteria_reviews.id} ;;
    relationship: many_to_one
  }

  join: live_interview_sessions {
    type: left_outer
    sql_on: ${live_criteria_review_answers.live_interview_session_id} = ${live_interview_sessions.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${live_criteria_review_answers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: live_interviews {
    type: left_outer
    sql_on: ${live_criteria_reviews.live_interview_id} = ${live_interviews.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${live_interviews.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: candidate_sessions {
    type: left_outer
    sql_on: ${live_interview_sessions.candidate_session_id} = ${candidate_sessions.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_sessions.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: screening_sessions {
    type: left_outer
    sql_on: ${candidate_sessions.screening_session_id} = ${screening_sessions.id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: integration_transactions {
    type: left_outer
    sql_on: ${screening_sessions.integration_transaction_id} = ${integration_transactions.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${integration_transactions.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_criteria_reviews {
  join: live_interviews {
    type: left_outer
    sql_on: ${live_criteria_reviews.live_interview_id} = ${live_interviews.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${live_interviews.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: live_employers {
  join: live_interviews {
    type: left_outer
    sql_on: ${live_employers.live_interview_id} = ${live_interviews.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${live_employers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${live_interviews.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_interview_logs {
  join: live_candidates {
    type: left_outer
    sql_on: ${live_interview_logs.live_candidate_id} = ${live_candidates.id} ;;
    relationship: many_to_one
  }

  join: live_employers {
    type: left_outer
    sql_on: ${live_interview_logs.live_employer_id} = ${live_employers.id} ;;
    relationship: many_to_one
  }

  join: live_interviews {
    type: left_outer
    sql_on: ${live_interview_logs.live_interview_id} = ${live_interviews.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${live_employers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${live_interviews.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: live_interview_messages {}

explore: live_interview_sessions {
  join: candidate_sessions {
    type: left_outer
    sql_on: ${live_interview_sessions.candidate_session_id} = ${candidate_sessions.id} ;;
    relationship: many_to_one
  }

  join: live_interviews {
    type: left_outer
    sql_on: ${live_interview_sessions.live_interview_id} = ${live_interviews.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_sessions.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: screening_sessions {
    type: left_outer
    sql_on: ${candidate_sessions.screening_session_id} = ${screening_sessions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: integration_transactions {
    type: left_outer
    sql_on: ${screening_sessions.integration_transaction_id} = ${integration_transactions.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${integration_transactions.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: live_interview_slots {
  join: live_available_schedules {
    type: left_outer
    sql_on: ${live_interview_slots.live_available_schedule_id} = ${live_available_schedules.id} ;;
    relationship: many_to_one
  }

  join: live_candidates {
    type: left_outer
    sql_on: ${live_interview_slots.live_candidate_id} = ${live_candidates.id} ;;
    relationship: many_to_one
  }

  join: live_interviews {
    type: left_outer
    sql_on: ${live_interview_slots.live_interview_id} = ${live_interviews.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${live_interviews.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: live_interviews {
  join: accounts {
    type: left_outer
    sql_on: ${live_interviews.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: live_invites_csv_uploads {
  join: accounts {
    type: left_outer
    sql_on: ${live_invites_csv_uploads.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${live_invites_csv_uploads.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${live_assessments.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_meeting_deadlines {
  join: live_meetings {
    type: left_outer
    sql_on: ${live_meeting_deadlines.live_meeting_id} = ${live_meetings.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${live_meetings.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${live_meetings.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_meeting_providers {
  join: live_meetings {
    type: left_outer
    sql_on: ${live_meeting_providers.live_meeting_id} = ${live_meetings.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${live_meetings.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${live_meetings.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_meetings {
  join: candidate_records {
    type: left_outer
    sql_on: ${live_meetings.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${live_meetings.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_pool_ids {
  join: users {
    type: left_outer
    sql_on: ${live_pool_ids.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_providers {}

explore: live_review_comments {
  join: live_interview_sessions {
    type: left_outer
    sql_on: ${live_review_comments.live_interview_session_id} = ${live_interview_sessions.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${live_review_comments.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: candidate_sessions {
    type: left_outer
    sql_on: ${live_interview_sessions.candidate_session_id} = ${candidate_sessions.id} ;;
    relationship: many_to_one
  }

  join: live_interviews {
    type: left_outer
    sql_on: ${live_interview_sessions.live_interview_id} = ${live_interviews.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${candidate_sessions.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: screening_sessions {
    type: left_outer
    sql_on: ${candidate_sessions.screening_session_id} = ${screening_sessions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: integration_transactions {
    type: left_outer
    sql_on: ${screening_sessions.integration_transaction_id} = ${integration_transactions.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${integration_transactions.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_schedule_records {
  join: live_schedules {
    type: left_outer
    sql_on: ${live_schedule_records.live_schedule_id} = ${live_schedules.id} ;;
    relationship: many_to_one
  }

  join: live_meetings {
    type: left_outer
    sql_on: ${live_schedules.live_meeting_id} = ${live_meetings.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${live_meetings.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${live_meetings.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_scheduled_answers {
  join: live_candidates {
    type: left_outer
    sql_on: ${live_scheduled_answers.live_candidate_id} = ${live_candidates.id} ;;
    relationship: many_to_one
  }

  join: live_criteria_reviews {
    type: left_outer
    sql_on: ${live_scheduled_answers.live_criteria_review_id} = ${live_criteria_reviews.id} ;;
    relationship: many_to_one
  }

  join: live_employers {
    type: left_outer
    sql_on: ${live_scheduled_answers.live_employer_id} = ${live_employers.id} ;;
    relationship: many_to_one
  }

  join: live_scheduled_sessions {
    type: left_outer
    sql_on: ${live_scheduled_answers.live_scheduled_session_id} = ${live_scheduled_sessions.id} ;;
    relationship: many_to_one
  }

  join: live_interviews {
    type: left_outer
    sql_on: ${live_candidates.live_interview_id} = ${live_interviews.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${live_interviews.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${live_employers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_scheduled_comments {
  join: live_candidates {
    type: left_outer
    sql_on: ${live_scheduled_comments.live_candidate_id} = ${live_candidates.id} ;;
    relationship: many_to_one
  }

  join: live_employers {
    type: left_outer
    sql_on: ${live_scheduled_comments.live_employer_id} = ${live_employers.id} ;;
    relationship: many_to_one
  }

  join: live_scheduled_sessions {
    type: left_outer
    sql_on: ${live_scheduled_comments.live_scheduled_session_id} = ${live_scheduled_sessions.id} ;;
    relationship: many_to_one
  }

  join: live_interviews {
    type: left_outer
    sql_on: ${live_candidates.live_interview_id} = ${live_interviews.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${live_interviews.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${live_employers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_scheduled_sessions {
  join: live_interviews {
    type: left_outer
    sql_on: ${live_scheduled_sessions.live_interview_id} = ${live_interviews.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${live_interviews.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: live_schedules {
  join: live_meetings {
    type: left_outer
    sql_on: ${live_schedules.live_meeting_id} = ${live_meetings.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${live_meetings.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: live_assessments {
    type: left_outer
    sql_on: ${live_meetings.live_assessment_id} = ${live_assessments.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: live_user_providers {
  join: live_assessment_reviewers {
    type: left_outer
    sql_on: ${live_user_providers.live_assessment_reviewer_id} = ${live_assessment_reviewers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${live_assessment_reviewers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: locations {}

explore: lumesse_notifications {}

explore: mail_loggers {}

explore: mailer_locks {}

explore: modular_assessment_results {
  join: conductor_instruments {
    type: left_outer
    sql_on: ${modular_assessment_results.conductor_instrument_id} = ${conductor_instruments.id} ;;
    relationship: many_to_one
  }

  join: conductors {
    type: left_outer
    sql_on: ${conductor_instruments.conductor_id} = ${conductors.id} ;;
    relationship: many_to_one
  }

  join: instruments {
    type: left_outer
    sql_on: ${conductor_instruments.instrument_id} = ${instruments.id} ;;
    relationship: many_to_one
  }

  join: modular_assessments {
    type: left_outer
    sql_on: ${conductor_instruments.modular_assessment_id} = ${modular_assessments.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${conductors.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${conductors.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${modular_assessments.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: modular_assessments {
  join: packages {
    type: left_outer
    sql_on: ${modular_assessments.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: modular_encodings {}

explore: modular_invites {}

explore: modular_jobs {}

explore: modular_screening_questions {
  join: screening_assessments {
    type: left_outer
    sql_on: ${modular_screening_questions.screening_assessment_id} = ${screening_assessments.id} ;;
    relationship: many_to_one
  }

  join: screening_questions {
    type: left_outer
    sql_on: ${modular_screening_questions.screening_question_id} = ${screening_questions.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${screening_assessments.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: modular_assessments {
    type: left_outer
    sql_on: ${screening_assessments.modular_assessment_id} = ${modular_assessments.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${modular_assessments.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_library_questions {
    type: left_outer
    sql_on: ${screening_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_question_images {
    type: left_outer
    sql_on: ${screening_questions.screening_question_image_id} = ${screening_question_images.id} ;;
    relationship: many_to_one
  }
}

explore: modular_videos {}

explore: mother_packers {
  join: jobs {
    type: left_outer
    sql_on: ${mother_packers.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: mother_packing_codes {}

explore: mother_packing_packages {
  join: mother_packers {
    type: left_outer
    sql_on: ${mother_packing_packages.mother_packer_id} = ${mother_packers.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${mother_packing_packages.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${mother_packers.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: mp_display_settings {}

explore: options {}

explore: package_emails {
  join: email_types {
    type: left_outer
    sql_on: ${package_emails.email_type_id} = ${email_types.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${package_emails.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: packages {}

explore: pandastream_videos {}

explore: part_time_availabilities {
  join: job_seekers {
    type: left_outer
    sql_on: ${part_time_availabilities.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_seekers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: part_time_schedules {}

explore: partner_assessments {
  join: partners {
    type: left_outer
    sql_on: ${partner_assessments.partner_id} = ${partners.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${partners.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: partners {
  join: accounts {
    type: left_outer
    sql_on: ${partners.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: pay_types {}

explore: people {}

explore: permissions {}

explore: person_email_addresses {}

explore: plans {
  join: billing_periods {
    type: left_outer
    sql_on: ${plans.billing_period_id} = ${billing_periods.id} ;;
    relationship: many_to_one
  }
}

explore: platforms {}

explore: predict_settings {
  join: jobs {
    type: left_outer
    sql_on: ${predict_settings.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: question_candidate_review_criteria {
  join: answer_attempts {
    type: left_outer
    sql_on: ${question_candidate_review_criteria.answer_attempt_id} = ${answer_attempts.id} ;;
    relationship: many_to_one
  }

  join: candidate_review_criteria {
    type: left_outer
    sql_on: ${question_candidate_review_criteria.candidate_review_criteria_id} = ${candidate_review_criteria.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_review_criteria.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: question_group_v2s {}

explore: question_groups {
  join: interviews {
    type: left_outer
    sql_on: ${question_groups.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${interviews.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: question_lists {}

explore: question_types {}

explore: question_uuids {}

explore: question_v2s {}

explore: question_version_tags {}

explore: question_video_attachments {}

explore: questions {
  join: interviews {
    type: left_outer
    sql_on: ${questions.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: question_groups {
    type: left_outer
    sql_on: ${questions.question_group_id} = ${question_groups.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${interviews.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: recorder_errors {
  join: jobs {
    type: left_outer
    sql_on: ${recorder_errors.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${recorder_errors.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: registered_partners {}

explore: release_notes {}

explore: retake_deadlines {}

explore: retake_reason_templates {}

explore: review_buckets {
  join: jobs {
    type: left_outer
    sql_on: ${review_buckets.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${review_buckets.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: review_criteria {}

explore: review_list_shares {
  join: review_lists {
    type: left_outer
    sql_on: ${review_list_shares.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: reviewers {
    type: left_outer
    sql_on: ${review_list_shares.reviewer_id} = ${reviewers.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${reviewers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: review_lists {}

explore: reviewers {
  join: users {
    type: left_outer
    sql_on: ${reviewers.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: roles {}

explore: s3_informations {}

explore: schedule_types {}

explore: scheduling_providers {}

explore: schema_migrations {}

explore: screening_assessments {
  join: jobs {
    type: left_outer
    sql_on: ${screening_assessments.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: modular_assessments {
    type: left_outer
    sql_on: ${screening_assessments.modular_assessment_id} = ${modular_assessments.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${modular_assessments.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: screening_competency_measures {
  join: jobs {
    type: left_outer
    sql_on: ${screening_competency_measures.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: screening_feedback_frameworks {
  join: jobs {
    type: left_outer
    sql_on: ${screening_feedback_frameworks.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: screening_invites {
  join: candidate_records {
    type: left_outer
    sql_on: ${screening_invites.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: screening_library_categories {
  join: accounts {
    type: left_outer
    sql_on: ${screening_library_categories.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: screening_library_options {
  join: screening_library_questions {
    type: left_outer
    sql_on: ${screening_library_options.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${screening_library_questions.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: screening_library_questions {
  join: accounts {
    type: left_outer
    sql_on: ${screening_library_questions.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: screening_multiple_scores {
  join: jobs {
    type: left_outer
    sql_on: ${screening_multiple_scores.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: screening_options {
  join: screening_questions {
    type: left_outer
    sql_on: ${screening_options.screening_question_id} = ${screening_questions.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${screening_questions.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_library_questions {
    type: left_outer
    sql_on: ${screening_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_question_images {
    type: left_outer
    sql_on: ${screening_questions.screening_question_image_id} = ${screening_question_images.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: screening_question_groups {
  join: jobs {
    type: left_outer
    sql_on: ${screening_question_groups.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: screening_question_images {
  join: jobs {
    type: left_outer
    sql_on: ${screening_question_images.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_questions {
    type: left_outer
    sql_on: ${screening_question_images.screening_question_id} = ${screening_questions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_library_questions {
    type: left_outer
    sql_on: ${screening_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }
}

explore: screening_questions {
  join: jobs {
    type: left_outer
    sql_on: ${screening_questions.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_library_questions {
    type: left_outer
    sql_on: ${screening_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_question_images {
    type: left_outer
    sql_on: ${screening_questions.screening_question_image_id} = ${screening_question_images.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: screening_retakes {
  join: candidate_records {
    type: left_outer
    sql_on: ${screening_retakes.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: screening_questions {
    type: left_outer
    sql_on: ${screening_retakes.screening_question_ids} = ${screening_questions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_library_questions {
    type: left_outer
    sql_on: ${screening_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_question_images {
    type: left_outer
    sql_on: ${screening_questions.screening_question_image_id} = ${screening_question_images.id} ;;
    relationship: many_to_one
  }
}

explore: screening_reviews {
  join: candidate_records {
    type: left_outer
    sql_on: ${screening_reviews.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${screening_reviews.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: screening_score_ranges {
  join: jobs {
    type: left_outer
    sql_on: ${screening_score_ranges.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: screening_session_options {
  join: screening_options {
    type: left_outer
    sql_on: ${screening_session_options.screening_option_id} = ${screening_options.id} ;;
    relationship: many_to_one
  }

  join: screening_questions {
    type: left_outer
    sql_on: ${screening_session_options.screening_question_id} = ${screening_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_sessions {
    type: left_outer
    sql_on: ${screening_session_options.screening_session_id} = ${screening_sessions.id} ;;
    relationship: many_to_one
  }

  join: versions {
    type: left_outer
    sql_on: ${screening_session_options.version_id} = ${versions.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${screening_questions.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_library_questions {
    type: left_outer
    sql_on: ${screening_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_question_images {
    type: left_outer
    sql_on: ${screening_questions.screening_question_image_id} = ${screening_question_images.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${screening_sessions.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: integration_transactions {
    type: left_outer
    sql_on: ${screening_sessions.integration_transaction_id} = ${integration_transactions.id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${integration_transactions.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: screening_session_questions {
  join: screening_library_questions {
    type: left_outer
    sql_on: ${screening_session_questions.screening_library_question_id} = ${screening_library_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_questions {
    type: left_outer
    sql_on: ${screening_session_questions.screening_question_id} = ${screening_questions.id} ;;
    relationship: many_to_one
  }

  join: screening_sessions {
    type: left_outer
    sql_on: ${screening_session_questions.screening_session_id} = ${screening_sessions.id} ;;
    relationship: many_to_one
  }

  join: versions {
    type: left_outer
    sql_on: ${screening_session_questions.version_id} = ${versions.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${screening_library_questions.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${screening_questions.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_competency_measures {
    type: left_outer
    sql_on: ${screening_questions.screening_competency_measure_id} = ${screening_competency_measures.id} ;;
    relationship: many_to_one
  }

  join: screening_question_groups {
    type: left_outer
    sql_on: ${screening_questions.screening_question_group_id} = ${screening_question_groups.id} ;;
    relationship: many_to_one
  }

  join: screening_question_images {
    type: left_outer
    sql_on: ${screening_questions.screening_question_image_id} = ${screening_question_images.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${screening_sessions.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: integration_transactions {
    type: left_outer
    sql_on: ${screening_sessions.integration_transaction_id} = ${integration_transactions.id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${integration_transactions.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: screening_sessions {
  join: accounts {
    type: left_outer
    sql_on: ${screening_sessions.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${screening_sessions.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: integration_transactions {
    type: left_outer
    sql_on: ${screening_sessions.integration_transaction_id} = ${integration_transactions.id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${integration_transactions.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: security_login_attempts {}

explore: security_password_controls {
  join: accounts {
    type: left_outer
    sql_on: ${security_password_controls.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }
}

explore: security_reuse_passwords {
  join: users {
    type: left_outer
    sql_on: ${security_reuse_passwords.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: settings {
  join: users {
    type: left_outer
    sql_on: ${settings.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: slugs {}

explore: stage_candidate_review_criteria {
  join: candidate_review_criteria {
    type: left_outer
    sql_on: ${stage_candidate_review_criteria.candidate_review_criteria_id} = ${candidate_review_criteria.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_review_criteria.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: stage_categories {}

explore: stage_emails {
  join: email_types {
    type: left_outer
    sql_on: ${stage_emails.email_type_id} = ${email_types.id} ;;
    relationship: many_to_one
  }
}

explore: stage_groups {}

explore: stage_ui_configs {}

explore: stages {}

explore: subset_settings {}

explore: system_settings {}

explore: tcc_candidate_files {
  join: candidate_records {
    type: left_outer
    sql_on: ${tcc_candidate_files.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: trial_user_invites {
  join: accounts {
    type: left_outer
    sql_on: ${trial_user_invites.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${trial_user_invites.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: universities {}

explore: university_courses {}

explore: user_account_changes {
  join: accounts {
    type: left_outer
    sql_on: ${user_account_changes.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${user_account_changes.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: user_admin_announcements {
  join: admin_announcements {
    type: left_outer
    sql_on: ${user_admin_announcements.admin_announcement_id} = ${admin_announcements.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${user_admin_announcements.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: user_view_counters {
  join: jobs {
    type: left_outer
    sql_on: ${user_view_counters.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${user_view_counters.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: users {
  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: versions {}

explore: video_assessment_v2s {}

explore: video_assessments {
  join: jobs {
    type: left_outer
    sql_on: ${video_assessments.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: modular_assessments {
    type: left_outer
    sql_on: ${video_assessments.modular_assessment_id} = ${modular_assessments.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${modular_assessments.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: video_encodings {
  join: videos {
    type: left_outer
    sql_on: ${video_encodings.video_id} = ${videos.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${videos.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${videos.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: job_application_question_versions {
    type: left_outer
    sql_on: ${videos.job_application_question_version_id} = ${job_application_question_versions.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${videos.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: questions {
    type: left_outer
    sql_on: ${videos.question_id} = ${questions.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_applications.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: versions {
    type: left_outer
    sql_on: ${job_application_question_versions.version_id} = ${versions.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: question_groups {
    type: left_outer
    sql_on: ${questions.question_group_id} = ${question_groups.id} ;;
    relationship: many_to_one
  }
}

explore: video_playback_logs {
  join: job_applications {
    type: left_outer
    sql_on: ${video_playback_logs.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${video_playback_logs.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${video_playback_logs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}

explore: video_question_groups {}

explore: video_question_v2s {}

explore: video_questions {
  join: questions {
    type: left_outer
    sql_on: ${video_questions.question_id} = ${questions.id} ;;
    relationship: many_to_one
  }

  join: video_assessments {
    type: left_outer
    sql_on: ${video_questions.video_assessment_id} = ${video_assessments.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${questions.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: question_groups {
    type: left_outer
    sql_on: ${questions.question_group_id} = ${question_groups.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${interviews.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${jobs.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${jobs.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: modular_assessments {
    type: left_outer
    sql_on: ${video_assessments.modular_assessment_id} = ${modular_assessments.id} ;;
    relationship: many_to_one
  }

  join: packages {
    type: left_outer
    sql_on: ${modular_assessments.package_id} = ${packages.id} ;;
    relationship: many_to_one
  }
}

explore: videos {
  join: accounts {
    type: left_outer
    sql_on: ${videos.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${videos.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: job_application_question_versions {
    type: left_outer
    sql_on: ${videos.job_application_question_version_id} = ${job_application_question_versions.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${videos.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: questions {
    type: left_outer
    sql_on: ${videos.question_id} = ${questions.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: candidate_records {
    type: left_outer
    sql_on: ${job_applications.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${job_applications.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${job_applications.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${job_seekers.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${job_seekers.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }

  join: versions {
    type: left_outer
    sql_on: ${job_application_question_versions.version_id} = ${versions.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: question_groups {
    type: left_outer
    sql_on: ${questions.question_group_id} = ${question_groups.id} ;;
    relationship: many_to_one
  }
}

explore: vq_rc_groupings {}

explore: work_experience_durations {}

explore: work_experiences {
  join: candidate_records {
    type: left_outer
    sql_on: ${work_experiences.candidate_record_id} = ${candidate_records.id} ;;
    relationship: many_to_one
  }

  join: industry_roles {
    type: left_outer
    sql_on: ${work_experiences.industry_role_id} = ${industry_roles.id} ;;
    relationship: many_to_one
  }

  join: job_seekers {
    type: left_outer
    sql_on: ${work_experiences.job_seeker_id} = ${job_seekers.id} ;;
    relationship: many_to_one
  }

  join: work_experience_durations {
    type: left_outer
    sql_on: ${work_experiences.work_experience_duration_id} = ${work_experience_durations.id} ;;
    relationship: many_to_one
  }

  join: accounts {
    type: left_outer
    sql_on: ${candidate_records.account_id} = ${accounts.provisioner_account_id} ;;
    relationship: many_to_one
  }

  join: applicant_invites {
    type: left_outer
    sql_on: ${candidate_records.applicant_invite_id} = ${applicant_invites.id} ;;
    relationship: many_to_one
  }

  join: job_applications {
    type: left_outer
    sql_on: ${candidate_records.job_application_id} = ${job_applications.id} ;;
    relationship: many_to_one
  }

  join: jobs {
    type: left_outer
    sql_on: ${candidate_records.job_id} = ${jobs.root_job_id} ;;
    relationship: many_to_one
  }

  join: screening_invites {
    type: left_outer
    sql_on: ${candidate_records.screening_invite_id} = ${screening_invites.id} ;;
    relationship: many_to_one
  }

  join: users {
    type: left_outer
    sql_on: ${candidate_records.user_id} = ${users.id} ;;
    relationship: many_to_one
  }

  join: integration_interviews {
    type: left_outer
    sql_on: ${applicant_invites.integration_interview_id} = ${integration_interviews.id} ;;
    relationship: many_to_one
  }

  join: account_integrations {
    type: left_outer
    sql_on: ${integration_interviews.account_integration_id} = ${account_integrations.id} ;;
    relationship: many_to_one
  }

  join: interviews {
    type: left_outer
    sql_on: ${integration_interviews.interview_id} = ${interviews.id} ;;
    relationship: many_to_one
  }

  join: integrations {
    type: left_outer
    sql_on: ${account_integrations.integration_id} = ${integrations.id} ;;
    relationship: many_to_one
  }

  join: candidate_message_threads {
    type: left_outer
    sql_on: ${job_applications.candidate_message_thread_id} = ${candidate_message_threads.id} ;;
    relationship: many_to_one
  }

  join: review_lists {
    type: left_outer
    sql_on: ${job_applications.review_list_id} = ${review_lists.id} ;;
    relationship: many_to_one
  }

  join: degrees {
    type: left_outer
    sql_on: ${jobs.degree_id} = ${degrees.id} ;;
    relationship: many_to_one
  }

  join: locations {
    type: left_outer
    sql_on: ${jobs.location_id} = ${locations.id} ;;
    relationship: many_to_one
  }

  join: part_time_schedules {
    type: left_outer
    sql_on: ${jobs.part_time_schedule_id} = ${part_time_schedules.id} ;;
    relationship: many_to_one
  }

  join: pay_types {
    type: left_outer
    sql_on: ${jobs.pay_type_id} = ${pay_types.id} ;;
    relationship: many_to_one
  }

  join: schedule_types {
    type: left_outer
    sql_on: ${jobs.schedule_type_id} = ${schedule_types.id} ;;
    relationship: many_to_one
  }

  join: admin_roles {
    type: left_outer
    sql_on: ${users.admin_role_id} = ${admin_roles.id} ;;
    relationship: many_to_one
  }
}
