view: application_statuses {
  sql_table_name: willh.application_statuses ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: base_video_name {
    type: string
    sql: ${TABLE}.base_video_name ;;
  }

  dimension: callback_api_attempts {
    type: number
    sql: ${TABLE}.callback_api_attempts ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: filter_question_answers {
    type: string
    sql: ${TABLE}.filter_question_answers ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: note {
    type: string
    sql: ${TABLE}.note ;;
  }

  dimension: single_question_retake {
    type: yesno
    sql: ${TABLE}.single_question_retake ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: unsubscribe_code {
    type: string
    sql: ${TABLE}.unsubscribe_code ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: uploaded_requirements {
    type: yesno
    sql: ${TABLE}.uploaded_requirements ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  dimension: video_interview_last_type {
    type: string
    sql: ${TABLE}.video_interview_last_type ;;
  }

  dimension: video_interview_types {
    type: string
    sql: ${TABLE}.video_interview_types ;;
  }

  dimension: viewed_questions {
    type: string
    sql: ${TABLE}.viewed_questions ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      base_video_name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name
    ]
  }
}
