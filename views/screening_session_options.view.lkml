view: screening_session_options {
  sql_table_name: willh.screening_session_options ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: screening_option_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_option_id ;;
  }

  dimension: screening_question_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_question_id ;;
  }

  dimension: screening_session_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_session_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: version_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.version_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      screening_options.id,
      screening_options.image_file_name,
      screening_options.name,
      screening_questions.id,
      screening_questions.question_image_file_name,
      screening_questions.screening_competency_measure_name,
      screening_sessions.id,
      versions.id
    ]
  }
}
