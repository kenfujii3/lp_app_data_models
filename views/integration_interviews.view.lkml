view: integration_interviews {
  sql_table_name: willh.integration_interviews ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_integration_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_integration_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: external_interview_id {
    type: string
    sql: ${TABLE}.external_interview_id ;;
  }

  dimension: interview_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.interview_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      account_integrations.endpoint_username,
      account_integrations.id,
      account_integrations.name,
      account_integrations.username,
      interviews.id,
      interviews.redirect_button_name,
      interviews.screening_redirect_button_name,
      applicant_invites.count
    ]
  }
}
