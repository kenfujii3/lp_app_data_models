view: job_integration_mappings {
  sql_table_name: willh.job_integration_mappings ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_integration_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_integration_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: interview_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.interview_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: package_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.package_id ;;
  }

  dimension: package_title {
    type: string
    sql: ${TABLE}.package_title ;;
  }

  dimension: requisition_id {
    type: string
    sql: ${TABLE}.requisition_id ;;
  }

  dimension: requisition_title {
    type: string
    sql: ${TABLE}.requisition_title ;;
  }

  dimension: template_id {
    type: string
    sql: ${TABLE}.template_id ;;
  }

  dimension: template_title {
    type: string
    sql: ${TABLE}.template_title ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      account_integrations.endpoint_username,
      account_integrations.id,
      account_integrations.name,
      account_integrations.username,
      interviews.id,
      interviews.redirect_button_name,
      interviews.screening_redirect_button_name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      packages.id,
      packages.name
    ]
  }
}
