view: review_criteria {
  sql_table_name: willh.review_criteria ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: negative_indicator {
    type: string
    sql: ${TABLE}.negative_indicator ;;
  }

  dimension: position {
    type: number
    sql: ${TABLE}.position ;;
  }

  dimension: positive_indicator {
    type: string
    sql: ${TABLE}.positive_indicator ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.uuid ;;
  }

  dimension: va_rc_group_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.va_rc_group_uuid ;;
  }

  dimension: video_question_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.video_question_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name]
  }
}
