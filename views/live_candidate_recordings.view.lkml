view: live_candidate_recordings {
  sql_table_name: willh.live_candidate_recordings ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: filename {
    type: string
    sql: ${TABLE}.filename ;;
  }

  dimension: live_candidate_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_candidate_id ;;
  }

  dimension: live_interview_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_interview_id ;;
  }

  dimension: sightcall_id {
    type: number
    sql: ${TABLE}.sightcall_id ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: url_mp4 {
    type: string
    sql: ${TABLE}.url_mp4 ;;
  }

  dimension: url_webm {
    type: string
    sql: ${TABLE}.url_webm ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      filename,
      live_candidates.first_name,
      live_candidates.id,
      live_candidates.last_name,
      live_interviews.id
    ]
  }
}
