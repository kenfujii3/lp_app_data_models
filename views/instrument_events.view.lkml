view: instrument_events {
  sql_table_name: willh.instrument_events ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: conductor_instrument_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.conductor_instrument_id ;;
  }

  dimension_group: event_timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.event_timestamp ;;
  }

  dimension: event_triggered {
    type: string
    sql: ${TABLE}.event_triggered ;;
  }

  measure: count {
    type: count
    drill_fields: [id, conductor_instruments.id]
  }
}
