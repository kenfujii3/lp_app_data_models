view: feedback_category_frameworks {
  sql_table_name: willh.feedback_category_frameworks ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: candidate_feedback_setting_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_feedback_setting_id ;;
  }

  dimension: feedback_category_name {
    type: string
    sql: ${TABLE}.feedback_category_name ;;
  }

  dimension: max_score {
    type: number
    sql: ${TABLE}.max_score ;;
  }

  dimension: min_score {
    type: number
    sql: ${TABLE}.min_score ;;
  }

  dimension: shown_competencies {
    type: number
    sql: ${TABLE}.shown_competencies ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }

  measure: count {
    type: count
    drill_fields: [id, feedback_category_name, candidate_feedback_settings.id]
  }
}
