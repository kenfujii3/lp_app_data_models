view: assessment_platforms {
  sql_table_name: willh.assessment_platforms ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: assessment_session_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.assessment_session_uuid ;;
  }

  dimension: platform_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.platform_id ;;
  }

  measure: count {
    type: count
    drill_fields: [id, platforms.display_name, platforms.id, platforms.name]
  }
}
