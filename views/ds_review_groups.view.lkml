view: ds_review_groups {
  sql_table_name: willh.ds_review_groups ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: applied_after {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.applied_after ;;
  }

  dimension_group: applied_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.applied_end_time ;;
  }

  dimension_group: applied_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.applied_start_time ;;
  }

  dimension: benchmark_scores_attendees {
    type: string
    sql: ${TABLE}.benchmark_scores_attendees ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: creator_id {
    type: number
    sql: ${TABLE}.creator_id ;;
  }

  dimension: criteria_review_type {
    type: string
    sql: ${TABLE}.criteria_review_type ;;
  }

  dimension: ds_review_group_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_review_group_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: no_of_candidates_to_test {
    type: number
    sql: ${TABLE}.no_of_candidates_to_test ;;
  }

  dimension: overlap_threshold {
    type: number
    sql: ${TABLE}.overlap_threshold ;;
  }

  dimension: pass_mark {
    type: number
    sql: ${TABLE}.pass_mark ;;
  }

  dimension: pending_reviewer_invites {
    type: string
    sql: ${TABLE}.pending_reviewer_invites ;;
  }

  dimension: percentage_overlap {
    type: number
    sql: ${TABLE}.percentage_overlap ;;
  }

  dimension: question_group {
    type: string
    sql: ${TABLE}.question_group ;;
  }

  dimension: review_experience {
    type: string
    sql: ${TABLE}.review_experience ;;
  }

  dimension: review_type {
    type: string
    sql: ${TABLE}.review_type ;;
  }

  dimension: test {
    type: yesno
    sql: ${TABLE}.test ;;
  }

  dimension: test_status {
    type: string
    sql: ${TABLE}.test_status ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      name,
      ds_review_groups.id,
      ds_review_groups.name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      criteria_reviews.count,
      ds_benchmark_scores.count,
      ds_prioritise_candidates.count,
      ds_rereview_candidates.count,
      ds_review_applicants.count,
      ds_review_group_memberships.count,
      ds_review_groups.count,
      job_application_reviews.count
    ]
  }
}
