view: security_password_controls {
  sql_table_name: willh.security_password_controls ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension_group: last_password_change {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_password_change ;;
  }

  dimension: notify_email {
    type: number
    sql: ${TABLE}.notify_email ;;
  }

  dimension: password_change {
    type: number
    sql: ${TABLE}.password_change ;;
  }

  dimension: password_mix {
    type: yesno
    sql: ${TABLE}.password_mix ;;
  }

  dimension: password_reuse {
    type: yesno
    sql: ${TABLE}.password_reuse ;;
  }

  dimension: password_suspended {
    type: number
    sql: ${TABLE}.password_suspended ;;
  }

  dimension: session_duration {
    type: number
    sql: ${TABLE}.session_duration ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, accounts.maybe_field_name, accounts.name, accounts.provisioner_account_id, accounts.subdomain_name]
  }
}
