view: competency_descriptions {
  sql_table_name: willh.competency_descriptions ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: content {
    type: string
    sql: ${TABLE}.content ;;
  }

  dimension: feedback_category_name {
    type: string
    sql: ${TABLE}.feedback_category_name ;;
  }

  dimension: feedback_competency_id {
    type: number
    sql: ${TABLE}.feedback_competency_id ;;
  }

  measure: count {
    type: count
    drill_fields: [id, feedback_category_name]
  }
}
