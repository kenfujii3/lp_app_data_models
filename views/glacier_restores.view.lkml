view: glacier_restores {
  sql_table_name: willh.glacier_restores ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: candidate_email {
    type: string
    sql: ${TABLE}.candidate_email ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: glacier_id {
    type: string
    sql: ${TABLE}.glacier_id ;;
  }

  dimension: glacier_job_id {
    type: string
    sql: ${TABLE}.glacier_job_id ;;
  }

  dimension: job_application_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_application_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: requester_id {
    type: number
    sql: ${TABLE}.requester_id ;;
  }

  dimension: restored {
    type: yesno
    sql: ${TABLE}.restored ;;
  }

  dimension: s3_path {
    type: string
    sql: ${TABLE}.s3_path ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: vault {
    type: string
    sql: ${TABLE}.vault ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      job_applications.base_video_name,
      job_applications.id,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id
    ]
  }
}
