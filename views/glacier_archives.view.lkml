view: glacier_archives {
  sql_table_name: willh.glacier_archives ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: archive_type {
    type: string
    sql: ${TABLE}.archive_type ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: glacier_id {
    type: string
    sql: ${TABLE}.glacier_id ;;
  }

  dimension: job_application_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_application_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: s3_bucket {
    type: string
    sql: ${TABLE}.s3_bucket ;;
  }

  dimension: s3_path {
    type: string
    sql: ${TABLE}.s3_path ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: vault {
    type: string
    sql: ${TABLE}.vault ;;
  }

  dimension: video_encoding_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.video_encoding_id ;;
  }

  dimension: video_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.video_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      job_applications.base_video_name,
      job_applications.id,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      video_encodings.extname,
      video_encodings.filename,
      video_encodings.id,
      video_encodings.panda_profile_name,
      videos.extname,
      videos.filename,
      videos.id
    ]
  }
}
