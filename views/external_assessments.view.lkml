view: external_assessments {
  sql_table_name: willh.external_assessments ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: cached_data_key {
    type: string
    sql: ${TABLE}.cached_data_key ;;
  }

  dimension: cached_data_value {
    type: string
    sql: ${TABLE}.cached_data_value ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: data_attributes {
    type: string
    sql: ${TABLE}.data_attributes ;;
  }

  dimension: modular_assessment_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.modular_assessment_id ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name, modular_assessments.id]
  }
}
