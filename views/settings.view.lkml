view: settings {
  sql_table_name: willh.settings ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: admin_default_rejection_message {
    type: string
    sql: ${TABLE}.admin_default_rejection_message ;;
  }

  dimension: applicant_interview_access {
    type: yesno
    sql: ${TABLE}.applicant_interview_access ;;
  }

  dimension: archive_notice {
    type: yesno
    sql: ${TABLE}.archive_notice ;;
  }

  dimension: autoplay_review_video {
    type: yesno
    sql: ${TABLE}.autoplay_review_video ;;
  }

  dimension: collaborator_add_comment {
    type: yesno
    sql: ${TABLE}.collaborator_add_comment ;;
  }

  dimension: collaborator_add_other {
    type: yesno
    sql: ${TABLE}.collaborator_add_other ;;
  }

  dimension: collaborator_add_rating {
    type: yesno
    sql: ${TABLE}.collaborator_add_rating ;;
  }

  dimension: collaborator_edit_interview {
    type: yesno
    sql: ${TABLE}.collaborator_edit_interview ;;
  }

  dimension: collaborator_edit_job {
    type: yesno
    sql: ${TABLE}.collaborator_edit_job ;;
  }

  dimension: collaborator_invite_applicants {
    type: yesno
    sql: ${TABLE}.collaborator_invite_applicants ;;
  }

  dimension: collaborator_publish_interview {
    type: yesno
    sql: ${TABLE}.collaborator_publish_interview ;;
  }

  dimension: collaborator_remove_candidates {
    type: yesno
    sql: ${TABLE}.collaborator_remove_candidates ;;
  }

  dimension: collaborator_send_message {
    type: yesno
    sql: ${TABLE}.collaborator_send_message ;;
  }

  dimension: collaborator_share_interview {
    type: yesno
    sql: ${TABLE}.collaborator_share_interview ;;
  }

  dimension: collaborator_shortlist_reject {
    type: yesno
    sql: ${TABLE}.collaborator_shortlist_reject ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: default_user_language {
    type: string
    sql: ${TABLE}.default_user_language ;;
  }

  dimension: default_user_timezone {
    type: string
    sql: ${TABLE}.default_user_timezone ;;
  }

  dimension: email_bounce_notice {
    type: yesno
    sql: ${TABLE}.email_bounce_notice ;;
  }

  dimension: employer_default_closing_message {
    type: string
    sql: ${TABLE}.employer_default_closing_message ;;
  }

  dimension: employer_default_intro_message {
    type: string
    sql: ${TABLE}.employer_default_intro_message ;;
  }

  dimension: employer_default_invite_message {
    type: string
    sql: ${TABLE}.employer_default_invite_message ;;
  }

  dimension: employer_default_rejection_message {
    type: string
    sql: ${TABLE}.employer_default_rejection_message ;;
  }

  dimension: employer_notify_new_applications {
    type: yesno
    sql: ${TABLE}.employer_notify_new_applications ;;
  }

  dimension: employer_notify_new_applications_frequency {
    type: string
    sql: ${TABLE}.employer_notify_new_applications_frequency ;;
  }

  dimension: employer_prompt_rejection_notifications {
    type: yesno
    sql: ${TABLE}.employer_prompt_rejection_notifications ;;
  }

  dimension: jobseeker_notify_new_job_listings {
    type: yesno
    sql: ${TABLE}.jobseeker_notify_new_job_listings ;;
  }

  dimension: new_email_address {
    type: string
    sql: ${TABLE}.new_email_address ;;
  }

  dimension: new_email_confirmed {
    type: yesno
    sql: ${TABLE}.new_email_confirmed ;;
  }

  dimension: notify_new_messages {
    type: yesno
    sql: ${TABLE}.notify_new_messages ;;
  }

  dimension: show_clone_interview_popup {
    type: yesno
    sql: ${TABLE}.show_clone_interview_popup ;;
  }

  dimension: subscribe_to_newsletter {
    type: yesno
    sql: ${TABLE}.subscribe_to_newsletter ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name
    ]
  }
}
