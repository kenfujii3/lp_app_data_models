view: work_experiences {
  sql_table_name: willh.work_experiences ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension: company_name {
    type: string
    sql: ${TABLE}.company_name ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: duration_month_value {
    type: number
    sql: ${TABLE}.duration_month_value ;;
  }

  dimension: duration_type {
    type: string
    sql: ${TABLE}.duration_type ;;
  }

  dimension: duration_value {
    type: string
    sql: ${TABLE}.duration_value ;;
  }

  dimension: duration_year_value {
    type: number
    sql: ${TABLE}.duration_year_value ;;
  }

  dimension: industry_id {
    type: number
    sql: ${TABLE}.industry_id ;;
  }

  dimension: industry_name {
    type: string
    sql: ${TABLE}.industry_name ;;
  }

  dimension: industry_role_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.industry_role_id ;;
  }

  dimension: industry_role_name {
    type: string
    sql: ${TABLE}.industry_role_name ;;
  }

  dimension: job_seeker_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_seeker_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: work_experience_duration_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.work_experience_duration_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      company_name,
      industry_name,
      industry_role_name,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      industry_roles.id,
      industry_roles.name,
      job_seekers.cv_file_name,
      job_seekers.id,
      job_seekers.location_name,
      job_seekers.nearest_location_name,
      job_seekers.photo_file_name,
      work_experience_durations.id,
      work_experience_durations.name
    ]
  }
}
