view: ds_reviewers {
  sql_table_name: willh.ds_reviewers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension_group: date_joined {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date_joined ;;
  }

  dimension_group: date_of_birth {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date_of_birth ;;
  }

  dimension: ds_city_id {
    type: number
    sql: ${TABLE}.ds_city_id ;;
  }

  dimension: ds_country_id {
    type: number
    sql: ${TABLE}.ds_country_id ;;
  }

  dimension: ds_ethnicity_id {
    type: number
    sql: ${TABLE}.ds_ethnicity_id ;;
  }

  dimension: ds_nationality_id {
    type: number
    sql: ${TABLE}.ds_nationality_id ;;
  }

  dimension: ds_reviewer_position_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_reviewer_position_id ;;
  }

  dimension: ds_seniority_id {
    type: number
    sql: ${TABLE}.ds_seniority_id ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: other_ethnicity {
    type: string
    sql: ${TABLE}.other_ethnicity ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      ds_reviewer_positions.id,
      ds_reviewer_positions.name,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name,
      ds_benchmark_scores.count,
      ds_job_infos.count,
      ds_prioritise_candidates.count,
      ds_question_answers.count,
      ds_review_group_memberships.count
    ]
  }
}
