view: educations {
  sql_table_name: willh.educations ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: degree_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.degree_id ;;
  }

  dimension: degree_name {
    type: string
    sql: ${TABLE}.degree_name ;;
  }

  dimension: degree_subject {
    type: string
    sql: ${TABLE}.degree_subject ;;
  }

  dimension: grade_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.grade_id ;;
  }

  dimension: grade_name {
    type: string
    sql: ${TABLE}.grade_name ;;
  }

  dimension: graduating_year {
    type: number
    sql: ${TABLE}.graduating_year ;;
  }

  dimension: job_seeker_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_seeker_id ;;
  }

  dimension: standard_grade_name {
    type: string
    sql: ${TABLE}.standard_grade_name ;;
  }

  dimension: university_course_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.university_course_id ;;
  }

  dimension: university_course_name {
    type: string
    sql: ${TABLE}.university_course_name ;;
  }

  dimension: university_id {
    type: number
    sql: ${TABLE}.university_id ;;
  }

  dimension: university_name {
    type: string
    sql: ${TABLE}.university_name ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      degree_name,
      grade_name,
      standard_grade_name,
      university_course_name,
      university_name,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      degrees.id,
      degrees.name,
      grades.id,
      grades.name,
      job_seekers.cv_file_name,
      job_seekers.id,
      job_seekers.location_name,
      job_seekers.nearest_location_name,
      job_seekers.photo_file_name,
      university_courses.id,
      university_courses.name
    ]
  }
}
