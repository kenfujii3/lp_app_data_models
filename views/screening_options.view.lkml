view: screening_options {
  sql_table_name: willh.screening_options ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: best_worst {
    type: string
    sql: ${TABLE}.best_worst ;;
  }

  dimension: correct_rank {
    type: number
    sql: ${TABLE}.correct_rank ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: custom_option {
    type: string
    sql: ${TABLE}.custom_option ;;
  }

  dimension: delete_flag {
    type: yesno
    sql: ${TABLE}.delete_flag ;;
  }

  dimension: image_content_type {
    type: string
    sql: ${TABLE}.image_content_type ;;
  }

  dimension: image_file_name {
    type: string
    sql: ${TABLE}.image_file_name ;;
  }

  dimension: image_file_size {
    type: number
    sql: ${TABLE}.image_file_size ;;
  }

  dimension_group: image_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.image_updated_at ;;
  }

  dimension: instant_exit {
    type: yesno
    sql: ${TABLE}.instant_exit ;;
  }

  dimension: most_least {
    type: string
    sql: ${TABLE}.most_least ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: position {
    type: number
    sql: ${TABLE}.position ;;
  }

  dimension: score {
    type: number
    sql: ${TABLE}.score ;;
  }

  dimension: score_correct {
    type: number
    sql: ${TABLE}.score_correct ;;
  }

  dimension: score_incorrect {
    type: number
    sql: ${TABLE}.score_incorrect ;;
  }

  dimension: screening_question_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_question_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      image_file_name,
      name,
      screening_questions.id,
      screening_questions.question_image_file_name,
      screening_questions.screening_competency_measure_name,
      candidate_screening_option_answers.count,
      candidate_screening_options.count,
      screening_session_options.count
    ]
  }
}
