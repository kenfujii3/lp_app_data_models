view: subset_settings {
  sql_table_name: willh.subset_settings ;;

  dimension: question_group_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.question_group_uuid ;;
  }

  dimension: value {
    type: number
    sql: ${TABLE}.value ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
