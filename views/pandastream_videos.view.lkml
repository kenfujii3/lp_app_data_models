view: pandastream_videos {
  sql_table_name: willh.pandastream_videos ;;
  drill_fields: [pandastream_video_id]

  dimension: pandastream_video_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.pandastream_video_id ;;
  }

  dimension: audio_bitrate {
    type: number
    sql: ${TABLE}.audio_bitrate ;;
  }

  dimension: audio_channels {
    type: number
    sql: ${TABLE}.audio_channels ;;
  }

  dimension: audio_codec {
    type: string
    sql: ${TABLE}.audio_codec ;;
  }

  dimension: audio_sample_rate {
    type: number
    sql: ${TABLE}.audio_sample_rate ;;
  }

  dimension: cloud_id {
    type: string
    sql: ${TABLE}.cloud_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: duration {
    type: number
    sql: ${TABLE}.duration ;;
  }

  dimension: error_class {
    type: string
    sql: ${TABLE}.error_class ;;
  }

  dimension: error_message {
    type: string
    sql: ${TABLE}.error_message ;;
  }

  dimension: file_size {
    type: number
    sql: ${TABLE}.file_size ;;
  }

  dimension: fps {
    type: number
    sql: ${TABLE}.fps ;;
  }

  dimension: height {
    type: number
    sql: ${TABLE}.height ;;
  }

  dimension: id {
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: mime_type {
    type: string
    sql: ${TABLE}.mime_type ;;
  }

  dimension: modular_video_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.modular_video_uuid ;;
  }

  dimension: path {
    type: string
    sql: ${TABLE}.path ;;
  }

  dimension: payload {
    type: string
    sql: ${TABLE}.payload ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: video_bitrate {
    type: number
    sql: ${TABLE}.video_bitrate ;;
  }

  dimension: video_codec {
    type: string
    sql: ${TABLE}.video_codec ;;
  }

  dimension: video_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.video_uuid ;;
  }

  dimension: width {
    type: number
    sql: ${TABLE}.width ;;
  }

  measure: count {
    type: count
    drill_fields: [pandastream_video_id]
  }
}
