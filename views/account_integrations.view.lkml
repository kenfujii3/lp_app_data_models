view: account_integrations {
  sql_table_name: willh.account_integrations ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: auto_complete_days {
    type: number
    sql: ${TABLE}.auto_complete_days ;;
  }

  dimension: auto_complete_no_criteria {
    type: yesno
    sql: ${TABLE}.auto_complete_no_criteria ;;
  }

  dimension: client_id {
    type: number
    sql: ${TABLE}.client_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: current {
    type: yesno
    sql: ${TABLE}.current ;;
  }

  dimension: default_reviewer_id {
    type: number
    sql: ${TABLE}.default_reviewer_id ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: enable_integrated_subaccounts {
    type: yesno
    sql: ${TABLE}.enable_integrated_subaccounts ;;
  }

  dimension: enable_raw_score_callback {
    type: yesno
    sql: ${TABLE}.enable_raw_score_callback ;;
  }

  dimension: enable_single_reviewer {
    type: yesno
    sql: ${TABLE}.enable_single_reviewer ;;
  }

  dimension: enable_template_mapping {
    type: yesno
    sql: ${TABLE}.enable_template_mapping ;;
  }

  dimension: endpoint {
    type: string
    sql: ${TABLE}.endpoint ;;
  }

  dimension: endpoint_password {
    type: string
    sql: ${TABLE}.endpoint_password ;;
  }

  dimension: endpoint_username {
    type: string
    sql: ${TABLE}.endpoint_username ;;
  }

  dimension: integration_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.integration_id ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: session_timeout_hours {
    type: number
    sql: ${TABLE}.session_timeout_hours ;;
  }

  dimension: settings {
    type: string
    sql: ${TABLE}.settings ;;
  }

  dimension: token {
    type: string
    sql: ${TABLE}.token ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      endpoint_username,
      name,
      username,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      integrations.id,
      integrations.name,
      integrations.test_username,
      integrations.username,
      integration_interviews.count,
      integration_transactions.count,
      job_integration_mappings.count
    ]
  }
}
