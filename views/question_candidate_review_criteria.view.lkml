view: question_candidate_review_criteria {
  sql_table_name: willh.question_candidate_review_criteria ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: answer_attempt_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.answer_attempt_id ;;
  }

  dimension: candidate_review_criteria_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_review_criteria_id ;;
  }

  dimension: question_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.question_uuid ;;
  }

  dimension: question_version_id {
    type: number
    sql: ${TABLE}.question_version_id ;;
  }

  measure: count {
    type: count
    drill_fields: [id, answer_attempts.id, candidate_review_criteria.id]
  }
}
