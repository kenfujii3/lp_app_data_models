view: companies {
  sql_table_name: willh.companies ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: cached_slug {
    type: string
    sql: ${TABLE}.cached_slug ;;
  }

  dimension: candidate_feedback_image_one_content_type {
    type: string
    sql: ${TABLE}.candidate_feedback_image_one_content_type ;;
  }

  dimension: candidate_feedback_image_one_file_name {
    type: string
    sql: ${TABLE}.candidate_feedback_image_one_file_name ;;
  }

  dimension: candidate_feedback_image_one_file_size {
    type: number
    sql: ${TABLE}.candidate_feedback_image_one_file_size ;;
  }

  dimension_group: candidate_feedback_image_one_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.candidate_feedback_image_one_updated_at ;;
  }

  dimension: candidate_feedback_image_two_content_type {
    type: string
    sql: ${TABLE}.candidate_feedback_image_two_content_type ;;
  }

  dimension: candidate_feedback_image_two_file_name {
    type: string
    sql: ${TABLE}.candidate_feedback_image_two_file_name ;;
  }

  dimension: candidate_feedback_image_two_file_size {
    type: number
    sql: ${TABLE}.candidate_feedback_image_two_file_size ;;
  }

  dimension_group: candidate_feedback_image_two_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.candidate_feedback_image_two_updated_at ;;
  }

  dimension: candidate_feedback_logo_content_type {
    type: string
    sql: ${TABLE}.candidate_feedback_logo_content_type ;;
  }

  dimension: candidate_feedback_logo_file_name {
    type: string
    sql: ${TABLE}.candidate_feedback_logo_file_name ;;
  }

  dimension: candidate_feedback_logo_file_size {
    type: number
    sql: ${TABLE}.candidate_feedback_logo_file_size ;;
  }

  dimension_group: candidate_feedback_logo_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.candidate_feedback_logo_updated_at ;;
  }

  dimension: comment {
    type: string
    sql: ${TABLE}.comment ;;
  }

  dimension: company_banner_content_type {
    type: string
    sql: ${TABLE}.company_banner_content_type ;;
  }

  dimension: company_banner_file_name {
    type: string
    sql: ${TABLE}.company_banner_file_name ;;
  }

  dimension: company_banner_file_size {
    type: number
    sql: ${TABLE}.company_banner_file_size ;;
  }

  dimension_group: company_banner_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.company_banner_updated_at ;;
  }

  dimension: company_branded_logo_content_type {
    type: string
    sql: ${TABLE}.company_branded_logo_content_type ;;
  }

  dimension: company_branded_logo_file_name {
    type: string
    sql: ${TABLE}.company_branded_logo_file_name ;;
  }

  dimension: company_branded_logo_file_size {
    type: number
    sql: ${TABLE}.company_branded_logo_file_size ;;
  }

  dimension_group: company_branded_logo_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.company_branded_logo_updated_at ;;
  }

  dimension: company_logo_content_type {
    type: string
    sql: ${TABLE}.company_logo_content_type ;;
  }

  dimension: company_logo_file_name {
    type: string
    sql: ${TABLE}.company_logo_file_name ;;
  }

  dimension: company_logo_file_size {
    type: number
    sql: ${TABLE}.company_logo_file_size ;;
  }

  dimension_group: company_logo_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.company_logo_updated_at ;;
  }

  dimension: company_name {
    type: string
    sql: ${TABLE}.company_name ;;
  }

  dimension: company_size_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.company_size_id ;;
  }

  dimension: country_name {
    type: string
    sql: ${TABLE}.country_name ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: ds_company_size_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_company_size_id ;;
  }

  dimension: ds_company_type_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_company_type_id ;;
  }

  dimension: ds_industry_id {
    type: number
    sql: ${TABLE}.ds_industry_id ;;
  }

  dimension: geo_city {
    type: string
    sql: ${TABLE}.geo_city ;;
  }

  dimension: image_background_content_type {
    type: string
    sql: ${TABLE}.image_background_content_type ;;
  }

  dimension: image_background_file_name {
    type: string
    sql: ${TABLE}.image_background_file_name ;;
  }

  dimension: image_background_file_size {
    type: number
    sql: ${TABLE}.image_background_file_size ;;
  }

  dimension_group: image_background_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.image_background_updated_at ;;
  }

  dimension: industry_id {
    type: number
    sql: ${TABLE}.industry_id ;;
  }

  dimension_group: last_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_updated ;;
  }

  dimension: latitude {
    type: number
    sql: ${TABLE}.latitude ;;
  }

  dimension: linkedin_profile {
    type: string
    sql: ${TABLE}.linkedin_profile ;;
  }

  dimension: location_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.location_id ;;
  }

  dimension: location_name {
    type: string
    sql: ${TABLE}.location_name ;;
  }

  dimension: longitude {
    type: number
    sql: ${TABLE}.longitude ;;
  }

  dimension: post_code {
    type: string
    sql: ${TABLE}.post_code ;;
  }

  dimension_group: posting {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.posting_date ;;
  }

  dimension: profile_status {
    type: string
    sql: ${TABLE}.profile_status ;;
  }

  dimension: sms_company_name {
    type: string
    sql: ${TABLE}.sms_company_name ;;
  }

  dimension: street_address {
    type: string
    sql: ${TABLE}.street_address ;;
  }

  dimension: twitter_profile {
    type: string
    sql: ${TABLE}.twitter_profile ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  dimension: video_url {
    type: string
    sql: ${TABLE}.video_url ;;
  }

  dimension: visible {
    type: yesno
    sql: ${TABLE}.visible ;;
  }

  dimension: website {
    type: string
    sql: ${TABLE}.website ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      candidate_feedback_image_one_file_name,
      candidate_feedback_image_two_file_name,
      candidate_feedback_logo_file_name,
      company_banner_file_name,
      company_branded_logo_file_name,
      company_logo_file_name,
      company_name,
      country_name,
      image_background_file_name,
      location_name,
      sms_company_name,
      company_sizes.id,
      company_sizes.name,
      ds_company_sizes.id,
      ds_company_sizes.name,
      ds_company_types.id,
      ds_company_types.name,
      locations.id,
      locations.name,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name
    ]
  }
}
