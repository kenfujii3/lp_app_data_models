view: integrations {
  sql_table_name: willh.integrations ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: api_key {
    type: string
    sql: ${TABLE}.api_key ;;
  }

  dimension: callback_url {
    type: string
    sql: ${TABLE}.callback_url ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: current_version {
    type: string
    sql: ${TABLE}.current_version ;;
  }

  dimension: endpoint {
    type: string
    sql: ${TABLE}.endpoint ;;
  }

  dimension: identifier {
    type: string
    sql: ${TABLE}.identifier ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: password {
    type: string
    sql: ${TABLE}.password ;;
  }

  dimension: secondary_endpoint {
    type: string
    sql: ${TABLE}.secondary_endpoint ;;
  }

  dimension: test_api_key {
    type: string
    sql: ${TABLE}.test_api_key ;;
  }

  dimension: test_callback_url {
    type: string
    sql: ${TABLE}.test_callback_url ;;
  }

  dimension: test_endpoint {
    type: string
    sql: ${TABLE}.test_endpoint ;;
  }

  dimension: test_password {
    type: string
    sql: ${TABLE}.test_password ;;
  }

  dimension: test_secondary_endpoint {
    type: string
    sql: ${TABLE}.test_secondary_endpoint ;;
  }

  dimension: test_username {
    type: string
    sql: ${TABLE}.test_username ;;
  }

  dimension: test_wsdl_endpoint {
    type: string
    sql: ${TABLE}.test_wsdl_endpoint ;;
  }

  dimension: test_wsdl_secondary_endpoint {
    type: string
    sql: ${TABLE}.test_wsdl_secondary_endpoint ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }

  dimension: wsdl_endpoint {
    type: string
    sql: ${TABLE}.wsdl_endpoint ;;
  }

  dimension: wsdl_secondary_endpoint {
    type: string
    sql: ${TABLE}.wsdl_secondary_endpoint ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name, test_username, username, account_integrations.count]
  }
}
