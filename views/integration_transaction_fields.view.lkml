view: integration_transaction_fields {
  sql_table_name: willh.integration_transaction_fields ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: data_category {
    type: string
    sql: ${TABLE}.data_category ;;
  }

  dimension: data_key {
    type: string
    sql: ${TABLE}.data_key ;;
  }

  dimension: data_value {
    type: string
    sql: ${TABLE}.data_value ;;
  }

  dimension: integration_transaction_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.integration_transaction_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      integration_transactions.candidate_first_name,
      integration_transactions.candidate_last_name,
      integration_transactions.id,
      integration_transactions.requester_first_name,
      integration_transactions.requester_last_name
    ]
  }
}
