view: criteria_review_answers {
  sql_table_name: willh.criteria_review_answers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: answer {
    type: string
    sql: ${TABLE}.answer ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: criteria_review_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.criteria_review_id ;;
  }

  dimension: job_application_review_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_application_review_id ;;
  }

  dimension: last_session_id {
    type: string
    sql: ${TABLE}.last_session_id ;;
  }

  dimension: live_assessment_review_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_assessment_review_id ;;
  }

  dimension_group: review_score {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.review_score_time ;;
  }

  dimension_group: review_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.review_start_time ;;
  }

  dimension: screening_review_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_review_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, criteria_reviews.id, job_application_reviews.id, live_assessment_reviews.id, screening_reviews.id]
  }
}
