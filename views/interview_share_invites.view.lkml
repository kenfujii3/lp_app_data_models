view: interview_share_invites {
  sql_table_name: willh.interview_share_invites ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: active {
    type: yesno
    sql: ${TABLE}.active ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: current_views {
    type: number
    sql: ${TABLE}.current_views ;;
  }

  dimension: expire_type {
    type: string
    sql: ${TABLE}.expire_type ;;
  }

  dimension: invite_code {
    type: string
    sql: ${TABLE}.invite_code ;;
  }

  dimension: invitor_id {
    type: number
    sql: ${TABLE}.invitor_id ;;
  }

  dimension: job_application_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_application_id ;;
  }

  dimension: max_time {
    type: number
    sql: ${TABLE}.max_time ;;
  }

  dimension: max_views {
    type: number
    sql: ${TABLE}.max_views ;;
  }

  dimension: reference {
    type: string
    sql: ${TABLE}.reference ;;
  }

  dimension_group: renew_expire {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.renew_expire_time ;;
  }

  dimension: renew_max_view {
    type: number
    sql: ${TABLE}.renew_max_view ;;
  }

  dimension: show_details {
    type: yesno
    sql: ${TABLE}.show_details ;;
  }

  dimension: show_screening_results {
    type: yesno
    sql: ${TABLE}.show_screening_results ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      job_applications.base_video_name,
      job_applications.id,
      interview_share_invite_logs.count
    ]
  }
}
