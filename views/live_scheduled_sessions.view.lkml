view: live_scheduled_sessions {
  sql_table_name: willh.live_scheduled_sessions ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension_group: last_candidate_activity {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_candidate_activity ;;
  }

  dimension: last_candidate_id {
    type: number
    sql: ${TABLE}.last_candidate_id ;;
  }

  dimension_group: last_interviewer_activity {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_interviewer_activity ;;
  }

  dimension: last_interviewer_id {
    type: number
    sql: ${TABLE}.last_interviewer_id ;;
  }

  dimension: live_interview_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_interview_id ;;
  }

  dimension: scope {
    type: string
    sql: ${TABLE}.scope ;;
  }

  dimension_group: session_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.session_end ;;
  }

  dimension_group: session_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.session_start ;;
  }

  dimension: total_minutes {
    type: number
    sql: ${TABLE}.total_minutes ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, live_interviews.id, live_scheduled_answers.count, live_scheduled_comments.count]
  }
}
