view: candidate_screening_option_answers {
  sql_table_name: willh.candidate_screening_option_answers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: candidate_screening_question_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_screening_question_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: option_type {
    type: string
    sql: ${TABLE}.option_type ;;
  }

  dimension: screening_option_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_option_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, candidate_screening_questions.id, screening_options.id, screening_options.image_file_name, screening_options.name]
  }
}
