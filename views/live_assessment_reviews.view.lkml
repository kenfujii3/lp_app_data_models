view: live_assessment_reviews {
  sql_table_name: willh.live_assessment_reviews ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: comments {
    type: string
    sql: ${TABLE}.comments ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: finished {
    type: yesno
    sql: ${TABLE}.finished ;;
  }

  dimension: invitor_id {
    type: number
    sql: ${TABLE}.invitor_id ;;
  }

  dimension: live_assessment_reviewer_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_assessment_reviewer_id ;;
  }

  dimension: live_meeting_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_meeting_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      live_assessment_reviewers.first_name,
      live_assessment_reviewers.id,
      live_assessment_reviewers.last_name,
      live_meetings.id,
      criteria_review_answers.count
    ]
  }
}
