view: account_candidate_feedbacks {
  sql_table_name: willh.account_candidate_feedbacks ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: footer_text {
    type: string
    sql: ${TABLE}.footer_text ;;
  }

  dimension: include_competencies {
    type: yesno
    sql: ${TABLE}.include_competencies ;;
  }

  dimension: include_numeric_scores {
    type: yesno
    sql: ${TABLE}.include_numeric_scores ;;
  }

  dimension: include_reviewer_comments {
    type: yesno
    sql: ${TABLE}.include_reviewer_comments ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: use_company_logo {
    type: yesno
    sql: ${TABLE}.use_company_logo ;;
  }

  measure: count {
    type: count
    drill_fields: [id, accounts.maybe_field_name, accounts.name, accounts.provisioner_account_id, accounts.subdomain_name]
  }
}
