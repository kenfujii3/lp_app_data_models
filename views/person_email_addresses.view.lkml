view: person_email_addresses {
  sql_table_name: willh.person_email_addresses ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: email_address_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.email_address_uuid ;;
  }

  dimension: person_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.person_uuid ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
