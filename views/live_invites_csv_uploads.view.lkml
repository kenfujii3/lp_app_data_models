view: live_invites_csv_uploads {
  sql_table_name: willh.live_invites_csv_uploads ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: csv_content_type {
    type: string
    sql: ${TABLE}.csv_content_type ;;
  }

  dimension: csv_file_name {
    type: string
    sql: ${TABLE}.csv_file_name ;;
  }

  dimension: csv_file_size {
    type: number
    sql: ${TABLE}.csv_file_size ;;
  }

  dimension_group: csv_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.csv_updated_at ;;
  }

  dimension: live_assessment_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_assessment_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: uploader_id {
    type: number
    sql: ${TABLE}.uploader_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      csv_file_name,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      live_assessments.id
    ]
  }
}
