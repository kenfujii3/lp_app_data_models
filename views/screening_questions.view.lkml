view: screening_questions {
  sql_table_name: willh.screening_questions ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: answer_type {
    type: string
    sql: ${TABLE}.answer_type ;;
  }

  dimension: auto_submit {
    type: yesno
    sql: ${TABLE}.auto_submit ;;
  }

  dimension: bool_negative_exit {
    type: yesno
    sql: ${TABLE}.bool_negative_exit ;;
  }

  dimension: bool_negative_score {
    type: number
    sql: ${TABLE}.bool_negative_score ;;
  }

  dimension: bool_positive_exit {
    type: yesno
    sql: ${TABLE}.bool_positive_exit ;;
  }

  dimension: bool_positive_score {
    type: number
    sql: ${TABLE}.bool_positive_score ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: custom_option_text {
    type: string
    sql: ${TABLE}.custom_option_text ;;
  }

  dimension: delete_flag {
    type: yesno
    sql: ${TABLE}.delete_flag ;;
  }

  dimension: display_type {
    type: string
    sql: ${TABLE}.display_type ;;
  }

  dimension: enable_ratings {
    type: yesno
    sql: ${TABLE}.enable_ratings ;;
  }

  dimension: first_option {
    type: string
    sql: ${TABLE}.first_option ;;
  }

  dimension: hidden {
    type: yesno
    sql: ${TABLE}.hidden ;;
  }

  dimension: is_practice_question {
    type: yesno
    sql: ${TABLE}.is_practice_question ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: last_option {
    type: string
    sql: ${TABLE}.last_option ;;
  }

  dimension: library_question {
    type: yesno
    sql: ${TABLE}.library_question ;;
  }

  dimension: multiple_selection {
    type: yesno
    sql: ${TABLE}.multiple_selection ;;
  }

  dimension: position {
    type: number
    sql: ${TABLE}.position ;;
  }

  dimension: process_video_question {
    type: yesno
    sql: ${TABLE}.process_video_question ;;
  }

  dimension: question {
    type: string
    sql: ${TABLE}.question ;;
  }

  dimension: question_image_content_type {
    type: string
    sql: ${TABLE}.question_image_content_type ;;
  }

  dimension: question_image_file_name {
    type: string
    sql: ${TABLE}.question_image_file_name ;;
  }

  dimension: question_image_file_size {
    type: number
    sql: ${TABLE}.question_image_file_size ;;
  }

  dimension_group: question_image_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.question_image_updated_at ;;
  }

  dimension: rating_mode {
    type: string
    sql: ${TABLE}.rating_mode ;;
  }

  dimension: rating_scale {
    type: number
    sql: ${TABLE}.rating_scale ;;
  }

  dimension: rating_scale_type {
    type: string
    sql: ${TABLE}.rating_scale_type ;;
  }

  dimension: right_to_left {
    type: yesno
    sql: ${TABLE}.right_to_left ;;
  }

  dimension: screening_competency_measure_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_competency_measure_id ;;
  }

  dimension: screening_competency_measure_name {
    type: string
    sql: ${TABLE}.screening_competency_measure_name ;;
  }

  dimension: screening_library_question_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_library_question_id ;;
  }

  dimension: screening_question_group_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_question_group_id ;;
  }

  dimension: screening_question_image_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_question_image_id ;;
  }

  dimension: text_answer_limit {
    type: number
    sql: ${TABLE}.text_answer_limit ;;
  }

  dimension: time_limit {
    type: number
    sql: ${TABLE}.time_limit ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: video_question_enabled {
    type: yesno
    sql: ${TABLE}.video_question_enabled ;;
  }

  dimension: video_question_file {
    type: string
    sql: ${TABLE}.video_question_file ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      question_image_file_name,
      screening_competency_measure_name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      screening_competency_measures.id,
      screening_competency_measures.name,
      screening_library_questions.id,
      screening_question_groups.id,
      screening_question_groups.name,
      screening_question_images.attachment_file_name,
      screening_question_images.id,
      candidate_screening_options.count,
      candidate_screening_questions.count,
      criteria_reviews.count,
      modular_screening_questions.count,
      screening_options.count,
      screening_question_images.count,
      screening_retakes.count,
      screening_session_options.count,
      screening_session_questions.count
    ]
  }
}
