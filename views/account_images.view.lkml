view: account_images {
  sql_table_name: willh.account_images ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: attachment_content_type {
    type: string
    sql: ${TABLE}.attachment_content_type ;;
  }

  dimension: attachment_file_name {
    type: string
    sql: ${TABLE}.attachment_file_name ;;
  }

  dimension: attachment_file_size {
    type: number
    sql: ${TABLE}.attachment_file_size ;;
  }

  dimension_group: attachment_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.attachment_updated_at ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: email_tag {
    type: string
    sql: ${TABLE}.email_tag ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      attachment_file_name,
      name,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name
    ]
  }
}
