view: question_groups {
  sql_table_name: willh.question_groups ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: interview_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.interview_id ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: question_count {
    type: number
    sql: ${TABLE}.question_count ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      name,
      interviews.id,
      interviews.redirect_button_name,
      interviews.screening_redirect_button_name,
      questions.count
    ]
  }
}
