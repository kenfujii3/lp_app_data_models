view: video_question_groups {
  sql_table_name: willh.video_question_groups ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: question_group_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.question_group_uuid ;;
  }

  dimension: stage_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.stage_uuid ;;
  }

  dimension: video_assessment_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.video_assessment_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
