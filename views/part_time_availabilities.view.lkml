view: part_time_availabilities {
  sql_table_name: willh.part_time_availabilities ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: availability_id {
    type: number
    sql: ${TABLE}.availability_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: job_seeker_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_seeker_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      job_seekers.cv_file_name,
      job_seekers.id,
      job_seekers.location_name,
      job_seekers.nearest_location_name,
      job_seekers.photo_file_name
    ]
  }
}
