view: ds_review_group_memberships {
  sql_table_name: willh.ds_review_group_memberships ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: ds_review_group_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_review_group_id ;;
  }

  dimension: ds_reviewer_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_reviewer_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, ds_review_groups.id, ds_review_groups.name, ds_reviewers.id]
  }
}
