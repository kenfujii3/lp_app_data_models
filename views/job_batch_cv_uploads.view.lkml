view: job_batch_cv_uploads {
  sql_table_name: willh.job_batch_cv_uploads ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: custom_cv_content_type {
    type: string
    sql: ${TABLE}.custom_cv_content_type ;;
  }

  dimension: custom_cv_file_name {
    type: string
    sql: ${TABLE}.custom_cv_file_name ;;
  }

  dimension: custom_cv_file_size {
    type: number
    sql: ${TABLE}.custom_cv_file_size ;;
  }

  dimension_group: custom_cv_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.custom_cv_updated_at ;;
  }

  dimension_group: date_processed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_processed ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      custom_cv_file_name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id
    ]
  }
}
