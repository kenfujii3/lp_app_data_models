view: users {
  sql_table_name: willh.users ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: accept_privacy_policy {
    type: yesno
    sql: ${TABLE}.accept_privacy_policy ;;
  }

  dimension: active {
    type: yesno
    sql: ${TABLE}.active ;;
  }

  dimension: admin_role_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.admin_role_id ;;
  }

  dimension: alt_confirmation_token {
    type: string
    sql: ${TABLE}.alt_confirmation_token ;;
  }

  dimension: api_created {
    type: yesno
    sql: ${TABLE}.api_created ;;
  }

  dimension: api_created_account_id {
    type: number
    sql: ${TABLE}.api_created_account_id ;;
  }

  dimension: authentication_token {
    type: string
    sql: ${TABLE}.authentication_token ;;
  }

  dimension: candidate_hash {
    type: string
    sql: ${TABLE}.candidate_hash ;;
  }

  dimension_group: confirmation_sent {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.confirmation_sent_at ;;
  }

  dimension: confirmation_token {
    type: string
    sql: ${TABLE}.confirmation_token ;;
  }

  dimension_group: confirmed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.confirmed_at ;;
  }

  dimension: contact_number {
    type: string
    sql: ${TABLE}.contact_number ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension_group: current_sign_in {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.current_sign_in_at ;;
  }

  dimension: current_sign_in_ip {
    type: string
    sql: ${TABLE}.current_sign_in_ip ;;
  }

  dimension: demo_id {
    type: string
    sql: ${TABLE}.demo_id ;;
  }

  dimension: demo_type {
    type: string
    sql: ${TABLE}.demo_type ;;
  }

  dimension: demo_video_name {
    type: string
    sql: ${TABLE}.demo_video_name ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: encrypted_password {
    type: string
    sql: ${TABLE}.encrypted_password ;;
  }

  dimension: failed_login_attempt {
    type: number
    sql: ${TABLE}.failed_login_attempt ;;
  }

  dimension: failed_login_attempt_email_sent {
    type: yesno
    sql: ${TABLE}.failed_login_attempt_email_sent ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}.first_name ;;
  }

  dimension: formatted_mobile {
    type: string
    sql: ${TABLE}.formatted_mobile ;;
  }

  dimension: has_video {
    type: yesno
    sql: ${TABLE}.has_video ;;
  }

  dimension: integration_password {
    type: string
    sql: ${TABLE}.integration_password ;;
  }

  dimension: integration_username {
    type: string
    sql: ${TABLE}.integration_username ;;
  }

  dimension_group: last_integration_login {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_integration_login ;;
  }

  dimension_group: last_message_notification {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_message_notification_at ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}.last_name ;;
  }

  dimension_group: last_password_change {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_password_change ;;
  }

  dimension_group: last_sign_in {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_sign_in_at ;;
  }

  dimension: last_sign_in_ip {
    type: string
    sql: ${TABLE}.last_sign_in_ip ;;
  }

  dimension: live_disable_email_domain {
    type: yesno
    sql: ${TABLE}.live_disable_email_domain ;;
  }

  dimension: mail_confirmed {
    type: yesno
    sql: ${TABLE}.mail_confirmed ;;
  }

  dimension: original_mobile {
    type: string
    sql: ${TABLE}.original_mobile ;;
  }

  dimension_group: remember_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.remember_created_at ;;
  }

  dimension_group: reset_password_sent {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.reset_password_sent_at ;;
  }

  dimension: reset_password_token {
    type: string
    sql: ${TABLE}.reset_password_token ;;
  }

  dimension: return_to {
    type: string
    sql: ${TABLE}.return_to ;;
  }

  dimension: sign_in_count {
    type: number
    sql: ${TABLE}.sign_in_count ;;
  }

  dimension: temp_password {
    type: yesno
    sql: ${TABLE}.temp_password ;;
  }

  dimension: terms {
    type: yesno
    sql: ${TABLE}.terms ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_type {
    type: string
    sql: ${TABLE}.user_type ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      demo_video_name,
      first_name,
      integration_username,
      last_name,
      admin_roles.id,
      admin_roles.name,
      account_images.count,
      account_notes.count,
      account_roles.count,
      account_users.count,
      activity_logs.count,
      admin_reports.count,
      applicant_invite_reminder_histories.count,
      applicant_subscriptions.count,
      application_statuses.count,
      assessment_invites.count,
      authentications.count,
      candidate_message_users.count,
      candidate_records.count,
      candidate_review_criteria.count,
      candidate_screening_view_statuses.count,
      companies.count,
      documents.count,
      ds_review_applicants.count,
      ds_reviewers.count,
      hiring_team_members.count,
      job_application_comments.count,
      job_application_question_versions.count,
      job_application_ratings.count,
      job_application_reviews.count,
      job_application_view_statuses.count,
      job_applications.count,
      job_clicks.count,
      job_collaborators.count,
      job_favorites.count,
      job_seeker_video_sessions.count,
      job_seeker_videos.count,
      job_seekers.count,
      jobs.count,
      live_assessment_reviewers.count,
      live_colleagues.count,
      live_criteria_review_answers.count,
      live_employers.count,
      live_pool_ids.count,
      live_review_comments.count,
      recorder_errors.count,
      reviewers.count,
      screening_reviews.count,
      security_reuse_passwords.count,
      settings.count,
      trial_user_invites.count,
      user_account_changes.count,
      user_admin_announcements.count,
      user_view_counters.count,
      video_playback_logs.count
    ]
  }
}
