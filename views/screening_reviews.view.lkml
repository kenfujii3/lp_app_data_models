view: screening_reviews {
  sql_table_name: willh.screening_reviews ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: average_score {
    type: number
    sql: ${TABLE}.average_score ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension: comment {
    type: string
    sql: ${TABLE}.comment ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: finished {
    type: yesno
    sql: ${TABLE}.finished ;;
  }

  dimension: finished_notice {
    type: yesno
    sql: ${TABLE}.finished_notice ;;
  }

  dimension: total_score {
    type: number
    sql: ${TABLE}.total_score ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name,
      criteria_review_answers.count
    ]
  }
}
