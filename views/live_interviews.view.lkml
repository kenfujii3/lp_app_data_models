view: live_interviews {
  sql_table_name: willh.live_interviews ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: allow_slot_request {
    type: yesno
    sql: ${TABLE}.allow_slot_request ;;
  }

  dimension: archived {
    type: yesno
    sql: ${TABLE}.archived ;;
  }

  dimension: badge {
    type: string
    sql: ${TABLE}.badge ;;
  }

  dimension: cached_slug {
    type: string
    sql: ${TABLE}.cached_slug ;;
  }

  dimension: comments {
    type: string
    sql: ${TABLE}.comments ;;
  }

  dimension: contingency {
    type: number
    sql: ${TABLE}.contingency ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: creator_id {
    type: number
    sql: ${TABLE}.creator_id ;;
  }

  dimension: default_live_interview_timezone {
    type: string
    sql: ${TABLE}.default_live_interview_timezone ;;
  }

  dimension: delete_flag {
    type: yesno
    sql: ${TABLE}.delete_flag ;;
  }

  dimension: duration {
    type: number
    sql: ${TABLE}.duration ;;
  }

  dimension: enable_ratings {
    type: yesno
    sql: ${TABLE}.enable_ratings ;;
  }

  dimension: interview_type {
    type: string
    sql: ${TABLE}.interview_type ;;
  }

  dimension: meeting_point_id {
    type: string
    sql: ${TABLE}.meeting_point_id ;;
  }

  dimension: new_slots {
    type: yesno
    sql: ${TABLE}.new_slots ;;
  }

  dimension: publish_status {
    type: string
    sql: ${TABLE}.publish_status ;;
  }

  dimension: rating_scale {
    type: number
    sql: ${TABLE}.rating_scale ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      live_available_schedules.count,
      live_candidate_recordings.count,
      live_candidates.count,
      live_colleagues.count,
      live_criteria_reviews.count,
      live_employers.count,
      live_interview_logs.count,
      live_interview_sessions.count,
      live_interview_slots.count,
      live_scheduled_sessions.count
    ]
  }
}
