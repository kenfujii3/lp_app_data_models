view: candidate_message_notes {
  sql_table_name: willh.candidate_message_notes ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: candidate_message_thread_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_message_thread_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: support_notes {
    type: string
    sql: ${TABLE}.support_notes ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, candidate_message_threads.id]
  }
}
