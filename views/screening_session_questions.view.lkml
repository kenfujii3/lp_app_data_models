view: screening_session_questions {
  sql_table_name: willh.screening_session_questions ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: answer {
    type: string
    sql: ${TABLE}.answer ;;
  }

  dimension: answer_normalized {
    type: number
    sql: ${TABLE}.answer_normalized ;;
  }

  dimension: answer_option_id {
    type: number
    sql: ${TABLE}.answer_option_id ;;
  }

  dimension: answer_time {
    type: number
    sql: ${TABLE}.answer_time ;;
  }

  dimension: answer_type {
    type: string
    sql: ${TABLE}.answer_type ;;
  }

  dimension: answer_unit {
    type: string
    sql: ${TABLE}.answer_unit ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: hidden {
    type: yesno
    sql: ${TABLE}.hidden ;;
  }

  dimension: option_answers {
    type: string
    sql: ${TABLE}.option_answers ;;
  }

  dimension: screening_library_question_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_library_question_id ;;
  }

  dimension: screening_question_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_question_id ;;
  }

  dimension: screening_session_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_session_id ;;
  }

  dimension: text_answer {
    type: string
    sql: ${TABLE}.text_answer ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: version_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.version_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      screening_library_questions.id,
      screening_questions.id,
      screening_questions.question_image_file_name,
      screening_questions.screening_competency_measure_name,
      screening_sessions.id,
      versions.id
    ]
  }
}
