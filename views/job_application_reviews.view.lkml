view: job_application_reviews {
  sql_table_name: willh.job_application_reviews ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: comment {
    type: string
    sql: ${TABLE}.comment ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: ds_review_group_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.ds_review_group_id ;;
  }

  dimension: finished {
    type: yesno
    sql: ${TABLE}.finished ;;
  }

  dimension: finished_notice {
    type: yesno
    sql: ${TABLE}.finished_notice ;;
  }

  dimension: job_application_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_application_id ;;
  }

  dimension: review_list_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.review_list_id ;;
  }

  dimension: reviewer_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.reviewer_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      ds_review_groups.id,
      ds_review_groups.name,
      job_applications.base_video_name,
      job_applications.id,
      review_lists.id,
      review_lists.name,
      reviewers.first_name,
      reviewers.id,
      reviewers.last_name,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name,
      criteria_review_answers.count
    ]
  }
}
