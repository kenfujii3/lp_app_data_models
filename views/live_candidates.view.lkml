view: live_candidates {
  sql_table_name: willh.live_candidates ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: badge {
    type: string
    sql: ${TABLE}.badge ;;
  }

  dimension: channel_hash {
    type: string
    sql: ${TABLE}.channel_hash ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}.first_name ;;
  }

  dimension: host_interviewer_id {
    type: number
    sql: ${TABLE}.host_interviewer_id ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}.last_name ;;
  }

  dimension: last_recording_index {
    type: number
    sql: ${TABLE}.last_recording_index ;;
  }

  dimension: live_interview_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_interview_id ;;
  }

  dimension: live_pool_id {
    type: string
    sql: ${TABLE}.live_pool_id ;;
  }

  dimension: meeting_point_id {
    type: string
    sql: ${TABLE}.meeting_point_id ;;
  }

  dimension_group: original_booking {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.original_booking ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: telephone {
    type: string
    sql: ${TABLE}.telephone ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      first_name,
      last_name,
      live_interviews.id,
      live_candidate_recordings.count,
      live_interview_logs.count,
      live_interview_slots.count,
      live_scheduled_answers.count,
      live_scheduled_comments.count
    ]
  }
}
