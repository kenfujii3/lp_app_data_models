view: account_videos {
  sql_table_name: willh.account_videos ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: assessment_closing_video {
    type: string
    sql: ${TABLE}.assessment_closing_video ;;
  }

  dimension: assessment_company_video {
    type: string
    sql: ${TABLE}.assessment_company_video ;;
  }

  dimension: assessment_intro_video {
    type: string
    sql: ${TABLE}.assessment_intro_video ;;
  }

  dimension: assessment_practice_video {
    type: string
    sql: ${TABLE}.assessment_practice_video ;;
  }

  dimension: assessment_setup1_image {
    type: string
    sql: ${TABLE}.assessment_setup1_image ;;
  }

  dimension: assessment_setup2_image {
    type: string
    sql: ${TABLE}.assessment_setup2_image ;;
  }

  dimension: assessment_setup3_image {
    type: string
    sql: ${TABLE}.assessment_setup3_image ;;
  }

  dimension: assessment_tips1_image {
    type: string
    sql: ${TABLE}.assessment_tips1_image ;;
  }

  dimension: assessment_tips2_image {
    type: string
    sql: ${TABLE}.assessment_tips2_image ;;
  }

  dimension: assessment_tips3_image {
    type: string
    sql: ${TABLE}.assessment_tips3_image ;;
  }

  dimension: assessment_tips4_image {
    type: string
    sql: ${TABLE}.assessment_tips4_image ;;
  }

  dimension: assessment_tips_video {
    type: string
    sql: ${TABLE}.assessment_tips_video ;;
  }

  dimension: closing_video {
    type: string
    sql: ${TABLE}.closing_video ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: intro1_video {
    type: string
    sql: ${TABLE}.intro1_video ;;
  }

  dimension: intro2_video {
    type: string
    sql: ${TABLE}.intro2_video ;;
  }

  dimension: live_assessment_company_video {
    type: string
    sql: ${TABLE}.live_assessment_company_video ;;
  }

  dimension: locale {
    type: string
    sql: ${TABLE}.locale ;;
  }

  dimension: practice_video {
    type: string
    sql: ${TABLE}.practice_video ;;
  }

  dimension: screening_video {
    type: string
    sql: ${TABLE}.screening_video ;;
  }

  dimension: tips_video {
    type: string
    sql: ${TABLE}.tips_video ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, accounts.maybe_field_name, accounts.name, accounts.provisioner_account_id, accounts.subdomain_name]
  }
}
