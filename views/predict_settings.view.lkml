view: predict_settings {
  sql_table_name: willh.predict_settings ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension_group: model_first_deploy {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.model_first_deploy_date ;;
  }

  dimension_group: model_last_deploy {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.model_last_deploy_date ;;
  }

  dimension: predict_enabled {
    type: yesno
    sql: ${TABLE}.predict_enabled ;;
  }

  dimension: predict_mode {
    type: string
    sql: ${TABLE}.predict_mode ;;
  }

  dimension: predict_model_id {
    type: string
    sql: ${TABLE}.predict_model_id ;;
  }

  dimension: predict_model_name {
    type: string
    sql: ${TABLE}.predict_model_name ;;
  }

  dimension: root_job_id {
    type: number
    sql: ${TABLE}.root_job_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      predict_model_name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id
    ]
  }
}
