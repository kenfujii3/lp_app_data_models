view: question_video_attachments {
  sql_table_name: willh.question_video_attachments ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: question_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.question_uuid ;;
  }

  dimension: uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.uuid ;;
  }

  dimension: video_content_type {
    type: string
    sql: ${TABLE}.video_content_type ;;
  }

  dimension: video_file_name {
    type: string
    sql: ${TABLE}.video_file_name ;;
  }

  dimension: video_file_size {
    type: number
    sql: ${TABLE}.video_file_size ;;
  }

  dimension_group: video_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.video_updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, video_file_name]
  }
}
