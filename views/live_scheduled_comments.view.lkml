view: live_scheduled_comments {
  sql_table_name: willh.live_scheduled_comments ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: comments {
    type: string
    sql: ${TABLE}.comments ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: live_candidate_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_candidate_id ;;
  }

  dimension: live_employer_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_employer_id ;;
  }

  dimension: live_scheduled_session_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_scheduled_session_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      live_candidates.first_name,
      live_candidates.id,
      live_candidates.last_name,
      live_employers.first_name,
      live_employers.id,
      live_employers.last_name,
      live_scheduled_sessions.id
    ]
  }
}
