view: screening_multiple_scores {
  sql_table_name: willh.screening_multiple_scores ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: end_question {
    type: number
    sql: ${TABLE}.end_question ;;
  }

  dimension: exit_score {
    type: number
    sql: ${TABLE}.exit_score ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: start_question {
    type: number
    sql: ${TABLE}.start_question ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, jobs.country_name, jobs.industry_name, jobs.location_name, jobs.root_job_id]
  }
}
