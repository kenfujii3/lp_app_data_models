view: candidate_notifications {
  sql_table_name: willh.candidate_notifications ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: assessment_invite_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.assessment_invite_uuid ;;
  }

  dimension: assessment_session_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.assessment_session_uuid ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: notification_type {
    type: string
    sql: ${TABLE}.notification_type ;;
  }

  dimension_group: sent {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sent_at ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
