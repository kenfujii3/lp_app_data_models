view: cronofy_credentials {
  sql_table_name: willh.cronofy_credentials ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: access_token {
    type: string
    sql: ${TABLE}.access_token ;;
  }

  dimension: account_scheduling_provider_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_scheduling_provider_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: external_account_id {
    type: string
    sql: ${TABLE}.external_account_id ;;
  }

  dimension: live_assessment_reviewer_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_assessment_reviewer_id ;;
  }

  dimension: refresh_token {
    type: string
    sql: ${TABLE}.refresh_token ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, account_scheduling_providers.id, live_assessment_reviewers.first_name, live_assessment_reviewers.id, live_assessment_reviewers.last_name]
  }
}
