view: part_time_schedules {
  sql_table_name: willh.part_time_schedules ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: max_hours_per_week {
    type: number
    sql: ${TABLE}.max_hours_per_week ;;
  }

  dimension: min_hours_per_week {
    type: number
    sql: ${TABLE}.min_hours_per_week ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name, job_seekers.count, jobs.count]
  }
}
