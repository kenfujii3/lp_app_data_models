view: ds_job_infos {
  sql_table_name: willh.ds_job_infos ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: ds_city_id {
    type: number
    sql: ${TABLE}.ds_city_id ;;
  }

  dimension: ds_country_id {
    type: number
    sql: ${TABLE}.ds_country_id ;;
  }

  dimension: ds_currency_id {
    type: number
    sql: ${TABLE}.ds_currency_id ;;
  }

  dimension: ds_industry_id {
    type: number
    sql: ${TABLE}.ds_industry_id ;;
  }

  dimension: ds_position_type_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_position_type_id ;;
  }

  dimension: ds_reviewer_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_reviewer_id ;;
  }

  dimension: ds_seniority_id {
    type: number
    sql: ${TABLE}.ds_seniority_id ;;
  }

  dimension: ds_team_size_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_team_size_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: salary_level_currency {
    type: string
    sql: ${TABLE}.salary_level_currency ;;
  }

  dimension: salary_level_high {
    type: number
    sql: ${TABLE}.salary_level_high ;;
  }

  dimension: salary_level_low {
    type: number
    sql: ${TABLE}.salary_level_low ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.start_date ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      ds_position_types.id,
      ds_position_types.name,
      ds_reviewers.id,
      ds_team_sizes.id,
      ds_team_sizes.name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      ds_question_answers.count
    ]
  }
}
