view: plans {
  sql_table_name: willh.plans ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: applicants_per_month {
    type: number
    sql: ${TABLE}.applicants_per_month ;;
  }

  dimension: billing_period_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.billing_period_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: identifier {
    type: string
    sql: ${TABLE}.identifier ;;
  }

  dimension: max_applicants_per_job {
    type: number
    sql: ${TABLE}.max_applicants_per_job ;;
  }

  dimension: max_customised_content {
    type: number
    sql: ${TABLE}.max_customised_content ;;
  }

  dimension: max_full_branding {
    type: number
    sql: ${TABLE}.max_full_branding ;;
  }

  dimension: max_live_license {
    type: number
    sql: ${TABLE}.max_live_license ;;
  }

  dimension: max_published_jobs {
    type: number
    sql: ${TABLE}.max_published_jobs ;;
  }

  dimension: max_total_applicants {
    type: number
    sql: ${TABLE}.max_total_applicants ;;
  }

  dimension: max_training_sessions {
    type: number
    sql: ${TABLE}.max_training_sessions ;;
  }

  dimension: max_users {
    type: number
    sql: ${TABLE}.max_users ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: price_cents {
    type: number
    sql: ${TABLE}.price_cents ;;
  }

  dimension: trial_period_length {
    type: number
    sql: ${TABLE}.trial_period_length ;;
  }

  dimension: unlimited_interviews {
    type: yesno
    sql: ${TABLE}.unlimited_interviews ;;
  }

  dimension: unlimited_screen {
    type: yesno
    sql: ${TABLE}.unlimited_screen ;;
  }

  dimension: unlimited_share {
    type: yesno
    sql: ${TABLE}.unlimited_share ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: video_retention_period {
    type: number
    sql: ${TABLE}.video_retention_period ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name, billing_periods.id, billing_periods.name, account_plans.count]
  }
}
