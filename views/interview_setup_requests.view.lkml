view: interview_setup_requests {
  sql_table_name: willh.interview_setup_requests ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: callback_attempts {
    type: number
    sql: ${TABLE}.callback_attempts ;;
  }

  dimension: completion_message {
    type: string
    sql: ${TABLE}.completion_message ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: enable_review_criteria {
    type: yesno
    sql: ${TABLE}.enable_review_criteria ;;
  }

  dimension: enable_screening {
    type: yesno
    sql: ${TABLE}.enable_screening ;;
  }

  dimension_group: expiry {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.expiry ;;
  }

  dimension: external_css {
    type: string
    sql: ${TABLE}.external_css ;;
  }

  dimension: job_account_id {
    type: number
    sql: ${TABLE}.job_account_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: qualifications {
    type: string
    sql: ${TABLE}.qualifications ;;
  }

  dimension: request_code {
    type: string
    sql: ${TABLE}.request_code ;;
  }

  dimension: request_type {
    type: string
    sql: ${TABLE}.request_type ;;
  }

  dimension: requester_email {
    type: string
    sql: ${TABLE}.requester_email ;;
  }

  dimension: requester_first_name {
    type: string
    sql: ${TABLE}.requester_first_name ;;
  }

  dimension: requester_id {
    type: number
    sql: ${TABLE}.requester_id ;;
  }

  dimension: requester_last_name {
    type: string
    sql: ${TABLE}.requester_last_name ;;
  }

  dimension: responsibilities {
    type: string
    sql: ${TABLE}.responsibilities ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      requester_first_name,
      requester_last_name,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      integration_transaction_tokens.count
    ]
  }
}
