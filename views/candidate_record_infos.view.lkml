view: candidate_record_infos {
  sql_table_name: willh.candidate_record_infos ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: additional_profile {
    type: string
    sql: ${TABLE}.additional_profile ;;
  }

  dimension: age {
    type: number
    sql: ${TABLE}.age ;;
  }

  dimension: age_confidence {
    type: number
    sql: ${TABLE}.age_confidence ;;
  }

  dimension: birth_year {
    type: number
    sql: ${TABLE}.birth_year ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: custom_cv_content_type {
    type: string
    sql: ${TABLE}.custom_cv_content_type ;;
  }

  dimension: custom_cv_file_name {
    type: string
    sql: ${TABLE}.custom_cv_file_name ;;
  }

  dimension: custom_cv_file_size {
    type: number
    sql: ${TABLE}.custom_cv_file_size ;;
  }

  dimension_group: custom_cv_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.custom_cv_updated_at ;;
  }

  dimension: deadline_extended {
    type: yesno
    sql: ${TABLE}.deadline_extended ;;
  }

  dimension: disability {
    type: string
    sql: ${TABLE}.disability ;;
  }

  dimension: disability_description {
    type: string
    sql: ${TABLE}.disability_description ;;
  }

  dimension: ds_ethnicity_id {
    type: number
    sql: ${TABLE}.ds_ethnicity_id ;;
  }

  dimension: ds_seniority_id {
    type: number
    sql: ${TABLE}.ds_seniority_id ;;
  }

  dimension: ethnicity {
    type: string
    sql: ${TABLE}.ethnicity ;;
  }

  dimension: ethnicity_confidence {
    type: number
    sql: ${TABLE}.ethnicity_confidence ;;
  }

  dimension: first_language {
    type: string
    sql: ${TABLE}.first_language ;;
  }

  dimension: first_language_description {
    type: string
    sql: ${TABLE}.first_language_description ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: gender_confidence {
    type: number
    sql: ${TABLE}.gender_confidence ;;
  }

  dimension: gender_description {
    type: string
    sql: ${TABLE}.gender_description ;;
  }

  dimension: interview_attributes {
    type: string
    sql: ${TABLE}.interview_attributes ;;
  }

  dimension: predict_bucket {
    type: string
    sql: ${TABLE}.predict_bucket ;;
  }

  dimension: predict_opt_out {
    type: yesno
    sql: ${TABLE}.predict_opt_out ;;
  }

  dimension: predict_score {
    type: number
    sql: ${TABLE}.predict_score ;;
  }

  dimension: race {
    type: string
    sql: ${TABLE}.race ;;
  }

  dimension: race_description {
    type: string
    sql: ${TABLE}.race_description ;;
  }

  dimension: redirect_url {
    type: string
    sql: ${TABLE}.redirect_url ;;
  }

  dimension: sexual_orientation {
    type: string
    sql: ${TABLE}.sexual_orientation ;;
  }

  dimension: sexual_orientation_description {
    type: string
    sql: ${TABLE}.sexual_orientation_description ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      custom_cv_file_name,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name
    ]
  }
}
