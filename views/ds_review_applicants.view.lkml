view: ds_review_applicants {
  sql_table_name: willh.ds_review_applicants ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: ds_review_group_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_review_group_id ;;
  }

  dimension: focus {
    type: number
    sql: ${TABLE}.focus ;;
  }

  dimension: job_application_question_version_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_application_question_version_id ;;
  }

  dimension: number_of_pauses {
    type: number
    sql: ${TABLE}.number_of_pauses ;;
  }

  dimension: pause_times {
    type: string
    sql: ${TABLE}.pause_times ;;
  }

  dimension: platform {
    type: string
    sql: ${TABLE}.platform ;;
  }

  dimension: play_times {
    type: string
    sql: ${TABLE}.play_times ;;
  }

  dimension: questions_reviewed_in_session {
    type: number
    sql: ${TABLE}.questions_reviewed_in_session ;;
  }

  dimension_group: review_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.review_start_time ;;
  }

  dimension: session_id {
    type: string
    sql: ${TABLE}.session_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      ds_review_groups.id,
      ds_review_groups.name,
      job_application_question_versions.id,
      job_application_question_versions.speech_filename,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name
    ]
  }
}
