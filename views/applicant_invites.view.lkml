view: applicant_invites {
  sql_table_name: willh.applicant_invites ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: applied {
    type: yesno
    sql: ${TABLE}.applied ;;
  }

  dimension_group: applied {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.applied_at ;;
  }

  dimension: apply_code {
    type: string
    sql: ${TABLE}.apply_code ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: custom_invite_id {
    type: string
    sql: ${TABLE}.custom_invite_id ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: formatted_mobile {
    type: string
    sql: ${TABLE}.formatted_mobile ;;
  }

  dimension: integration_application_id {
    type: string
    sql: ${TABLE}.integration_application_id ;;
  }

  dimension: integration_interview_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.integration_interview_id ;;
  }

  dimension: integration_requester {
    type: string
    sql: ${TABLE}.integration_requester ;;
  }

  dimension: integration_url {
    type: string
    sql: ${TABLE}.integration_url ;;
  }

  dimension: integration_user_id {
    type: string
    sql: ${TABLE}.integration_user_id ;;
  }

  dimension: invitor_id {
    type: number
    sql: ${TABLE}.invitor_id ;;
  }

  dimension: job_application_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_application_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: link_opened {
    type: yesno
    sql: ${TABLE}.link_opened ;;
  }

  dimension_group: link_opened {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.link_opened_at ;;
  }

  dimension: message {
    type: string
    sql: ${TABLE}.message ;;
  }

  dimension: mobile_message {
    type: string
    sql: ${TABLE}.mobile_message ;;
  }

  dimension: original_mobile {
    type: string
    sql: ${TABLE}.original_mobile ;;
  }

  dimension: seamless_code {
    type: string
    sql: ${TABLE}.seamless_code ;;
  }

  dimension: seamless_login {
    type: yesno
    sql: ${TABLE}.seamless_login ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: unsubscribe_code {
    type: string
    sql: ${TABLE}.unsubscribe_code ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      integration_interviews.id,
      job_applications.base_video_name,
      job_applications.id,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      applicant_invite_custom_ids.count,
      candidate_records.count
    ]
  }
}
