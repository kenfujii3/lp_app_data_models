view: accounts {
  sql_table_name: willh.accounts ;;
  drill_fields: [provisioner_account_id]

  dimension: provisioner_account_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.provisioner_account_id ;;
  }

  dimension: active {
    type: yesno
    sql: ${TABLE}.active ;;
  }

  dimension: admin_id {
    type: number
    sql: ${TABLE}.admin_id ;;
  }

  dimension: allow_change_account_timezone {
    type: yesno
    sql: ${TABLE}.allow_change_account_timezone ;;
  }

  dimension: allow_change_employer_language {
    type: yesno
    sql: ${TABLE}.allow_change_employer_language ;;
  }

  dimension: allow_employer_interview_language {
    type: string
    sql: ${TABLE}.allow_employer_interview_language ;;
  }

  dimension: api_force_login {
    type: string
    sql: ${TABLE}.api_force_login ;;
  }

  dimension: api_key {
    type: string
    sql: ${TABLE}.api_key ;;
  }

  dimension: api_timeout {
    type: number
    sql: ${TABLE}.api_timeout ;;
  }

  dimension: api_user_creation {
    type: string
    sql: ${TABLE}.api_user_creation ;;
  }

  dimension: auto_forward_candidate_msg {
    type: yesno
    sql: ${TABLE}.auto_forward_candidate_msg ;;
  }

  dimension: auto_forward_employer_email {
    type: string
    sql: ${TABLE}.auto_forward_employer_email ;;
  }

  dimension: banner_url {
    type: string
    sql: ${TABLE}.banner_url ;;
  }

  dimension: blur {
    type: yesno
    sql: ${TABLE}.blur ;;
  }

  dimension: callback_include_link {
    type: yesno
    sql: ${TABLE}.callback_include_link ;;
  }

  dimension: callback_include_shortlist_reject {
    type: yesno
    sql: ${TABLE}.callback_include_shortlist_reject ;;
  }

  dimension: callback_job_setup {
    type: yesno
    sql: ${TABLE}.callback_job_setup ;;
  }

  dimension: callback_json {
    type: yesno
    sql: ${TABLE}.callback_json ;;
  }

  dimension: callback_screening_status {
    type: yesno
    sql: ${TABLE}.callback_screening_status ;;
  }

  dimension: callback_skip_certificate_check {
    type: yesno
    sql: ${TABLE}.callback_skip_certificate_check ;;
  }

  dimension: callback_url {
    type: string
    sql: ${TABLE}.callback_url ;;
  }

  dimension: company_id {
    type: number
    sql: ${TABLE}.company_id ;;
  }

  dimension: contingency_max {
    type: number
    sql: ${TABLE}.contingency_max ;;
  }

  dimension: contingency_min {
    type: number
    sql: ${TABLE}.contingency_min ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: dashboard_after_interview {
    type: yesno
    sql: ${TABLE}.dashboard_after_interview ;;
  }

  dimension: default_account_language {
    type: string
    sql: ${TABLE}.default_account_language ;;
  }

  dimension: default_account_timezone {
    type: string
    sql: ${TABLE}.default_account_timezone ;;
  }

  dimension: default_country_code {
    type: string
    sql: ${TABLE}.default_country_code ;;
  }

  dimension: default_intro_video_file {
    type: string
    sql: ${TABLE}.default_intro_video_file ;;
  }

  dimension: default_language {
    type: string
    sql: ${TABLE}.default_language ;;
  }

  dimension: default_reviewer_id {
    type: number
    sql: ${TABLE}.default_reviewer_id ;;
  }

  dimension: default_video_file_2 {
    type: string
    sql: ${TABLE}.default_video_file_2 ;;
  }

  dimension: default_video_file_3 {
    type: string
    sql: ${TABLE}.default_video_file_3 ;;
  }

  dimension: disable_all_rejection_comms {
    type: yesno
    sql: ${TABLE}.disable_all_rejection_comms ;;
  }

  dimension: disable_cv_upload {
    type: yesno
    sql: ${TABLE}.disable_cv_upload ;;
  }

  dimension: disable_login_caching {
    type: yesno
    sql: ${TABLE}.disable_login_caching ;;
  }

  dimension: display_candidate_email {
    type: yesno
    sql: ${TABLE}.display_candidate_email ;;
  }

  dimension: duration_max {
    type: number
    sql: ${TABLE}.duration_max ;;
  }

  dimension: duration_min {
    type: number
    sql: ${TABLE}.duration_min ;;
  }

  dimension: email_applicants {
    type: yesno
    sql: ${TABLE}.email_applicants ;;
  }

  dimension: email_employers {
    type: yesno
    sql: ${TABLE}.email_employers ;;
  }

  dimension: enable_additional_profile {
    type: yesno
    sql: ${TABLE}.enable_additional_profile ;;
  }

  dimension: enable_analytics {
    type: yesno
    sql: ${TABLE}.enable_analytics ;;
  }

  dimension: enable_api {
    type: yesno
    sql: ${TABLE}.enable_api ;;
  }

  dimension: enable_api_logging {
    type: yesno
    sql: ${TABLE}.enable_api_logging ;;
  }

  dimension: enable_assessment_v2 {
    type: yesno
    sql: ${TABLE}.enable_assessment_v2 ;;
  }

  dimension: enable_batch_screening_questions {
    type: yesno
    sql: ${TABLE}.enable_batch_screening_questions ;;
  }

  dimension: enable_capp_rating {
    type: yesno
    sql: ${TABLE}.enable_capp_rating ;;
  }

  dimension: enable_capp_rating_percentage_workaround {
    type: yesno
    sql: ${TABLE}.enable_capp_rating_percentage_workaround ;;
  }

  dimension: enable_custom_cv {
    type: yesno
    sql: ${TABLE}.enable_custom_cv ;;
  }

  dimension: enable_email_reminder_final {
    type: yesno
    sql: ${TABLE}.enable_email_reminder_final ;;
  }

  dimension: enable_image_answers {
    type: yesno
    sql: ${TABLE}.enable_image_answers ;;
  }

  dimension: enable_increase_filesize_and_filetypes {
    type: yesno
    sql: ${TABLE}.enable_increase_filesize_and_filetypes ;;
  }

  dimension: enable_indi_screening {
    type: yesno
    sql: ${TABLE}.enable_indi_screening ;;
  }

  dimension: enable_live_interviews {
    type: yesno
    sql: ${TABLE}.enable_live_interviews ;;
  }

  dimension: enable_live_recording {
    type: yesno
    sql: ${TABLE}.enable_live_recording ;;
  }

  dimension: enable_localization {
    type: yesno
    sql: ${TABLE}.enable_localization ;;
  }

  dimension: enable_logo_link {
    type: yesno
    sql: ${TABLE}.enable_logo_link ;;
  }

  dimension: enable_lpr_validate {
    type: yesno
    sql: ${TABLE}.enable_lpr_validate ;;
  }

  dimension: enable_mobile_android_interview {
    type: yesno
    sql: ${TABLE}.enable_mobile_android_interview ;;
  }

  dimension: enable_mobile_employer_app {
    type: yesno
    sql: ${TABLE}.enable_mobile_employer_app ;;
  }

  dimension: enable_mobile_interview {
    type: yesno
    sql: ${TABLE}.enable_mobile_interview ;;
  }

  dimension: enable_new_live {
    type: yesno
    sql: ${TABLE}.enable_new_live ;;
  }

  dimension: enable_not_completed_feedback {
    type: yesno
    sql: ${TABLE}.enable_not_completed_feedback ;;
  }

  dimension: enable_params_sanitation {
    type: yesno
    sql: ${TABLE}.enable_params_sanitation ;;
  }

  dimension: enable_provisioning_api {
    type: yesno
    sql: ${TABLE}.enable_provisioning_api ;;
  }

  dimension: enable_recorder_reconnect {
    type: yesno
    sql: ${TABLE}.enable_recorder_reconnect ;;
  }

  dimension: enable_request_interview_retake {
    type: yesno
    sql: ${TABLE}.enable_request_interview_retake ;;
  }

  dimension: enable_screening {
    type: yesno
    sql: ${TABLE}.enable_screening ;;
  }

  dimension: enable_screening_personal_questions {
    type: yesno
    sql: ${TABLE}.enable_screening_personal_questions ;;
  }

  dimension: enable_seamless_invite {
    type: yesno
    sql: ${TABLE}.enable_seamless_invite ;;
  }

  dimension: enable_sharing_centre {
    type: yesno
    sql: ${TABLE}.enable_sharing_centre ;;
  }

  dimension: enable_single_reviewer {
    type: yesno
    sql: ${TABLE}.enable_single_reviewer ;;
  }

  dimension: enable_sms {
    type: yesno
    sql: ${TABLE}.enable_sms ;;
  }

  dimension: enable_validate_integrations {
    type: yesno
    sql: ${TABLE}.enable_validate_integrations ;;
  }

  dimension: enable_validate_testing {
    type: yesno
    sql: ${TABLE}.enable_validate_testing ;;
  }

  dimension: expand_applicant_list {
    type: yesno
    sql: ${TABLE}.expand_applicant_list ;;
  }

  dimension: expand_reviewer_list {
    type: yesno
    sql: ${TABLE}.expand_reviewer_list ;;
  }

  dimension: generate_share_link_on_new_applications {
    type: yesno
    sql: ${TABLE}.generate_share_link_on_new_applications ;;
  }

  dimension: grab_n_go {
    type: yesno
    sql: ${TABLE}.grab_n_go ;;
  }

  dimension: grab_n_go_blocked {
    type: yesno
    sql: ${TABLE}.grab_n_go_blocked ;;
  }

  dimension: id {
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: integration_account {
    type: yesno
    sql: ${TABLE}.integration_account ;;
  }

  dimension: integration_email {
    type: string
    sql: ${TABLE}.integration_email ;;
  }

  dimension: interview_reminder_days {
    type: string
    sql: ${TABLE}.interview_reminder_days ;;
  }

  dimension: interview_reminder_days_subsequent {
    type: string
    sql: ${TABLE}.interview_reminder_days_subsequent ;;
  }

  dimension: large_video_player {
    type: yesno
    sql: ${TABLE}.large_video_player ;;
  }

  dimension: logo_url {
    type: string
    sql: ${TABLE}.logo_url ;;
  }

  dimension: lp_admin_account_level_setting_note {
    type: string
    sql: ${TABLE}.lp_admin_account_level_setting_note ;;
  }

  dimension: lp_admin_allow_deadline_change {
    type: yesno
    sql: ${TABLE}.lp_admin_allow_deadline_change ;;
  }

  dimension: lp_admin_allow_reset {
    type: yesno
    sql: ${TABLE}.lp_admin_allow_reset ;;
  }

  dimension: lp_admin_allow_retake {
    type: yesno
    sql: ${TABLE}.lp_admin_allow_retake ;;
  }

  dimension: lp_admin_message_to_employer {
    type: yesno
    sql: ${TABLE}.lp_admin_message_to_employer ;;
  }

  dimension: maybe_bucket {
    type: yesno
    sql: ${TABLE}.maybe_bucket ;;
  }

  dimension: maybe_field_name {
    type: string
    sql: ${TABLE}.maybe_field_name ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: no_of_questions_limit {
    type: number
    sql: ${TABLE}.no_of_questions_limit ;;
  }

  dimension: parent_id {
    type: number
    sql: ${TABLE}.parent_id ;;
  }

  dimension: preparation_time_limits {
    type: string
    sql: ${TABLE}.preparation_time_limits ;;
  }

  dimension: provisioning_api_key {
    type: string
    sql: ${TABLE}.provisioning_api_key ;;
  }

  dimension: qa_enable_messaging {
    type: yesno
    sql: ${TABLE}.qa_enable_messaging ;;
  }

  dimension: qa_percentage_overlap {
    type: string
    sql: ${TABLE}.qa_percentage_overlap ;;
  }

  dimension: question_time_limits {
    type: string
    sql: ${TABLE}.question_time_limits ;;
  }

  dimension: salesforce_id {
    type: string
    sql: ${TABLE}.salesforce_id ;;
  }

  dimension: screening_character_count {
    type: string
    sql: ${TABLE}.screening_character_count ;;
  }

  dimension: send_invite_support_email {
    type: yesno
    sql: ${TABLE}.send_invite_support_email ;;
  }

  dimension: share_enable_branding {
    type: yesno
    sql: ${TABLE}.share_enable_branding ;;
  }

  dimension: share_enable_unlimited_views {
    type: yesno
    sql: ${TABLE}.share_enable_unlimited_views ;;
  }

  dimension: show_powered_by_link {
    type: yesno
    sql: ${TABLE}.show_powered_by_link ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: status_frequency {
    type: string
    sql: ${TABLE}.status_frequency ;;
  }

  dimension: subaccount {
    type: yesno
    sql: ${TABLE}.subaccount ;;
  }

  dimension: subdomain_limit_access {
    type: yesno
    sql: ${TABLE}.subdomain_limit_access ;;
  }

  dimension: subdomain_name {
    type: string
    sql: ${TABLE}.subdomain_name ;;
  }

  dimension: superaccount {
    type: yesno
    sql: ${TABLE}.superaccount ;;
  }

  dimension: transcription_cleanup {
    type: yesno
    sql: ${TABLE}.transcription_cleanup ;;
  }

  dimension: transcription_enable {
    type: yesno
    sql: ${TABLE}.transcription_enable ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: use_intro_video {
    type: yesno
    sql: ${TABLE}.use_intro_video ;;
  }

  dimension: videos_on_top {
    type: yesno
    sql: ${TABLE}.videos_on_top ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      provisioner_account_id,
      maybe_field_name,
      name,
      subdomain_name,
      account_available_stages.count,
      account_candidate_feedbacks.count,
      account_candidates.count,
      account_colors.count,
      account_emails.count,
      account_images.count,
      account_integrations.count,
      account_interview_contents.count,
      account_live_providers.count,
      account_mobiles.count,
      account_notes.count,
      account_plans.count,
      account_scheduling_providers.count,
      account_settings.count,
      account_tags.count,
      account_users.count,
      account_users_invites.count,
      account_videos.count,
      account_whitelabels.count,
      activity_logs.count,
      admin_reports.count,
      api_logs.count,
      applicant_invite_custom_ids.count,
      applicant_invite_reminder_histories.count,
      candidate_records.count,
      candidate_status_reports.count,
      client_roles.count,
      collaborator_invites.count,
      custom_candidate_ids.count,
      data_policy_logs.count,
      ds_reviewers.count,
      hiring_teams.count,
      integration_transactions.count,
      interview_setup_requests.count,
      interview_share_invites.count,
      job_collaborators.count,
      jobs.count,
      live_assessments.count,
      live_colleagues.count,
      live_interviews.count,
      live_invites_csv_uploads.count,
      partners.count,
      screening_library_categories.count,
      screening_library_questions.count,
      screening_sessions.count,
      security_password_controls.count,
      trial_user_invites.count,
      user_account_changes.count,
      videos.count
    ]
  }
}
