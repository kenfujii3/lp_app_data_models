view: screening_score_ranges {
  sql_table_name: willh.screening_score_ranges ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: max_score {
    type: number
    sql: ${TABLE}.max_score ;;
  }

  dimension: message {
    type: string
    sql: ${TABLE}.message ;;
  }

  dimension: min_score {
    type: number
    sql: ${TABLE}.min_score ;;
  }

  dimension: score_attachment_content_type {
    type: string
    sql: ${TABLE}.score_attachment_content_type ;;
  }

  dimension: score_attachment_file_name {
    type: string
    sql: ${TABLE}.score_attachment_file_name ;;
  }

  dimension: score_attachment_file_size {
    type: number
    sql: ${TABLE}.score_attachment_file_size ;;
  }

  dimension_group: score_attachment_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.score_attachment_updated_at ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      score_attachment_file_name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id
    ]
  }
}
