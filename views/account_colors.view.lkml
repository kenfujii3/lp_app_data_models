view: account_colors {
  sql_table_name: willh.account_colors ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: background {
    type: string
    sql: ${TABLE}.background ;;
  }

  dimension: background_header {
    type: string
    sql: ${TABLE}.background_header ;;
  }

  dimension: background_v2 {
    type: string
    sql: ${TABLE}.background_v2 ;;
  }

  dimension: border {
    type: string
    sql: ${TABLE}.border ;;
  }

  dimension: button {
    type: string
    sql: ${TABLE}.button ;;
  }

  dimension: button_disabled_v2 {
    type: string
    sql: ${TABLE}.button_disabled_v2 ;;
  }

  dimension: button_gradient {
    type: string
    sql: ${TABLE}.button_gradient ;;
  }

  dimension: button_gradient_style {
    type: string
    sql: ${TABLE}.button_gradient_style ;;
  }

  dimension: button_gradient_style_v2 {
    type: string
    sql: ${TABLE}.button_gradient_style_v2 ;;
  }

  dimension: button_gradient_v2 {
    type: string
    sql: ${TABLE}.button_gradient_v2 ;;
  }

  dimension: button_hover {
    type: string
    sql: ${TABLE}.button_hover ;;
  }

  dimension: button_hover_gradient_style_v2 {
    type: string
    sql: ${TABLE}.button_hover_gradient_style_v2 ;;
  }

  dimension: button_hover_gradient_v2 {
    type: string
    sql: ${TABLE}.button_hover_gradient_v2 ;;
  }

  dimension: button_hover_v2 {
    type: string
    sql: ${TABLE}.button_hover_v2 ;;
  }

  dimension: button_play {
    type: string
    sql: ${TABLE}.button_play ;;
  }

  dimension: button_play_gradient {
    type: string
    sql: ${TABLE}.button_play_gradient ;;
  }

  dimension: button_radius {
    type: string
    sql: ${TABLE}.button_radius ;;
  }

  dimension: button_text {
    type: string
    sql: ${TABLE}.button_text ;;
  }

  dimension: button_text_transform {
    type: string
    sql: ${TABLE}.button_text_transform ;;
  }

  dimension: button_text_v2 {
    type: string
    sql: ${TABLE}.button_text_v2 ;;
  }

  dimension: button_v2 {
    type: string
    sql: ${TABLE}.button_v2 ;;
  }

  dimension: candidate_feedback_footer_colour {
    type: string
    sql: ${TABLE}.candidate_feedback_footer_colour ;;
  }

  dimension: candidate_feedback_title_colour {
    type: string
    sql: ${TABLE}.candidate_feedback_title_colour ;;
  }

  dimension: circle_color {
    type: string
    sql: ${TABLE}.circle_color ;;
  }

  dimension: content_box_text_v2 {
    type: string
    sql: ${TABLE}.content_box_text_v2 ;;
  }

  dimension: content_box_v2 {
    type: string
    sql: ${TABLE}.content_box_v2 ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: header_text_v2 {
    type: string
    sql: ${TABLE}.header_text_v2 ;;
  }

  dimension: helpdesk_text_hover_v2 {
    type: string
    sql: ${TABLE}.helpdesk_text_hover_v2 ;;
  }

  dimension: helpdesk_text_v2 {
    type: string
    sql: ${TABLE}.helpdesk_text_v2 ;;
  }

  dimension: interview_v2_responsibilities_body_background {
    type: string
    sql: ${TABLE}.interview_v2_responsibilities_body_background ;;
  }

  dimension: interview_v2_responsibilities_body_text {
    type: string
    sql: ${TABLE}.interview_v2_responsibilities_body_text ;;
  }

  dimension: interview_v2_responsibilities_header_background {
    type: string
    sql: ${TABLE}.interview_v2_responsibilities_header_background ;;
  }

  dimension: interview_v2_responsibilities_header_text {
    type: string
    sql: ${TABLE}.interview_v2_responsibilities_header_text ;;
  }

  dimension: landing_instructions {
    type: string
    sql: ${TABLE}.landing_instructions ;;
  }

  dimension: link_hover {
    type: string
    sql: ${TABLE}.link_hover ;;
  }

  dimension: link_text_hover_v2 {
    type: string
    sql: ${TABLE}.link_text_hover_v2 ;;
  }

  dimension: link_text_v2 {
    type: string
    sql: ${TABLE}.link_text_v2 ;;
  }

  dimension: links {
    type: string
    sql: ${TABLE}.links ;;
  }

  dimension: live_background {
    type: string
    sql: ${TABLE}.live_background ;;
  }

  dimension: live_background_header {
    type: string
    sql: ${TABLE}.live_background_header ;;
  }

  dimension: live_button {
    type: string
    sql: ${TABLE}.live_button ;;
  }

  dimension: live_button_gradient {
    type: string
    sql: ${TABLE}.live_button_gradient ;;
  }

  dimension: live_button_hover {
    type: string
    sql: ${TABLE}.live_button_hover ;;
  }

  dimension: live_button_radius {
    type: string
    sql: ${TABLE}.live_button_radius ;;
  }

  dimension: live_button_text {
    type: string
    sql: ${TABLE}.live_button_text ;;
  }

  dimension: live_link_hover {
    type: string
    sql: ${TABLE}.live_link_hover ;;
  }

  dimension: live_links {
    type: string
    sql: ${TABLE}.live_links ;;
  }

  dimension: live_text_body {
    type: string
    sql: ${TABLE}.live_text_body ;;
  }

  dimension: live_text_heading {
    type: string
    sql: ${TABLE}.live_text_heading ;;
  }

  dimension: live_zendesk_link_color {
    type: string
    sql: ${TABLE}.live_zendesk_link_color ;;
  }

  dimension: live_zendesk_link_hover {
    type: string
    sql: ${TABLE}.live_zendesk_link_hover ;;
  }

  dimension: logo_align {
    type: string
    sql: ${TABLE}.logo_align ;;
  }

  dimension: logo_top {
    type: string
    sql: ${TABLE}.logo_top ;;
  }

  dimension: mobile_logo_header_colour {
    type: string
    sql: ${TABLE}.mobile_logo_header_colour ;;
  }

  dimension: mobile_record_button {
    type: string
    sql: ${TABLE}.mobile_record_button ;;
  }

  dimension: mobile_stop_recording_button {
    type: string
    sql: ${TABLE}.mobile_stop_recording_button ;;
  }

  dimension: mobile_taskbar_colour {
    type: string
    sql: ${TABLE}.mobile_taskbar_colour ;;
  }

  dimension: navbar_colour {
    type: string
    sql: ${TABLE}.navbar_colour ;;
  }

  dimension: navbar_opacity_v2 {
    type: string
    sql: ${TABLE}.navbar_opacity_v2 ;;
  }

  dimension: outside_box_header_text_v2 {
    type: string
    sql: ${TABLE}.outside_box_header_text_v2 ;;
  }

  dimension: privacy_link_hover_text {
    type: string
    sql: ${TABLE}.privacy_link_hover_text ;;
  }

  dimension: privacy_link_text {
    type: string
    sql: ${TABLE}.privacy_link_text ;;
  }

  dimension: scheduler_button {
    type: string
    sql: ${TABLE}.scheduler_button ;;
  }

  dimension: scheduler_button_slots {
    type: string
    sql: ${TABLE}.scheduler_button_slots ;;
  }

  dimension: screening_background_colour {
    type: string
    sql: ${TABLE}.screening_background_colour ;;
  }

  dimension: screening_button_bar_colour {
    type: string
    sql: ${TABLE}.screening_button_bar_colour ;;
  }

  dimension: screening_button_colour {
    type: string
    sql: ${TABLE}.screening_button_colour ;;
  }

  dimension: screening_button_gradient_colour {
    type: string
    sql: ${TABLE}.screening_button_gradient_colour ;;
  }

  dimension: screening_button_hover_colour {
    type: string
    sql: ${TABLE}.screening_button_hover_colour ;;
  }

  dimension: screening_button_roundness {
    type: string
    sql: ${TABLE}.screening_button_roundness ;;
  }

  dimension: screening_button_text_colour {
    type: string
    sql: ${TABLE}.screening_button_text_colour ;;
  }

  dimension: screening_circle_color {
    type: string
    sql: ${TABLE}.screening_circle_color ;;
  }

  dimension: screening_content_body_colour {
    type: string
    sql: ${TABLE}.screening_content_body_colour ;;
  }

  dimension: screening_header_colour {
    type: string
    sql: ${TABLE}.screening_header_colour ;;
  }

  dimension: screening_header_with_logo_height {
    type: string
    sql: ${TABLE}.screening_header_with_logo_height ;;
  }

  dimension: screening_link_colour {
    type: string
    sql: ${TABLE}.screening_link_colour ;;
  }

  dimension: screening_link_hover_colour {
    type: string
    sql: ${TABLE}.screening_link_hover_colour ;;
  }

  dimension: screening_logo_positioning {
    type: string
    sql: ${TABLE}.screening_logo_positioning ;;
  }

  dimension: screening_logo_top_spacing {
    type: string
    sql: ${TABLE}.screening_logo_top_spacing ;;
  }

  dimension: screening_subtitle_colour {
    type: string
    sql: ${TABLE}.screening_subtitle_colour ;;
  }

  dimension: screening_support_link_colour {
    type: string
    sql: ${TABLE}.screening_support_link_colour ;;
  }

  dimension: screening_support_link_hover_colour {
    type: string
    sql: ${TABLE}.screening_support_link_hover_colour ;;
  }

  dimension: screening_support_link_positioning {
    type: string
    sql: ${TABLE}.screening_support_link_positioning ;;
  }

  dimension: screening_text_colour {
    type: string
    sql: ${TABLE}.screening_text_colour ;;
  }

  dimension: screening_title_colour {
    type: string
    sql: ${TABLE}.screening_title_colour ;;
  }

  dimension: screening_v2_background {
    type: string
    sql: ${TABLE}.screening_v2_background ;;
  }

  dimension: screening_v2_best_worst_response_holder {
    type: string
    sql: ${TABLE}.screening_v2_best_worst_response_holder ;;
  }

  dimension: screening_v2_best_worst_response_hover {
    type: string
    sql: ${TABLE}.screening_v2_best_worst_response_hover ;;
  }

  dimension: screening_v2_best_worst_response_selected {
    type: string
    sql: ${TABLE}.screening_v2_best_worst_response_selected ;;
  }

  dimension: screening_v2_best_worst_table_header {
    type: string
    sql: ${TABLE}.screening_v2_best_worst_table_header ;;
  }

  dimension: screening_v2_best_worst_text {
    type: string
    sql: ${TABLE}.screening_v2_best_worst_text ;;
  }

  dimension: screening_v2_button {
    type: string
    sql: ${TABLE}.screening_v2_button ;;
  }

  dimension: screening_v2_button_gradient_lighter {
    type: string
    sql: ${TABLE}.screening_v2_button_gradient_lighter ;;
  }

  dimension: screening_v2_button_gradient_style {
    type: string
    sql: ${TABLE}.screening_v2_button_gradient_style ;;
  }

  dimension: screening_v2_button_hover {
    type: string
    sql: ${TABLE}.screening_v2_button_hover ;;
  }

  dimension: screening_v2_button_hover_gradient_lighter {
    type: string
    sql: ${TABLE}.screening_v2_button_hover_gradient_lighter ;;
  }

  dimension: screening_v2_button_hover_gradient_style {
    type: string
    sql: ${TABLE}.screening_v2_button_hover_gradient_style ;;
  }

  dimension: screening_v2_button_text {
    type: string
    sql: ${TABLE}.screening_v2_button_text ;;
  }

  dimension: screening_v2_content_box_background {
    type: string
    sql: ${TABLE}.screening_v2_content_box_background ;;
  }

  dimension: screening_v2_content_box_text {
    type: string
    sql: ${TABLE}.screening_v2_content_box_text ;;
  }

  dimension: screening_v2_disabled_button {
    type: string
    sql: ${TABLE}.screening_v2_disabled_button ;;
  }

  dimension: screening_v2_dropdown_selected_answer {
    type: string
    sql: ${TABLE}.screening_v2_dropdown_selected_answer ;;
  }

  dimension: screening_v2_header_text {
    type: string
    sql: ${TABLE}.screening_v2_header_text ;;
  }

  dimension: screening_v2_helpdesk_hover_text {
    type: string
    sql: ${TABLE}.screening_v2_helpdesk_hover_text ;;
  }

  dimension: screening_v2_helpdesk_text {
    type: string
    sql: ${TABLE}.screening_v2_helpdesk_text ;;
  }

  dimension: screening_v2_landing_page_instructions {
    type: string
    sql: ${TABLE}.screening_v2_landing_page_instructions ;;
  }

  dimension: screening_v2_link_hover_text {
    type: string
    sql: ${TABLE}.screening_v2_link_hover_text ;;
  }

  dimension: screening_v2_link_text {
    type: string
    sql: ${TABLE}.screening_v2_link_text ;;
  }

  dimension: screening_v2_media_background_colour {
    type: string
    sql: ${TABLE}.screening_v2_media_background_colour ;;
  }

  dimension: screening_v2_navbar {
    type: string
    sql: ${TABLE}.screening_v2_navbar ;;
  }

  dimension: screening_v2_navbar_opacity {
    type: string
    sql: ${TABLE}.screening_v2_navbar_opacity ;;
  }

  dimension: screening_v2_outside_box_header_text {
    type: string
    sql: ${TABLE}.screening_v2_outside_box_header_text ;;
  }

  dimension: screening_v2_response_hover_color {
    type: string
    sql: ${TABLE}.screening_v2_response_hover_color ;;
  }

  dimension: screening_v2_response_selected_color {
    type: string
    sql: ${TABLE}.screening_v2_response_selected_color ;;
  }

  dimension: screening_v2_responsibilities_body_background {
    type: string
    sql: ${TABLE}.screening_v2_responsibilities_body_background ;;
  }

  dimension: screening_v2_responsibilities_body_text {
    type: string
    sql: ${TABLE}.screening_v2_responsibilities_body_text ;;
  }

  dimension: screening_v2_responsibilities_header_background {
    type: string
    sql: ${TABLE}.screening_v2_responsibilities_header_background ;;
  }

  dimension: screening_v2_responsibilities_header_text {
    type: string
    sql: ${TABLE}.screening_v2_responsibilities_header_text ;;
  }

  dimension: screening_v2_step_text {
    type: string
    sql: ${TABLE}.screening_v2_step_text ;;
  }

  dimension: screening_v2_subheader_text {
    type: string
    sql: ${TABLE}.screening_v2_subheader_text ;;
  }

  dimension: screening_v2_text_input_field_glow {
    type: string
    sql: ${TABLE}.screening_v2_text_input_field_glow ;;
  }

  dimension: step_text {
    type: string
    sql: ${TABLE}.step_text ;;
  }

  dimension: subheader_text_v2 {
    type: string
    sql: ${TABLE}.subheader_text_v2 ;;
  }

  dimension: terms_link_color {
    type: string
    sql: ${TABLE}.terms_link_color ;;
  }

  dimension: terms_link_hover_color {
    type: string
    sql: ${TABLE}.terms_link_hover_color ;;
  }

  dimension: text_body {
    type: string
    sql: ${TABLE}.text_body ;;
  }

  dimension: text_heading {
    type: string
    sql: ${TABLE}.text_heading ;;
  }

  dimension: text_input_field_glow_color {
    type: string
    sql: ${TABLE}.text_input_field_glow_color ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: zendesk_link_color {
    type: string
    sql: ${TABLE}.zendesk_link_color ;;
  }

  dimension: zendesk_link_hover {
    type: string
    sql: ${TABLE}.zendesk_link_hover ;;
  }

  dimension: zendesk_link_position {
    type: string
    sql: ${TABLE}.zendesk_link_position ;;
  }

  dimension: zendesk_link_text {
    type: string
    sql: ${TABLE}.zendesk_link_text ;;
  }

  measure: count {
    type: count
    drill_fields: [id, accounts.maybe_field_name, accounts.name, accounts.provisioner_account_id, accounts.subdomain_name]
  }
}
