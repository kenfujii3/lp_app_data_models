view: jobs {
  sql_table_name: willh.jobs ;;
  drill_fields: [root_job_id]

  dimension: root_job_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.root_job_id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: apply_code {
    type: string
    sql: ${TABLE}.apply_code ;;
  }

  dimension: archived {
    type: yesno
    sql: ${TABLE}.archived ;;
  }

  dimension: cached_slug {
    type: string
    sql: ${TABLE}.cached_slug ;;
  }

  dimension: clone_job_id {
    type: number
    sql: ${TABLE}.clone_job_id ;;
  }

  dimension: closed_via_expired_trial_end_date {
    type: yesno
    sql: ${TABLE}.closed_via_expired_trial_end_date ;;
  }

  dimension: comments {
    type: string
    sql: ${TABLE}.comments ;;
  }

  dimension: country_name {
    type: string
    sql: ${TABLE}.country_name ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: degree_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.degree_id ;;
  }

  dimension: delete_flag {
    type: yesno
    sql: ${TABLE}.delete_flag ;;
  }

  dimension: demo_id {
    type: string
    sql: ${TABLE}.demo_id ;;
  }

  dimension: demo_type {
    type: string
    sql: ${TABLE}.demo_type ;;
  }

  dimension: enable_interview_level_screening_timer {
    type: yesno
    sql: ${TABLE}.enable_interview_level_screening_timer ;;
  }

  dimension: enable_screening_random_questions {
    type: yesno
    sql: ${TABLE}.enable_screening_random_questions ;;
  }

  dimension: enable_screening_score_range {
    type: yesno
    sql: ${TABLE}.enable_screening_score_range ;;
  }

  dimension: enable_screening_timer {
    type: yesno
    sql: ${TABLE}.enable_screening_timer ;;
  }

  dimension: favorite {
    type: yesno
    sql: ${TABLE}.favorite ;;
  }

  dimension: featured {
    type: yesno
    sql: ${TABLE}.featured ;;
  }

  dimension: filter_question {
    type: string
    sql: ${TABLE}.filter_question ;;
  }

  dimension: filter_question_response_type {
    type: string
    sql: ${TABLE}.filter_question_response_type ;;
  }

  dimension_group: first_opened {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.first_opened_at ;;
  }

  dimension: full_time_job {
    type: yesno
    sql: ${TABLE}.full_time_job ;;
  }

  dimension_group: full_time_start {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.full_time_start_date ;;
  }

  dimension: gmaps {
    type: yesno
    sql: ${TABLE}.gmaps ;;
  }

  dimension: id {
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: industry_id {
    type: number
    sql: ${TABLE}.industry_id ;;
  }

  dimension: industry_name {
    type: string
    sql: ${TABLE}.industry_name ;;
  }

  dimension: interview_level_screening_time_limit {
    type: number
    sql: ${TABLE}.interview_level_screening_time_limit ;;
  }

  dimension: jobseeker_publish_notification_sent {
    type: yesno
    sql: ${TABLE}.jobseeker_publish_notification_sent ;;
  }

  dimension_group: last_opened {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_opened_at ;;
  }

  dimension_group: last_unpublished {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_unpublished_at ;;
  }

  dimension_group: last_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_updated ;;
  }

  dimension: latitude {
    type: number
    sql: ${TABLE}.latitude ;;
  }

  dimension: location_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.location_id ;;
  }

  dimension: location_name {
    type: string
    sql: ${TABLE}.location_name ;;
  }

  dimension: longitude {
    type: number
    sql: ${TABLE}.longitude ;;
  }

  dimension: part_time_job {
    type: yesno
    sql: ${TABLE}.part_time_job ;;
  }

  dimension: part_time_schedule_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.part_time_schedule_id ;;
  }

  dimension: pay_amount {
    type: string
    sql: ${TABLE}.pay_amount ;;
  }

  dimension: pay_type_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.pay_type_id ;;
  }

  dimension: post_code {
    type: string
    sql: ${TABLE}.post_code ;;
  }

  dimension_group: posting {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.posting_date ;;
  }

  dimension: predict_automation {
    type: yesno
    sql: ${TABLE}.predict_automation ;;
  }

  dimension: predict_automation_reject {
    type: yesno
    sql: ${TABLE}.predict_automation_reject ;;
  }

  dimension: profile_status {
    type: string
    sql: ${TABLE}.profile_status ;;
  }

  dimension: publish_status {
    type: string
    sql: ${TABLE}.publish_status ;;
  }

  dimension: published {
    type: yesno
    sql: ${TABLE}.published ;;
  }

  dimension: qualifications {
    type: string
    sql: ${TABLE}.qualifications ;;
  }

  dimension: redirect_url {
    type: string
    sql: ${TABLE}.redirect_url ;;
  }

  dimension: reports_to {
    type: string
    sql: ${TABLE}.reports_to ;;
  }

  dimension: rerecord_flag {
    type: yesno
    sql: ${TABLE}.rerecord_flag ;;
  }

  dimension: responsibilities {
    type: string
    sql: ${TABLE}.responsibilities ;;
  }

  dimension: schedule_type_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.schedule_type_id ;;
  }

  dimension: screen_enable {
    type: yesno
    sql: ${TABLE}.screen_enable ;;
  }

  dimension: screen_exit {
    type: yesno
    sql: ${TABLE}.screen_exit ;;
  }

  dimension: screen_exit_message {
    type: string
    sql: ${TABLE}.screen_exit_message ;;
  }

  dimension: screen_exit_score {
    type: number
    sql: ${TABLE}.screen_exit_score ;;
  }

  dimension: screen_exit_type {
    type: string
    sql: ${TABLE}.screen_exit_type ;;
  }

  dimension: setup_mode {
    type: string
    sql: ${TABLE}.setup_mode ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  dimension: virtual_job_preference {
    type: yesno
    sql: ${TABLE}.virtual_job_preference ;;
  }

  dimension: visible {
    type: yesno
    sql: ${TABLE}.visible ;;
  }

  dimension: work_type {
    type: string
    sql: ${TABLE}.work_type ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      root_job_id,
      country_name,
      industry_name,
      location_name,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      degrees.id,
      degrees.name,
      locations.id,
      locations.name,
      part_time_schedules.id,
      part_time_schedules.name,
      pay_types.id,
      pay_types.name,
      schedule_types.display_name,
      schedule_types.id,
      schedule_types.name,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name,
      activity_logs.count,
      admin_reports.count,
      api_logs.count,
      api_review_interview_links.count,
      applicant_invite_reminder_histories.count,
      applicant_invite_system_reminder_histories.count,
      applicant_invites.count,
      applicant_subscriptions.count,
      application_retakes.count,
      application_statuses.count,
      archived_candidates.count,
      candidate_message_threads.count,
      candidate_records.count,
      candidate_screening_view_statuses.count,
      candidate_status_reports.count,
      collaborator_invites.count,
      conductors.count,
      data_policy_logs.count,
      documents.count,
      ds_job_infos.count,
      ds_review_groups.count,
      glacier_archives.count,
      glacier_restores.count,
      integration_reviews.count,
      integration_transactions.count,
      interview_requirements.count,
      interview_setup_requests.count,
      interviews.count,
      job_application_question_versions.count,
      job_applications.count,
      job_availabilities.count,
      job_batch_cv_uploads.count,
      job_clicks.count,
      job_collaborators.count,
      job_favorites.count,
      job_integration_mapping_notifications.count,
      job_integration_mappings.count,
      job_review_lists.count,
      job_roles.count,
      job_seeker_favorite_jobs.count,
      job_settings_stores.count,
      job_team_collaborators.count,
      live_assessments.count,
      mother_packers.count,
      predict_settings.count,
      recorder_errors.count,
      review_buckets.count,
      screening_assessments.count,
      screening_competency_measures.count,
      screening_feedback_frameworks.count,
      screening_multiple_scores.count,
      screening_question_groups.count,
      screening_question_images.count,
      screening_questions.count,
      screening_score_ranges.count,
      user_view_counters.count,
      video_assessments.count,
      video_playback_logs.count,
      videos.count
    ]
  }
}
