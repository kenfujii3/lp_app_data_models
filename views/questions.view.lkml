view: questions {
  sql_table_name: willh.questions ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: base_video_name {
    type: string
    sql: ${TABLE}.base_video_name ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: delete_flag {
    type: yesno
    sql: ${TABLE}.delete_flag ;;
  }

  dimension: interview_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.interview_id ;;
  }

  dimension: position {
    type: number
    sql: ${TABLE}.position ;;
  }

  dimension: preparation_time {
    type: number
    sql: ${TABLE}.preparation_time ;;
  }

  dimension: process_video_question {
    type: yesno
    sql: ${TABLE}.process_video_question ;;
  }

  dimension: question {
    type: string
    sql: ${TABLE}.question ;;
  }

  dimension: question_group_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.question_group_id ;;
  }

  dimension: random_question {
    type: yesno
    sql: ${TABLE}.random_question ;;
  }

  dimension: time_allowed {
    type: number
    sql: ${TABLE}.time_allowed ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      base_video_name,
      interviews.id,
      interviews.redirect_button_name,
      interviews.screening_redirect_button_name,
      question_groups.id,
      question_groups.name,
      candidate_average_scores.count,
      criteria_reviews.count,
      ds_candidate_question_scores.count,
      job_application_question_versions.count,
      video_questions.count,
      videos.count
    ]
  }
}
