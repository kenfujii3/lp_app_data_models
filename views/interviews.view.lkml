view: interviews {
  sql_table_name: willh.interviews ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: assessment_closing_video {
    type: string
    sql: ${TABLE}.assessment_closing_video ;;
  }

  dimension: assessment_company_video {
    type: string
    sql: ${TABLE}.assessment_company_video ;;
  }

  dimension: assessment_intro_video {
    type: string
    sql: ${TABLE}.assessment_intro_video ;;
  }

  dimension: assessment_practice_video {
    type: string
    sql: ${TABLE}.assessment_practice_video ;;
  }

  dimension: assessment_setup1_image {
    type: string
    sql: ${TABLE}.assessment_setup1_image ;;
  }

  dimension: assessment_setup2_image {
    type: string
    sql: ${TABLE}.assessment_setup2_image ;;
  }

  dimension: assessment_setup3_image {
    type: string
    sql: ${TABLE}.assessment_setup3_image ;;
  }

  dimension: assessment_tips1_image {
    type: string
    sql: ${TABLE}.assessment_tips1_image ;;
  }

  dimension: assessment_tips2_image {
    type: string
    sql: ${TABLE}.assessment_tips2_image ;;
  }

  dimension: assessment_tips3_image {
    type: string
    sql: ${TABLE}.assessment_tips3_image ;;
  }

  dimension: assessment_tips4_image {
    type: string
    sql: ${TABLE}.assessment_tips4_image ;;
  }

  dimension: assessment_tips_video {
    type: string
    sql: ${TABLE}.assessment_tips_video ;;
  }

  dimension: closing_message {
    type: string
    sql: ${TABLE}.closing_message ;;
  }

  dimension: closing_video_file {
    type: string
    sql: ${TABLE}.closing_video_file ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: custom_interview_id {
    type: string
    sql: ${TABLE}.custom_interview_id ;;
  }

  dimension_group: deadline {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.deadline ;;
  }

  dimension: deadline_flag {
    type: yesno
    sql: ${TABLE}.deadline_flag ;;
  }

  dimension: deadline_invite_days {
    type: number
    sql: ${TABLE}.deadline_invite_days ;;
  }

  dimension: deadline_mode {
    type: string
    sql: ${TABLE}.deadline_mode ;;
  }

  dimension: default_language {
    type: string
    sql: ${TABLE}.default_language ;;
  }

  dimension: enable_ratings {
    type: yesno
    sql: ${TABLE}.enable_ratings ;;
  }

  dimension: interview_deadline_timezone {
    type: string
    sql: ${TABLE}.interview_deadline_timezone ;;
  }

  dimension: intro_video_file {
    type: string
    sql: ${TABLE}.intro_video_file ;;
  }

  dimension: introduction_message {
    type: string
    sql: ${TABLE}.introduction_message ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: practice_video_url {
    type: string
    sql: ${TABLE}.practice_video_url ;;
  }

  dimension: preparation_mode {
    type: string
    sql: ${TABLE}.preparation_mode ;;
  }

  dimension: random_questions {
    type: yesno
    sql: ${TABLE}.random_questions ;;
  }

  dimension: random_questions_count {
    type: number
    sql: ${TABLE}.random_questions_count ;;
  }

  dimension: rating_mode {
    type: string
    sql: ${TABLE}.rating_mode ;;
  }

  dimension: rating_scale {
    type: number
    sql: ${TABLE}.rating_scale ;;
  }

  dimension: rating_scale_custom {
    type: yesno
    sql: ${TABLE}.rating_scale_custom ;;
  }

  dimension: rating_scale_type {
    type: string
    sql: ${TABLE}.rating_scale_type ;;
  }

  dimension: redirect_button_name {
    type: string
    sql: ${TABLE}.redirect_button_name ;;
  }

  dimension: redirect_url {
    type: string
    sql: ${TABLE}.redirect_url ;;
  }

  dimension: remind_candidates {
    type: yesno
    sql: ${TABLE}.remind_candidates ;;
  }

  dimension: rerecord_flag {
    type: yesno
    sql: ${TABLE}.rerecord_flag ;;
  }

  dimension: screening_redirect_button_name {
    type: string
    sql: ${TABLE}.screening_redirect_button_name ;;
  }

  dimension: screening_redirect_url {
    type: string
    sql: ${TABLE}.screening_redirect_url ;;
  }

  dimension: screening_video {
    type: string
    sql: ${TABLE}.screening_video ;;
  }

  dimension: show_redirect_button {
    type: yesno
    sql: ${TABLE}.show_redirect_button ;;
  }

  dimension: show_screening_redirect_button {
    type: yesno
    sql: ${TABLE}.show_screening_redirect_button ;;
  }

  dimension: transcribe_all_videos {
    type: yesno
    sql: ${TABLE}.transcribe_all_videos ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: use_closing_video {
    type: yesno
    sql: ${TABLE}.use_closing_video ;;
  }

  dimension: use_intro_video {
    type: yesno
    sql: ${TABLE}.use_intro_video ;;
  }

  dimension: use_question_groups {
    type: yesno
    sql: ${TABLE}.use_question_groups ;;
  }

  dimension: use_video_2 {
    type: yesno
    sql: ${TABLE}.use_video_2 ;;
  }

  dimension: use_video_3 {
    type: yesno
    sql: ${TABLE}.use_video_3 ;;
  }

  dimension: video_file_2 {
    type: string
    sql: ${TABLE}.video_file_2 ;;
  }

  dimension: video_file_3 {
    type: string
    sql: ${TABLE}.video_file_3 ;;
  }

  dimension: view_time_limit {
    type: number
    sql: ${TABLE}.view_time_limit ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      redirect_button_name,
      screening_redirect_button_name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      candidate_average_scores.count,
      candidate_feedback_scoring_results.count,
      criteria_reviews.count,
      feedback_competencies.count,
      integration_interviews.count,
      job_candidate_feedbacks.count,
      job_integration_mappings.count,
      question_groups.count,
      questions.count
    ]
  }
}
