view: integration_reviews {
  sql_table_name: willh.integration_reviews ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: candidate_email {
    type: string
    sql: ${TABLE}.candidate_email ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension_group: expiry {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.expiry ;;
  }

  dimension: job_application_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_application_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: review_code {
    type: string
    sql: ${TABLE}.review_code ;;
  }

  dimension: reviewer_email {
    type: string
    sql: ${TABLE}.reviewer_email ;;
  }

  dimension: reviewer_first_name {
    type: string
    sql: ${TABLE}.reviewer_first_name ;;
  }

  dimension: reviewer_last_name {
    type: string
    sql: ${TABLE}.reviewer_last_name ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      reviewer_first_name,
      reviewer_last_name,
      job_applications.base_video_name,
      job_applications.id,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id
    ]
  }
}
