view: live_criteria_reviews {
  sql_table_name: willh.live_criteria_reviews ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: answer_with {
    type: string
    sql: ${TABLE}.answer_with ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: criteria {
    type: string
    sql: ${TABLE}.criteria ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: live_interview_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_interview_id ;;
  }

  dimension: numeric_count {
    type: number
    sql: ${TABLE}.numeric_count ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, live_interviews.id, live_criteria_review_answers.count, live_scheduled_answers.count]
  }
}
