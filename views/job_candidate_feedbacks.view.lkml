view: job_candidate_feedbacks {
  sql_table_name: willh.job_candidate_feedbacks ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: feedback_enabled_video {
    type: yesno
    sql: ${TABLE}.feedback_enabled_video ;;
  }

  dimension: interview_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.interview_id ;;
  }

  measure: count {
    type: count
    drill_fields: [id, interviews.id, interviews.redirect_button_name, interviews.screening_redirect_button_name, candidate_feedback_settings.count]
  }
}
