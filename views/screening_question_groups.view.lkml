view: screening_question_groups {
  sql_table_name: willh.screening_question_groups ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: is_mandatory {
    type: yesno
    sql: ${TABLE}.is_mandatory ;;
  }

  dimension: is_randomised {
    type: yesno
    sql: ${TABLE}.is_randomised ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      screening_questions.count
    ]
  }
}
