view: job_seeker_video_recordings {
  sql_table_name: willh.job_seeker_video_recordings ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: buffer_delay {
    type: number
    sql: ${TABLE}.buffer_delay ;;
  }

  dimension: created_at {
    type: number
    sql: ${TABLE}.created_at ;;
  }

  dimension: job_seeker_video_session_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_seeker_video_session_id ;;
  }

  dimension: length {
    type: number
    sql: ${TABLE}.length ;;
  }

  dimension: submitted {
    type: yesno
    sql: ${TABLE}.submitted ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, job_seeker_video_sessions.browser_name, job_seeker_video_sessions.id]
  }
}
