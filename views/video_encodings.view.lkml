view: video_encodings {
  sql_table_name: willh.video_encodings ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: attempts {
    type: number
    sql: ${TABLE}.attempts ;;
  }

  dimension: audio_codec {
    type: string
    sql: ${TABLE}.audio_codec ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: duration {
    type: number
    sql: ${TABLE}.duration ;;
  }

  dimension: encoding_progress {
    type: string
    sql: ${TABLE}.encoding_progress ;;
  }

  dimension: encoding_status {
    type: string
    sql: ${TABLE}.encoding_status ;;
  }

  dimension: encoding_time {
    type: number
    sql: ${TABLE}.encoding_time ;;
  }

  dimension_group: error_notification_sent {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.error_notification_sent_at ;;
  }

  dimension: extname {
    type: string
    sql: ${TABLE}.extname ;;
  }

  dimension: file_size {
    type: number
    sql: ${TABLE}.file_size ;;
  }

  dimension: filename {
    type: string
    sql: ${TABLE}.filename ;;
  }

  dimension: height {
    type: number
    sql: ${TABLE}.height ;;
  }

  dimension: mime_type {
    type: string
    sql: ${TABLE}.mime_type ;;
  }

  dimension: panda_id {
    type: string
    sql: ${TABLE}.panda_id ;;
  }

  dimension: panda_profile_id {
    type: string
    sql: ${TABLE}.panda_profile_id ;;
  }

  dimension: panda_profile_name {
    type: string
    sql: ${TABLE}.panda_profile_name ;;
  }

  dimension: path {
    type: string
    sql: ${TABLE}.path ;;
  }

  dimension_group: started_encoding {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.started_encoding_at ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: video_codec {
    type: string
    sql: ${TABLE}.video_codec ;;
  }

  dimension: video_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.video_id ;;
  }

  dimension: width {
    type: number
    sql: ${TABLE}.width ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      extname,
      filename,
      panda_profile_name,
      videos.extname,
      videos.filename,
      videos.id,
      glacier_archives.count
    ]
  }
}
