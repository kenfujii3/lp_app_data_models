view: stage_candidate_review_criteria {
  sql_table_name: willh.stage_candidate_review_criteria ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: candidate_review_criteria_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_review_criteria_id ;;
  }

  dimension: stage_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.stage_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [id, candidate_review_criteria.id]
  }
}
