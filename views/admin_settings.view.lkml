view: admin_settings {
  sql_table_name: willh.admin_settings ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: assessment_closing_video {
    type: string
    sql: ${TABLE}.assessment_closing_video ;;
  }

  dimension: assessment_intro_video {
    type: string
    sql: ${TABLE}.assessment_intro_video ;;
  }

  dimension: assessment_powered_by_url {
    type: string
    sql: ${TABLE}.assessment_powered_by_url ;;
  }

  dimension: assessment_practice_video {
    type: string
    sql: ${TABLE}.assessment_practice_video ;;
  }

  dimension: assessment_setup1_image {
    type: string
    sql: ${TABLE}.assessment_setup1_image ;;
  }

  dimension: assessment_setup2_image {
    type: string
    sql: ${TABLE}.assessment_setup2_image ;;
  }

  dimension: assessment_setup3_image {
    type: string
    sql: ${TABLE}.assessment_setup3_image ;;
  }

  dimension: assessment_tips1_image {
    type: string
    sql: ${TABLE}.assessment_tips1_image ;;
  }

  dimension: assessment_tips2_image {
    type: string
    sql: ${TABLE}.assessment_tips2_image ;;
  }

  dimension: assessment_tips3_image {
    type: string
    sql: ${TABLE}.assessment_tips3_image ;;
  }

  dimension: assessment_tips4_image {
    type: string
    sql: ${TABLE}.assessment_tips4_image ;;
  }

  dimension: assessment_tips_video {
    type: string
    sql: ${TABLE}.assessment_tips_video ;;
  }

  dimension: backup_media_server {
    type: string
    sql: ${TABLE}.backup_media_server ;;
  }

  dimension: candidate_feedback_email_days {
    type: string
    sql: ${TABLE}.candidate_feedback_email_days ;;
  }

  dimension: contact_request_emails {
    type: string
    sql: ${TABLE}.contact_request_emails ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: default_rejection_message {
    type: string
    sql: ${TABLE}.default_rejection_message ;;
  }

  dimension: encodings_check_frequency {
    type: number
    sql: ${TABLE}.encodings_check_frequency ;;
  }

  dimension: encodings_queue_length_threshold {
    type: number
    sql: ${TABLE}.encodings_queue_length_threshold ;;
  }

  dimension: failed_encodings_threshold {
    type: number
    sql: ${TABLE}.failed_encodings_threshold ;;
  }

  dimension: file_not_founds_threshold {
    type: number
    sql: ${TABLE}.file_not_founds_threshold ;;
  }

  dimension: grab_n_go_notification_emails {
    type: string
    sql: ${TABLE}.grab_n_go_notification_emails ;;
  }

  dimension: interview_reminder_days {
    type: string
    sql: ${TABLE}.interview_reminder_days ;;
  }

  dimension: interview_reminder_days_subsequent {
    type: string
    sql: ${TABLE}.interview_reminder_days_subsequent ;;
  }

  dimension: job_seeker_profile_fields {
    type: number
    sql: ${TABLE}.job_seeker_profile_fields ;;
  }

  dimension: live_boarding_lease_time {
    type: number
    sql: ${TABLE}.live_boarding_lease_time ;;
  }

  dimension: live_compatibility_lease_time {
    type: number
    sql: ${TABLE}.live_compatibility_lease_time ;;
  }

  dimension: live_lobby_lease_time {
    type: number
    sql: ${TABLE}.live_lobby_lease_time ;;
  }

  dimension: media_server_backup_fallback {
    type: string
    sql: ${TABLE}.media_server_backup_fallback ;;
  }

  dimension: media_server_backup_initial {
    type: string
    sql: ${TABLE}.media_server_backup_initial ;;
  }

  dimension: media_server_primary_fallback {
    type: string
    sql: ${TABLE}.media_server_primary_fallback ;;
  }

  dimension: media_server_primary_initial {
    type: string
    sql: ${TABLE}.media_server_primary_initial ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: primary_media_server {
    type: string
    sql: ${TABLE}.primary_media_server ;;
  }

  dimension: question_time_limits {
    type: string
    sql: ${TABLE}.question_time_limits ;;
  }

  dimension_group: rejection_message_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.rejection_message_updated_at ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: uploads_server {
    type: string
    sql: ${TABLE}.uploads_server ;;
  }

  dimension: use_primary_media_server {
    type: yesno
    sql: ${TABLE}.use_primary_media_server ;;
  }

  dimension: video_encode_mode {
    type: string
    sql: ${TABLE}.video_encode_mode ;;
  }

  dimension: video_encode_storage {
    type: string
    sql: ${TABLE}.video_encode_storage ;;
  }

  dimension: video_player_server {
    type: string
    sql: ${TABLE}.video_player_server ;;
  }

  dimension: video_recorder_audio_codec {
    type: string
    sql: ${TABLE}.video_recorder_audio_codec ;;
  }

  dimension: video_recorder_audio_frames_per_packet {
    type: number
    sql: ${TABLE}.video_recorder_audio_frames_per_packet ;;
  }

  dimension: video_recorder_audio_gain {
    type: number
    sql: ${TABLE}.video_recorder_audio_gain ;;
  }

  dimension: video_recorder_audio_quality {
    type: number
    sql: ${TABLE}.video_recorder_audio_quality ;;
  }

  dimension: video_recorder_audio_rate {
    type: number
    sql: ${TABLE}.video_recorder_audio_rate ;;
  }

  dimension: video_recorder_audio_set_loopback {
    type: number
    sql: ${TABLE}.video_recorder_audio_set_loopback ;;
  }

  dimension: video_recorder_audio_silence_threshold {
    type: number
    sql: ${TABLE}.video_recorder_audio_silence_threshold ;;
  }

  dimension: video_recorder_audio_silence_timeout {
    type: number
    sql: ${TABLE}.video_recorder_audio_silence_timeout ;;
  }

  dimension: video_recorder_audio_suppress_echo {
    type: number
    sql: ${TABLE}.video_recorder_audio_suppress_echo ;;
  }

  dimension: video_recorder_bandwidth_low_threshold {
    type: number
    sql: ${TABLE}.video_recorder_bandwidth_low_threshold ;;
  }

  dimension: video_recorder_bandwidth_medium_threshold {
    type: number
    sql: ${TABLE}.video_recorder_bandwidth_medium_threshold ;;
  }

  dimension: video_recorder_buffer_time {
    type: number
    sql: ${TABLE}.video_recorder_buffer_time ;;
  }

  dimension: video_recorder_cam_bandwidth {
    type: number
    sql: ${TABLE}.video_recorder_cam_bandwidth ;;
  }

  dimension: video_recorder_cam_fps {
    type: number
    sql: ${TABLE}.video_recorder_cam_fps ;;
  }

  dimension: video_recorder_cam_key_frame_interval {
    type: number
    sql: ${TABLE}.video_recorder_cam_key_frame_interval ;;
  }

  dimension: video_recorder_cam_quality {
    type: number
    sql: ${TABLE}.video_recorder_cam_quality ;;
  }

  dimension: video_recorder_cam_width {
    type: number
    sql: ${TABLE}.video_recorder_cam_width ;;
  }

  dimension: video_recorder_high_bandwidth_delta {
    type: string
    sql: ${TABLE}.video_recorder_high_bandwidth_delta ;;
  }

  dimension: video_recorder_highest_audio_rate {
    type: number
    sql: ${TABLE}.video_recorder_highest_audio_rate ;;
  }

  dimension: video_recorder_highest_cam_quality {
    type: number
    sql: ${TABLE}.video_recorder_highest_cam_quality ;;
  }

  dimension: video_recorder_max_capture_duration {
    type: number
    sql: ${TABLE}.video_recorder_max_capture_duration ;;
  }

  dimension: video_reencoding_max_duration {
    type: number
    sql: ${TABLE}.video_reencoding_max_duration ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name]
  }
}
