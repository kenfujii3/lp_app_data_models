view: live_meetings {
  sql_table_name: willh.live_meetings ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: apply_code {
    type: string
    sql: ${TABLE}.apply_code ;;
  }

  dimension: candidate_brief {
    type: string
    sql: ${TABLE}.candidate_brief ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension: complete_meeting_job_id {
    type: number
    sql: ${TABLE}.complete_meeting_job_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: invitor_id {
    type: number
    sql: ${TABLE}.invitor_id ;;
  }

  dimension: live_assessment_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_assessment_id ;;
  }

  dimension: live_scheduling_enabled {
    type: yesno
    sql: ${TABLE}.live_scheduling_enabled ;;
  }

  dimension: remaining_reschedules {
    type: number
    sql: ${TABLE}.remaining_reschedules ;;
  }

  dimension: reminder_id {
    type: number
    sql: ${TABLE}.reminder_id ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      live_assessments.id,
      live_assessment_candidates.count,
      live_assessment_reviews.count,
      live_meeting_deadlines.count,
      live_meeting_providers.count,
      live_schedules.count
    ]
  }
}
