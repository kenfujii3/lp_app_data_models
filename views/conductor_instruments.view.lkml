view: conductor_instruments {
  sql_table_name: willh.conductor_instruments ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: conductor_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.conductor_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: current {
    type: yesno
    sql: ${TABLE}.current ;;
  }

  dimension: data {
    type: string
    sql: ${TABLE}.data ;;
  }

  dimension: instrument_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.instrument_id ;;
  }

  dimension: modular_assessment_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.modular_assessment_id ;;
  }

  dimension: order {
    type: number
    sql: ${TABLE}.`order` ;;
  }

  dimension: position {
    type: number
    sql: ${TABLE}.position ;;
  }

  dimension: result {
    type: string
    sql: ${TABLE}.result ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      conductors.id,
      instruments.id,
      instruments.name,
      modular_assessments.id,
      data_fields.count,
      instrument_events.count,
      modular_assessment_results.count
    ]
  }

## The following generate candidate statuses for all assessments
  dimension: screening_invited {
    type: yesno
    hidden: yes
    sql: ${TABLE}.status = 'unstarted' AND ${instruments.name} = 'Screening';;
  }

  measure: total_screening_invited {
    type: sum
    label: "Screening Invited"
    sql: ${screening_invited};;
  }

  dimension: screening_in_progress {
    type: yesno
    hidden: yes
    sql: ${instruments.name} = 'Screening' AND ${TABLE}.status = 'ongoing' OR
      ${instruments.name} = 'Screening' AND ${TABLE}.status = 'did_not_complete';;
  }

  measure: total_screening_in_progress {
    type: sum
    label: "Screening In Progress"
    sql: ${screening_in_progress};;
  }

  dimension: screening_completed {
    type: yesno
    hidden: yes
    sql: ${instruments.name} = 'Screening' AND ${TABLE}.status = 'completed' OR
      ${instruments.name} = 'Screening' AND ${TABLE}.status = 'finished';;
  }

  measure: total_screening_completed {
    type: sum
    label: "Screening Completed"
    sql: ${screening_completed};;
  }

  dimension: external_assessment_invited {
    type: yesno
    hidden: yes
    sql: ${TABLE}.status = 'unstarted' AND ${instruments.name} = 'External Assessment';;
  }

  measure: total_external_assessment_invited {
    type: sum
    label: "External Assessment Invited"
    sql: ${external_assessment_invited};;
  }

  dimension: external_assessment_in_progress {
    type: yesno
    hidden: yes
    sql:  ${instruments.name} = 'External Assessment' AND ${TABLE}.status = 'ongoing' OR
      ${instruments.name} = 'External Assessment' AND ${TABLE}.status = 'did not complete';;
  }

  measure: total_external_assessment_in_progress {
    type: sum
    label: "External Assessment In Progress"
    sql: ${external_assessment_in_progress};;
  }

  dimension: external_assessment_completed {
    type: yesno
    hidden: yes
    sql:  ${instruments.name} = 'External Assessment' AND ${TABLE}.status = 'completed' OR
      ${instruments.name} = 'External Assessment' AND ${TABLE}.status = 'finished';;
  }

  measure: total_external_assessment_completed {
    type: sum
    label: "External Assessment Completed"
    sql: ${external_assessment_completed};;
  }

  dimension: rvi_invited {
    type: yesno
    hidden: yes
    sql: ${instruments.name} = 'Video Assessment' AND ${TABLE}.status = 'unstarted';;
  }

  measure: total_rvi_invited {
    type: sum
    label: "RVI Invited"
    sql: ${rvi_invited};;
  }

  dimension: rvi_in_progress {
    type: yesno
    hidden: yes
    sql: ${instruments.name} = 'Video Assessment' AND ${TABLE}.status = 'ongoing';;
  }

  measure: total_rvi_in_progress {
    type: sum
    label: "RVI In Progress"
    sql: ${rvi_in_progress};;
  }

  dimension: rvi_completed {
    type: yesno
    hidden: yes
    sql:  ${instruments.name} = 'Video Assessment' AND ${TABLE}.status = 'retaken' OR
      ${instruments.name} = 'Video Assessment' AND ${TABLE}.status = 'completed';;
  }

  measure: total_rvi_completed {
    type: sum
    label: "RVI Completed"
    sql: ${rvi_completed};;
  }

  dimension: cute_invited {
    type: yesno
    hidden: yes
    sql: ${instruments.name} = 'Cut-e Assessment' AND ${TABLE}.status = 'unstarted';;
  }

  measure: total_cute_invited {
    type: sum
    label: "Cut-e Invited"
    sql: ${cute_invited};;
  }

  dimension: cute_in_progress {
    type: yesno
    hidden: yes
    sql: ${instruments.name} = 'Cut-e Assessment' AND ${TABLE}.status = 'ongoing';;
  }

  measure: total_cute_in_progress {
    type: sum
    label: "Cut-e In Progress"
    sql: ${cute_in_progress};;
  }

  dimension: cute_completed {
    type: yesno
    hidden: yes
    sql: ${instruments.name} = 'Cut-e Assessment' AND ${TABLE}.status = 'completed';;
  }

  measure: total_cute_completed {
    type: sum
    label: "Cut-e Completed"
    sql: ${cute_completed};;
  }

  dimension: shl_invited {
    type: yesno
    hidden: yes
    sql:  ${instruments.name} = 'SHL Assessment' AND ${TABLE}.status = 'unstarted';;
  }

  measure: total_shl_invited {
    type: sum
    label: "SHL Invited"
    sql: ${shl_invited};;
  }

  dimension: shl_in_progress {
    type: yesno
    hidden: yes
    sql:  ${instruments.name} = 'SHL Assessment' AND ${TABLE}.status = 'ongoing' OR
      ${instruments.name} = 'SHL Assessment' AND ${TABLE}.status = 'finished';;
  }

  measure: total_shl_in_progress {
    type: sum
    label: "SHL In Progress"
    sql: ${shl_in_progress};;
  }

  dimension: shl_completed {
    type: yesno
    hidden: yes
    sql:  ${instruments.name} = 'SHL Assessment' AND ${TABLE}.status = 'completed' OR
      ${instruments.name} = 'SHL Assessment' AND ${TABLE}.status = 'failed';;
  }

  measure: total_shl_completed {
    type: sum
    label: "SHL Completed"
    sql: ${shl_completed};;
  }


  dimension: candidate_status {
    case: {
      when: {sql: ${TABLE}.status = 'unstarted' AND ${instruments.name} = 'Screening' ;;
        label: "Screening Invited"
        }
      when: {sql: ${TABLE}.status = 'ongoing' AND ${instruments.name} = 'Screening' ;;
        label: "Screening In Progress"
        }
      when: {sql: ${TABLE}.status = 'did_not_complete' AND ${instruments.name} = 'Screening' ;;
        label: "Screening In Progress"
      }
      when: {sql: ${TABLE}.status = 'completed' AND ${instruments.name} = 'Screening' ;;
        label: "Screening Completed"
      }
      when: {sql: ${TABLE}.status = 'finished' AND ${instruments.name} = 'Screening' ;;
        label: "Screening Completed"
      }

      when: {sql: ${TABLE}.status = 'unstarted' AND ${instruments.name} = 'External Assessment' ;;
        label: "External Assessment Invited"
      }
      when: {sql: ${TABLE}.status = 'ongoing' AND ${instruments.name} = 'External Assessment' ;;
        label: "External Assessment In Progress"
      }
      when: {sql: ${TABLE}.status = 'did not complete' AND ${instruments.name} = 'External Assessment' ;;
        label: "External Assessment In Progress"
      }
      when: {sql: ${TABLE}.status = 'completed' AND ${instruments.name} = 'External Assessment' ;;
        label: "External Assessment Completed"
      }
      when: {sql: ${TABLE}.status = 'finished' AND ${instruments.name} = 'External Assessment' ;;
        label: "External Assessment Completed"
      }

      when: {sql: ${TABLE}.status = 'unstarted' AND ${instruments.name} = 'Video Assessment' ;;
        label: "RVI Invited"
      }
      when: {sql: ${TABLE}.status = 'ongoing' AND ${instruments.name} = 'Video Assessment' ;;
        label: "RVI In Progress"
      }
      when: {sql: ${TABLE}.status = 'retaken' AND ${instruments.name} = 'Video Assessment' ;;
        label: "RVI Completed"
      }
      when: {sql: ${TABLE}.status = 'completed' AND ${instruments.name} = 'Video Assessment' ;;
        label: "RVI Completed"
      }

      when: {sql: ${TABLE}.status = 'unstarted' AND ${instruments.name} = 'Cut-e Assessment' ;;
        label: "Cut-e Invited"
      }
      when: {sql: ${TABLE}.status = 'ongoing' AND ${instruments.name} = 'Cut-e Assessment' ;;
        label: "Cut-e In Progress"
      }
      when: {sql: ${TABLE}.status = 'completed' AND ${instruments.name} = 'Cut-e Assessment' ;;
        label: "Cut-e Completed"
      }

      when: {sql: ${TABLE}.status = 'unstarted' AND ${instruments.name} = 'SHL Assessment' ;;
        label: "SHL Invited"
      }
      when: {sql: ${TABLE}.status = 'ongoing' AND ${instruments.name} = 'SHL Assessment' ;;
        label: "SHL In Progress"
      }
      when: {sql: ${TABLE}.status = 'finished' AND ${instruments.name} = 'SHL Assessment' ;;
        label: "SHL In Progress"
      }
      when: {sql: ${TABLE}.status = 'completed' AND ${instruments.name} = 'SHL Assessment' ;;
        label: "SHL Completed"
      }
      when: {sql: ${TABLE}.status = 'failed' AND ${instruments.name} = 'SHL Assessment' ;;
        label: "SHL Completed"
      }
      else: "Other"
    }
  }

  dimension: candidate_status_order {
    case: {
      when: {sql: ${candidate_status} = 'Screening Invited' ;;
        label: "1"
      }
      when: {sql: ${candidate_status} = 'Screening In Progress' ;;
        label: "2"
      }
      when: {sql: ${candidate_status} = 'Screening Completed' ;;
        label: "3"
      }
      when: {sql: ${candidate_status} = 'External Assessment Invited' ;;
        label: "4"
      }
      when: {sql: ${candidate_status} = 'External Assessment In Progress' ;;
        label: "5"
      }
      when: {sql: ${candidate_status} = 'External Assessment Completed' ;;
        label: "6"
      }
      when: {sql: ${candidate_status} = 'RVI Invited' ;;
        label: "7"
      }
      when: {sql: ${candidate_status} = 'RVI In Progress' ;;
        label: "8"
      }
      when: {sql: ${candidate_status} = 'RVI Completed' ;;
        label: "9"
      }
      when: {sql: ${candidate_status} = 'Cut-e Invited' ;;
        label: "10"
      }
      when: {sql: ${candidate_status} = 'Cut-e In Progress' ;;
        label: "11"
      }
      when: {sql: ${candidate_status} = 'Cut-e Completed' ;;
        label: "12"
      }
      when: {sql: ${candidate_status} = 'SHL Invited' ;;
        label: "13"
      }
      when: {sql: ${candidate_status} = 'SHL In Progress' ;;
        label: "14"
      }
      when: {sql: ${candidate_status} = 'SHL Completed' ;;
        label: "15"
      }
      when: {sql: ${candidate_status} = 'Other' ;;
        label: "16"
      }
    }

  }


}
