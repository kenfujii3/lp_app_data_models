view: mother_packing_codes {
  sql_table_name: willh.mother_packing_codes ;;

  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }

  dimension: current {
    type: yesno
    sql: ${TABLE}.current ;;
  }

  dimension: mother_packer_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.mother_packer_uuid ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
