view: employer_video_questions {
  sql_table_name: willh.employer_video_questions ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: modular_video_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.modular_video_uuid ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.uuid ;;
  }

  dimension: video_question_v2_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.video_question_v2_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
