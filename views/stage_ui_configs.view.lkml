view: stage_ui_configs {
  sql_table_name: willh.stage_ui_configs ;;

  dimension: category {
    type: string
    sql: ${TABLE}.category ;;
  }

  dimension: position {
    type: number
    sql: ${TABLE}.position ;;
  }

  dimension: section {
    type: string
    sql: ${TABLE}.section ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
