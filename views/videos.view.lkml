view: videos {
  sql_table_name: willh.videos ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: applicant_id {
    type: number
    sql: ${TABLE}.applicant_id ;;
  }

  dimension: audio_codec {
    type: string
    sql: ${TABLE}.audio_codec ;;
  }

  dimension: bucket {
    type: string
    sql: ${TABLE}.bucket ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: duration {
    type: number
    sql: ${TABLE}.duration ;;
  }

  dimension: employer_id {
    type: number
    sql: ${TABLE}.employer_id ;;
  }

  dimension: error_class {
    type: string
    sql: ${TABLE}.error_class ;;
  }

  dimension: error_message {
    type: string
    sql: ${TABLE}.error_message ;;
  }

  dimension: extname {
    type: string
    sql: ${TABLE}.extname ;;
  }

  dimension: file_size {
    type: number
    sql: ${TABLE}.file_size ;;
  }

  dimension: filename {
    type: string
    sql: ${TABLE}.filename ;;
  }

  dimension: height {
    type: number
    sql: ${TABLE}.height ;;
  }

  dimension: job_application_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_application_id ;;
  }

  dimension: job_application_question_version_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_application_question_version_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: mime_type {
    type: string
    sql: ${TABLE}.mime_type ;;
  }

  dimension: panda_cloud_id {
    type: string
    sql: ${TABLE}.panda_cloud_id ;;
  }

  dimension: panda_id {
    type: string
    sql: ${TABLE}.panda_id ;;
  }

  dimension: panda_status {
    type: string
    sql: ${TABLE}.panda_status ;;
  }

  dimension: path {
    type: string
    sql: ${TABLE}.path ;;
  }

  dimension: question_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.question_id ;;
  }

  dimension: question_index {
    type: number
    sql: ${TABLE}.question_index ;;
  }

  dimension: reencoded {
    type: yesno
    sql: ${TABLE}.reencoded ;;
  }

  dimension_group: reencoded {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.reencoded_at ;;
  }

  dimension: region {
    type: string
    sql: ${TABLE}.region ;;
  }

  dimension: source_url {
    type: string
    sql: ${TABLE}.source_url ;;
  }

  dimension: transcript {
    type: string
    sql: ${TABLE}.transcript ;;
  }

  dimension: transcript_data {
    type: string
    sql: ${TABLE}.transcript_data ;;
  }

  dimension: transcript_job_id {
    type: string
    sql: ${TABLE}.transcript_job_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: video_codec {
    type: string
    sql: ${TABLE}.video_codec ;;
  }

  dimension: video_type {
    type: string
    sql: ${TABLE}.video_type ;;
  }

  dimension: width {
    type: number
    sql: ${TABLE}.width ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      extname,
      filename,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      job_applications.base_video_name,
      job_applications.id,
      job_application_question_versions.id,
      job_application_question_versions.speech_filename,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      questions.base_video_name,
      questions.id,
      glacier_archives.count,
      video_encodings.count
    ]
  }
}
