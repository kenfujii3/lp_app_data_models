view: mother_packing_packages {
  sql_table_name: willh.mother_packing_packages ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: mother_packer_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.mother_packer_id ;;
  }

  dimension: package_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.package_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, mother_packers.id, packages.id, packages.name]
  }
}
