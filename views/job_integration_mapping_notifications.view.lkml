view: job_integration_mapping_notifications {
  sql_table_name: willh.job_integration_mapping_notifications ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: integration_transaction_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.integration_transaction_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: notification_type {
    type: string
    sql: ${TABLE}.notification_type ;;
  }

  dimension: requisition_id {
    type: string
    sql: ${TABLE}.requisition_id ;;
  }

  dimension: requisition_title {
    type: string
    sql: ${TABLE}.requisition_title ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      integration_transactions.candidate_first_name,
      integration_transactions.candidate_last_name,
      integration_transactions.id,
      integration_transactions.requester_first_name,
      integration_transactions.requester_last_name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id
    ]
  }
}
