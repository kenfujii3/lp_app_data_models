view: video_questions {
  sql_table_name: willh.video_questions ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: question_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.question_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: video_assessment_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.video_assessment_id ;;
  }

  measure: count {
    type: count
    drill_fields: [id, questions.base_video_name, questions.id, video_assessments.id]
  }
}
