view: account_available_stages {
  sql_table_name: willh.account_available_stages ;;

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: stage_category_name {
    type: string
    sql: ${TABLE}.stage_category_name ;;
  }

  measure: count {
    type: count
    drill_fields: [stage_category_name, accounts.maybe_field_name, accounts.name, accounts.provisioner_account_id, accounts.subdomain_name]
  }
}
