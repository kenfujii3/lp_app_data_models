view: criteria_reviews {
  sql_table_name: willh.criteria_reviews ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: answer_with {
    type: string
    sql: ${TABLE}.answer_with ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: criteria {
    type: string
    sql: ${TABLE}.criteria ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: ds_interview_id {
    type: number
    sql: ${TABLE}.ds_interview_id ;;
  }

  dimension: ds_question_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_question_id ;;
  }

  dimension: ds_review_group_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_review_group_id ;;
  }

  dimension: interview_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.interview_id ;;
  }

  dimension: interview_id_for_rating_scale {
    type: number
    value_format_name: id
    sql: ${TABLE}.interview_id_for_rating_scale ;;
  }

  dimension: live_assessment_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_assessment_id ;;
  }

  dimension: numeric_count {
    type: number
    sql: ${TABLE}.numeric_count ;;
  }

  dimension: question_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.question_id ;;
  }

  dimension: screening_question_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_question_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: weight {
    type: number
    sql: ${TABLE}.weight ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      ds_questions.id,
      ds_review_groups.id,
      ds_review_groups.name,
      interviews.id,
      interviews.redirect_button_name,
      interviews.screening_redirect_button_name,
      live_assessments.id,
      questions.base_video_name,
      questions.id,
      screening_questions.id,
      screening_questions.question_image_file_name,
      screening_questions.screening_competency_measure_name,
      candidate_average_scores.count,
      criteria_review_answers.count,
      ds_benchmark_scores.count,
      job_application_criteria_scores.count
    ]
  }
}
