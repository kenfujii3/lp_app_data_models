view: candidate_screening_options {
  sql_table_name: willh.candidate_screening_options ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: screening_option_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_option_id ;;
  }

  dimension: screening_question_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_question_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: version_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.version_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      screening_options.id,
      screening_options.image_file_name,
      screening_options.name,
      screening_questions.id,
      screening_questions.question_image_file_name,
      screening_questions.screening_competency_measure_name,
      versions.id
    ]
  }
}
