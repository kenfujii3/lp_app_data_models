view: application_retakes {
  sql_table_name: willh.application_retakes ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: applicant_id {
    type: number
    sql: ${TABLE}.applicant_id ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: job_application_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_application_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: process_retake {
    type: yesno
    sql: ${TABLE}.process_retake ;;
  }

  dimension: requester_id {
    type: number
    sql: ${TABLE}.requester_id ;;
  }

  dimension: retake_message {
    type: string
    sql: ${TABLE}.retake_message ;;
  }

  dimension: retake_questions {
    type: string
    sql: ${TABLE}.retake_questions ;;
  }

  dimension: retake_reason {
    type: string
    sql: ${TABLE}.retake_reason ;;
  }

  dimension: send_email {
    type: yesno
    sql: ${TABLE}.send_email ;;
  }

  dimension: unlimited_share_code {
    type: string
    sql: ${TABLE}.unlimited_share_code ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      job_applications.base_video_name,
      job_applications.id,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id
    ]
  }
}
