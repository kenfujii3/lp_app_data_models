view: candidate_records {
  sql_table_name: willh.candidate_records ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: api_created {
    type: yesno
    sql: ${TABLE}.api_created ;;
  }

  dimension: applicant_invite_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.applicant_invite_id ;;
  }

  dimension: application_status_id {
    type: number
    sql: ${TABLE}.application_status_id ;;
  }

  dimension: applied {
    type: yesno
    sql: ${TABLE}.applied ;;
  }

  dimension_group: applied {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.applied_at ;;
  }

  dimension: apply_code {
    type: string
    sql: ${TABLE}.apply_code ;;
  }

  dimension: archived_logs {
    type: yesno
    sql: ${TABLE}.archived_logs ;;
  }

  dimension: base_video_name {
    type: string
    sql: ${TABLE}.base_video_name ;;
  }

  dimension: callback_api_attempts {
    type: number
    sql: ${TABLE}.callback_api_attempts ;;
  }

  dimension: country_code {
    type: string
    sql: ${TABLE}.country_code ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: custom_fields {
    type: string
    sql: ${TABLE}.custom_fields ;;
  }

  dimension: cv_content_type {
    type: string
    sql: ${TABLE}.cv_content_type ;;
  }

  dimension: cv_file_name {
    type: string
    sql: ${TABLE}.cv_file_name ;;
  }

  dimension: cv_file_size {
    type: number
    sql: ${TABLE}.cv_file_size ;;
  }

  dimension_group: cv_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.cv_updated_at ;;
  }

  dimension_group: deadline {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.deadline ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: email_status {
    type: string
    sql: ${TABLE}.email_status ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}.first_name ;;
  }

  dimension: formatted_mobile {
    type: string
    sql: ${TABLE}.formatted_mobile ;;
  }

  dimension: has_cv {
    type: yesno
    sql: ${TABLE}.has_cv ;;
  }

  dimension: has_photo {
    type: yesno
    sql: ${TABLE}.has_photo ;;
  }

  dimension: integration_application_id {
    type: string
    sql: ${TABLE}.integration_application_id ;;
  }

  dimension: interview_status {
    type: string
    sql: ${TABLE}.interview_status ;;
  }

  dimension: invitation_type {
    type: string
    sql: ${TABLE}.invitation_type ;;
  }

  dimension: invitor_id {
    type: number
    sql: ${TABLE}.invitor_id ;;
  }

  dimension: job_application_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_application_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: keep_name_private {
    type: yesno
    sql: ${TABLE}.keep_name_private ;;
  }

  dimension_group: last_activity {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_activity_at ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}.last_name ;;
  }

  dimension: link_opened {
    type: yesno
    sql: ${TABLE}.link_opened ;;
  }

  dimension_group: link_opened {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.link_opened_at ;;
  }

  dimension: linkedin_profile {
    type: string
    sql: ${TABLE}.linkedin_profile ;;
  }

  dimension: message {
    type: string
    sql: ${TABLE}.message ;;
  }

  dimension: original_mobile {
    type: string
    sql: ${TABLE}.original_mobile ;;
  }

  dimension: parsed_cv {
    type: string
    sql: ${TABLE}.parsed_cv ;;
  }

  dimension: photo_content_type {
    type: string
    sql: ${TABLE}.photo_content_type ;;
  }

  dimension: photo_file_name {
    type: string
    sql: ${TABLE}.photo_file_name ;;
  }

  dimension: photo_file_size {
    type: number
    sql: ${TABLE}.photo_file_size ;;
  }

  dimension_group: photo_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.photo_updated_at ;;
  }

  dimension: profile_description {
    type: string
    sql: ${TABLE}.profile_description ;;
  }

  dimension: profile_title {
    type: string
    sql: ${TABLE}.profile_title ;;
  }

  dimension: projects_and_organizations {
    type: string
    sql: ${TABLE}.projects_and_organizations ;;
  }

  dimension: ranking_percentage {
    type: number
    sql: ${TABLE}.ranking_percentage ;;
  }

  dimension: ranking_score_type {
    type: string
    sql: ${TABLE}.ranking_score_type ;;
  }

  dimension: rejected_by_kq {
    type: yesno
    sql: ${TABLE}.rejected_by_kq ;;
  }

  dimension: rereview_confidence_level {
    type: string
    sql: ${TABLE}.rereview_confidence_level ;;
  }

  dimension: rereview_status {
    type: string
    sql: ${TABLE}.rereview_status ;;
  }

  dimension: research_consent_accepted {
    type: yesno
    sql: ${TABLE}.research_consent_accepted ;;
  }

  dimension_group: research_consent_timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.research_consent_timestamp ;;
  }

  dimension: review_status {
    type: string
    sql: ${TABLE}.review_status ;;
  }

  dimension: screen_exit_score {
    type: number
    sql: ${TABLE}.screen_exit_score ;;
  }

  dimension: screening_instant_exit {
    type: yesno
    sql: ${TABLE}.screening_instant_exit ;;
  }

  dimension: screening_invite_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.screening_invite_id ;;
  }

  dimension: screening_last_type {
    type: string
    sql: ${TABLE}.screening_last_type ;;
  }

  dimension: screening_score {
    type: number
    sql: ${TABLE}.screening_score ;;
  }

  dimension: screening_status {
    type: string
    sql: ${TABLE}.screening_status ;;
  }

  dimension: seamless_login {
    type: yesno
    sql: ${TABLE}.seamless_login ;;
  }

  dimension: single_question_retake {
    type: yesno
    sql: ${TABLE}.single_question_retake ;;
  }

  dimension: skip_screening {
    type: yesno
    sql: ${TABLE}.skip_screening ;;
  }

  dimension: terms_and_privacy_policy_accepted {
    type: yesno
    sql: ${TABLE}.terms_and_privacy_policy_accepted ;;
  }

  dimension_group: terms_and_privacy_policy_accepted_timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.terms_and_privacy_policy_accepted_timestamp ;;
  }

  dimension: twitter_profile {
    type: string
    sql: ${TABLE}.twitter_profile ;;
  }

  dimension: unsubscribe_code {
    type: string
    sql: ${TABLE}.unsubscribe_code ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: uploaded_requirements {
    type: yesno
    sql: ${TABLE}.uploaded_requirements ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  dimension: video_interview_last_type {
    type: string
    sql: ${TABLE}.video_interview_last_type ;;
  }

  dimension: video_interview_types {
    type: string
    sql: ${TABLE}.video_interview_types ;;
  }

  dimension: viewed_questions {
    type: string
    sql: ${TABLE}.viewed_questions ;;
  }

  dimension: website {
    type: string
    sql: ${TABLE}.website ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      base_video_name,
      cv_file_name,
      first_name,
      last_name,
      photo_file_name,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      applicant_invites.id,
      job_applications.base_video_name,
      job_applications.id,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      screening_invites.id,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name,
      acc_can_trackers.count,
      api_logs.count,
      api_review_interview_links.count,
      applicant_invite_custom_ids.count,
      application_retakes.count,
      archived_candidates.count,
      candidate_average_scores.count,
      candidate_feedback_scoring_results.count,
      candidate_message_threads.count,
      candidate_record_deadlines.count,
      candidate_record_infos.count,
      candidate_record_ips.count,
      candidate_record_metadata.count,
      candidate_record_status_logs.count,
      candidate_record_tags.count,
      candidate_record_timestamps.count,
      candidate_screening_options.count,
      candidate_screening_questions.count,
      candidate_sessions.count,
      conductors.count,
      data_policy_logs.count,
      did_not_complete_trackers.count,
      documents.count,
      ds_benchmark_scores.count,
      ds_candidate_question_scores.count,
      ds_candidate_scores.count,
      ds_prioritise_candidates.count,
      ds_rereview_candidates.count,
      educations.count,
      interview_share_invites.count,
      job_application_question_versions.count,
      job_applications.count,
      live_meetings.count,
      screening_invites.count,
      screening_retakes.count,
      screening_reviews.count,
      screening_sessions.count,
      tcc_candidate_files.count,
      work_experiences.count
    ]
  }
}
