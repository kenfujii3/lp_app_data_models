view: candidate_question_retakes {
  sql_table_name: willh.candidate_question_retakes ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: answer_attempt_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.answer_attempt_id ;;
  }

  dimension: candidate_question_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.candidate_question_uuid ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: retake_request_id {
    type: number
    sql: ${TABLE}.retake_request_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, answer_attempts.id]
  }
}
