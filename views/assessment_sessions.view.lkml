view: assessment_sessions {
  sql_table_name: willh.assessment_sessions ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: assessment_invite_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.assessment_invite_uuid ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension_group: last_activity_timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_activity_timestamp ;;
  }

  dimension: modular_job_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.modular_job_uuid ;;
  }

  dimension: person_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.person_uuid ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
