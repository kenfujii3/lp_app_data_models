view: integration_transaction_tokens {
  sql_table_name: willh.integration_transaction_tokens ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: action {
    type: string
    sql: ${TABLE}.action ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension_group: expires {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.expires_at ;;
  }

  dimension: integration_transaction_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.integration_transaction_id ;;
  }

  dimension: interview_setup_request_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.interview_setup_request_id ;;
  }

  dimension: token {
    type: string
    sql: ${TABLE}.token ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      integration_transactions.candidate_first_name,
      integration_transactions.candidate_last_name,
      integration_transactions.id,
      integration_transactions.requester_first_name,
      integration_transactions.requester_last_name,
      interview_setup_requests.id,
      interview_setup_requests.requester_first_name,
      interview_setup_requests.requester_last_name
    ]
  }
}
