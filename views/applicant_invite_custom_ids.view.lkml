view: applicant_invite_custom_ids {
  sql_table_name: willh.applicant_invite_custom_ids ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: applicant_invite_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.applicant_invite_id ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: custom_id {
    type: string
    sql: ${TABLE}.custom_id ;;
  }

  dimension: invite_type {
    type: string
    sql: ${TABLE}.invite_type ;;
  }

  dimension: live_assessment_candidate_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_assessment_candidate_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      applicant_invites.id,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      live_assessment_candidates.first_name,
      live_assessment_candidates.id,
      live_assessment_candidates.last_name
    ]
  }
}
