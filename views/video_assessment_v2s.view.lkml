view: video_assessment_v2s {
  sql_table_name: willh.video_assessment_v2s ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: allow_preparation_time {
    type: yesno
    sql: ${TABLE}.allow_preparation_time ;;
  }

  dimension: allow_re_recording {
    type: yesno
    sql: ${TABLE}.allow_re_recording ;;
  }

  dimension: allow_review_criteria {
    type: yesno
    sql: ${TABLE}.allow_review_criteria ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: question_delivery {
    type: string
    sql: ${TABLE}.question_delivery ;;
  }

  dimension: stage_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.stage_uuid ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
