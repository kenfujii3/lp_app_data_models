view: video_question_v2s {
  sql_table_name: willh.video_question_v2s ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: answer_time {
    type: string
    sql: ${TABLE}.answer_time ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: preparation_time {
    type: string
    sql: ${TABLE}.preparation_time ;;
  }

  dimension: question_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.question_uuid ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.uuid ;;
  }

  dimension: video_assessment_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.video_assessment_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
