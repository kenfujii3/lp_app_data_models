view: tcc_candidate_files {
  sql_table_name: willh.tcc_candidate_files ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: candidate_email {
    type: string
    sql: ${TABLE}.candidate_email ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: destination_status {
    type: string
    sql: ${TABLE}.destination_status ;;
  }

  dimension: filename {
    type: string
    sql: ${TABLE}.filename ;;
  }

  dimension: mode {
    type: string
    sql: ${TABLE}.mode ;;
  }

  dimension: original_status {
    type: string
    sql: ${TABLE}.original_status ;;
  }

  dimension: requisition_number {
    type: string
    sql: ${TABLE}.requisition_number ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      filename,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name
    ]
  }
}
