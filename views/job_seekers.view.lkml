view: job_seekers {
  sql_table_name: willh.job_seekers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: custom_fields {
    type: string
    sql: ${TABLE}.custom_fields ;;
  }

  dimension: cv_content_type {
    type: string
    sql: ${TABLE}.cv_content_type ;;
  }

  dimension: cv_file_name {
    type: string
    sql: ${TABLE}.cv_file_name ;;
  }

  dimension: cv_file_size {
    type: number
    sql: ${TABLE}.cv_file_size ;;
  }

  dimension_group: cv_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.cv_updated_at ;;
  }

  dimension: featured {
    type: yesno
    sql: ${TABLE}.featured ;;
  }

  dimension_group: full_time_availability {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.full_time_availability ;;
  }

  dimension: geo_city {
    type: string
    sql: ${TABLE}.geo_city ;;
  }

  dimension: has_cv {
    type: yesno
    sql: ${TABLE}.has_cv ;;
  }

  dimension: has_photo {
    type: yesno
    sql: ${TABLE}.has_photo ;;
  }

  dimension: has_video {
    type: yesno
    sql: ${TABLE}.has_video ;;
  }

  dimension: interest_full_time {
    type: yesno
    sql: ${TABLE}.interest_full_time ;;
  }

  dimension: interest_not_looking {
    type: yesno
    sql: ${TABLE}.interest_not_looking ;;
  }

  dimension: interest_part_time {
    type: yesno
    sql: ${TABLE}.interest_part_time ;;
  }

  dimension: keep_name_private {
    type: yesno
    sql: ${TABLE}.keep_name_private ;;
  }

  dimension_group: last_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_updated ;;
  }

  dimension: latitude {
    type: number
    sql: ${TABLE}.latitude ;;
  }

  dimension: linkedin_profile {
    type: string
    sql: ${TABLE}.linkedin_profile ;;
  }

  dimension: location_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.location_id ;;
  }

  dimension: location_name {
    type: string
    sql: ${TABLE}.location_name ;;
  }

  dimension: longitude {
    type: number
    sql: ${TABLE}.longitude ;;
  }

  dimension: nearest_location_name {
    type: string
    sql: ${TABLE}.nearest_location_name ;;
  }

  dimension: parsed_cv {
    type: string
    sql: ${TABLE}.parsed_cv ;;
  }

  dimension: part_time_schedule_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.part_time_schedule_id ;;
  }

  dimension: part_time_year_round {
    type: yesno
    sql: ${TABLE}.part_time_year_round ;;
  }

  dimension: photo_content_type {
    type: string
    sql: ${TABLE}.photo_content_type ;;
  }

  dimension: photo_file_name {
    type: string
    sql: ${TABLE}.photo_file_name ;;
  }

  dimension: photo_file_size {
    type: number
    sql: ${TABLE}.photo_file_size ;;
  }

  dimension_group: photo_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.photo_updated_at ;;
  }

  dimension_group: posting {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.posting_date ;;
  }

  dimension: profile_description {
    type: string
    sql: ${TABLE}.profile_description ;;
  }

  dimension: profile_score {
    type: number
    sql: ${TABLE}.profile_score ;;
  }

  dimension: profile_status {
    type: string
    sql: ${TABLE}.profile_status ;;
  }

  dimension: profile_title {
    type: string
    sql: ${TABLE}.profile_title ;;
  }

  dimension: projects_and_organizations {
    type: string
    sql: ${TABLE}.projects_and_organizations ;;
  }

  dimension: search_rank_score {
    type: number
    sql: ${TABLE}.search_rank_score ;;
  }

  dimension: search_rank_score_adjust {
    type: number
    sql: ${TABLE}.search_rank_score_adjust ;;
  }

  dimension: twitter_profile {
    type: string
    sql: ${TABLE}.twitter_profile ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  dimension: virtual_job_preference {
    type: yesno
    sql: ${TABLE}.virtual_job_preference ;;
  }

  dimension: visible {
    type: yesno
    sql: ${TABLE}.visible ;;
  }

  dimension: website {
    type: string
    sql: ${TABLE}.website ;;
  }

  dimension: willing_to_relocate {
    type: yesno
    sql: ${TABLE}.willing_to_relocate ;;
  }

  dimension: work_type {
    type: string
    sql: ${TABLE}.work_type ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      cv_file_name,
      location_name,
      nearest_location_name,
      photo_file_name,
      locations.id,
      locations.name,
      part_time_schedules.id,
      part_time_schedules.name,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name,
      educations.count,
      job_applications.count,
      job_seeker_favorite_jobs.count,
      job_seeker_industries.count,
      job_seeker_roles.count,
      job_seeker_schedule_types.count,
      part_time_availabilities.count,
      work_experiences.count
    ]
  }
}
