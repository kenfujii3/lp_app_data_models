view: live_assessments {
  sql_table_name: willh.live_assessments ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: archived {
    type: yesno
    sql: ${TABLE}.archived ;;
  }

  dimension: candidate_time_limit {
    type: number
    sql: ${TABLE}.candidate_time_limit ;;
  }

  dimension: comments {
    type: string
    sql: ${TABLE}.comments ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: creator_id {
    type: number
    sql: ${TABLE}.creator_id ;;
  }

  dimension: duration {
    type: number
    sql: ${TABLE}.duration ;;
  }

  dimension: enable_live_scheduling {
    type: yesno
    sql: ${TABLE}.enable_live_scheduling ;;
  }

  dimension: enable_recording {
    type: yesno
    sql: ${TABLE}.enable_recording ;;
  }

  dimension: is_f2f_interview {
    type: yesno
    sql: ${TABLE}.is_f2f_interview ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: rating_scale {
    type: number
    sql: ${TABLE}.rating_scale ;;
  }

  dimension: reminder_frequency {
    type: number
    sql: ${TABLE}.reminder_frequency ;;
  }

  dimension: rolling_deadline {
    type: number
    sql: ${TABLE}.rolling_deadline ;;
  }

  dimension: slot_availability {
    type: number
    sql: ${TABLE}.slot_availability ;;
  }

  dimension: timezone {
    type: string
    sql: ${TABLE}.timezone ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      criteria_reviews.count,
      data_policy_logs.count,
      live_assessment_locations.count,
      live_assigned_reviewers.count,
      live_invites_csv_uploads.count,
      live_meetings.count
    ]
  }
}
