view: s3_informations {
  sql_table_name: willh.s3_informations ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: bucket {
    type: string
    sql: ${TABLE}.bucket ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: extname {
    type: string
    sql: ${TABLE}.extname ;;
  }

  dimension: file_key {
    type: string
    sql: ${TABLE}.file_key ;;
  }

  dimension: file_size {
    type: number
    sql: ${TABLE}.file_size ;;
  }

  dimension: modular_video_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.modular_video_uuid ;;
  }

  dimension: region {
    type: string
    sql: ${TABLE}.region ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: video_answer_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.video_answer_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [id, extname]
  }
}
