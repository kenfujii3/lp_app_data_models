view: api_review_interview_links {
  sql_table_name: willh.api_review_interview_links ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: applicant_id {
    type: number
    sql: ${TABLE}.applicant_id ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: enable_request_retake {
    type: yesno
    sql: ${TABLE}.enable_request_retake ;;
  }

  dimension: enable_share_interview {
    type: yesno
    sql: ${TABLE}.enable_share_interview ;;
  }

  dimension_group: expiry {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.expiry ;;
  }

  dimension: hide_candidate_name {
    type: yesno
    sql: ${TABLE}.hide_candidate_name ;;
  }

  dimension: hide_comments {
    type: yesno
    sql: ${TABLE}.hide_comments ;;
  }

  dimension: hide_review_criteria {
    type: yesno
    sql: ${TABLE}.hide_review_criteria ;;
  }

  dimension: job_application_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_application_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: scrollable_questions {
    type: yesno
    sql: ${TABLE}.scrollable_questions ;;
  }

  dimension: share_code {
    type: string
    sql: ${TABLE}.share_code ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      hide_candidate_name,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      job_applications.base_video_name,
      job_applications.id,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id
    ]
  }
}
