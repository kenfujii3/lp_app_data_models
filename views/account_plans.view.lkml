view: account_plans {
  sql_table_name: willh.account_plans ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: applicants_per_month {
    type: number
    sql: ${TABLE}.applicants_per_month ;;
  }

  dimension: bill_per_user {
    type: yesno
    sql: ${TABLE}.bill_per_user ;;
  }

  dimension: billing_period_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.billing_period_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: custom_plan {
    type: yesno
    sql: ${TABLE}.custom_plan ;;
  }

  dimension_group: first_job_opened {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.first_job_opened_at ;;
  }

  dimension: max_applicants_per_job {
    type: number
    sql: ${TABLE}.max_applicants_per_job ;;
  }

  dimension: max_customised_content {
    type: number
    sql: ${TABLE}.max_customised_content ;;
  }

  dimension: max_full_branding {
    type: number
    sql: ${TABLE}.max_full_branding ;;
  }

  dimension: max_live_license {
    type: number
    sql: ${TABLE}.max_live_license ;;
  }

  dimension: max_published_jobs {
    type: number
    sql: ${TABLE}.max_published_jobs ;;
  }

  dimension: max_total_applicants {
    type: number
    sql: ${TABLE}.max_total_applicants ;;
  }

  dimension: max_training_sessions {
    type: number
    sql: ${TABLE}.max_training_sessions ;;
  }

  dimension: max_users {
    type: number
    sql: ${TABLE}.max_users ;;
  }

  dimension: plan_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.plan_id ;;
  }

  dimension_group: trial_end {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.trial_end_date ;;
  }

  dimension: trial_period_length {
    type: number
    sql: ${TABLE}.trial_period_length ;;
  }

  dimension: unlimited_interviews {
    type: yesno
    sql: ${TABLE}.unlimited_interviews ;;
  }

  dimension: unlimited_screen {
    type: yesno
    sql: ${TABLE}.unlimited_screen ;;
  }

  dimension: unlimited_share {
    type: yesno
    sql: ${TABLE}.unlimited_share ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: video_retention_period {
    type: number
    sql: ${TABLE}.video_retention_period ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      billing_periods.id,
      billing_periods.name,
      plans.id,
      plans.name
    ]
  }
}
