view: candidate_feedback_scoring_results {
  sql_table_name: willh.candidate_feedback_scoring_results ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: average_competencies {
    type: string
    sql: ${TABLE}.average_competencies ;;
  }

  dimension: average_competency_descriptions {
    type: string
    sql: ${TABLE}.average_competency_descriptions ;;
  }

  dimension: average_title {
    type: string
    sql: ${TABLE}.average_title ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension_group: expiry {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.expiry_date ;;
  }

  dimension: interview_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.interview_id ;;
  }

  dimension: strength_gate {
    type: number
    sql: ${TABLE}.strength_gate ;;
  }

  dimension: strength_title {
    type: string
    sql: ${TABLE}.strength_title ;;
  }

  dimension: strong_competencies {
    type: string
    sql: ${TABLE}.strong_competencies ;;
  }

  dimension: strong_competency_descriptions {
    type: string
    sql: ${TABLE}.strong_competency_descriptions ;;
  }

  dimension: weak_competencies {
    type: string
    sql: ${TABLE}.weak_competencies ;;
  }

  dimension: weak_competency_descriptions {
    type: string
    sql: ${TABLE}.weak_competency_descriptions ;;
  }

  dimension: weak_title {
    type: string
    sql: ${TABLE}.weak_title ;;
  }

  dimension: weakness_gate {
    type: number
    sql: ${TABLE}.weakness_gate ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      interviews.id,
      interviews.redirect_button_name,
      interviews.screening_redirect_button_name
    ]
  }
}
