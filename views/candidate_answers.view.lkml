view: candidate_answers {
  sql_table_name: willh.candidate_answers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: answer_attempt_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.answer_attempt_id ;;
  }

  dimension: answer_type_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.answer_type_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      answer_attempts.id,
      answer_types.id,
      answer_types.name,
      candidate_answer_statuses.count,
      candidate_dual_choice_answers.count,
      candidate_multiple_choice_answers.count,
      candidate_text_answers.count,
      candidate_video_answers.count
    ]
  }
}
