view: job_applications {
  sql_table_name: willh.job_applications ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: application_status {
    type: string
    sql: ${TABLE}.application_status ;;
  }

  dimension_group: application_status_change {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.application_status_change_date ;;
  }

  dimension: base_video_name {
    type: string
    sql: ${TABLE}.base_video_name ;;
  }

  dimension: candidate_message_thread_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_message_thread_id ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension: cover_letter {
    type: string
    sql: ${TABLE}.cover_letter ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: filter_answer {
    type: string
    sql: ${TABLE}.filter_answer ;;
  }

  dimension: has_video_application {
    type: yesno
    sql: ${TABLE}.has_video_application ;;
  }

  dimension: integration_code {
    type: string
    sql: ${TABLE}.integration_code ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: job_seeker_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_seeker_id ;;
  }

  dimension: message_thread_id {
    type: number
    sql: ${TABLE}.message_thread_id ;;
  }

  dimension: review_list_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.review_list_id ;;
  }

  dimension: total_questions {
    type: number
    sql: ${TABLE}.total_questions ;;
  }

  dimension: unlimited_share_code {
    type: string
    sql: ${TABLE}.unlimited_share_code ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      base_video_name,
      candidate_message_threads.id,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      job_seekers.cv_file_name,
      job_seekers.id,
      job_seekers.location_name,
      job_seekers.nearest_location_name,
      job_seekers.photo_file_name,
      review_lists.id,
      review_lists.name,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name,
      api_review_interview_links.count,
      applicant_invites.count,
      application_retakes.count,
      archived_candidates.count,
      candidate_message_threads.count,
      candidate_records.count,
      glacier_archives.count,
      glacier_restores.count,
      integration_reviews.count,
      interview_share_invites.count,
      job_application_comments.count,
      job_application_criteria_scores.count,
      job_application_histories.count,
      job_application_lists.count,
      job_application_ratings.count,
      job_application_reviews.count,
      job_application_shares.count,
      job_application_total_scores.count,
      job_application_view_statuses.count,
      video_playback_logs.count,
      videos.count
    ]
  }
}
