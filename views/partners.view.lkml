view: partners {
  sql_table_name: willh.partners ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: api_key {
    type: string
    sql: ${TABLE}.api_key ;;
  }

  dimension: assessment_title {
    type: string
    sql: ${TABLE}.assessment_title ;;
  }

  dimension: authentication_method {
    type: string
    sql: ${TABLE}.authentication_method ;;
  }

  dimension: candidate_presentation_settings {
    type: string
    sql: ${TABLE}.candidate_presentation_settings ;;
  }

  dimension: configuration_id {
    type: string
    sql: ${TABLE}.configuration_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: data {
    type: string
    sql: ${TABLE}.data ;;
  }

  dimension: external_account_id {
    type: string
    sql: ${TABLE}.external_account_id ;;
  }

  dimension: height {
    type: string
    sql: ${TABLE}.height ;;
  }

  dimension: invite_candidate_endpoint {
    type: string
    sql: ${TABLE}.invite_candidate_endpoint ;;
  }

  dimension: list_assessments_endpoint {
    type: string
    sql: ${TABLE}.list_assessments_endpoint ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: password {
    type: string
    sql: ${TABLE}.password ;;
  }

  dimension: polling_endpoint {
    type: string
    sql: ${TABLE}.polling_endpoint ;;
  }

  dimension: redirect_method {
    type: string
    sql: ${TABLE}.redirect_method ;;
  }

  dimension: reset_candidate_endpoint {
    type: string
    sql: ${TABLE}.reset_candidate_endpoint ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }

  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }

  dimension: width {
    type: string
    sql: ${TABLE}.width ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      name,
      username,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      partner_assessments.count
    ]
  }
}
