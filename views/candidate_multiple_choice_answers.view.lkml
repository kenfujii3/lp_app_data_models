view: candidate_multiple_choice_answers {
  sql_table_name: willh.candidate_multiple_choice_answers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: candidate_answer_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_answer_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: option_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.option_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, candidate_answers.id, options.id]
  }
}
