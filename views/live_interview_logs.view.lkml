view: live_interview_logs {
  sql_table_name: willh.live_interview_logs ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: entry {
    type: string
    sql: ${TABLE}.entry ;;
  }

  dimension: ip {
    type: string
    sql: ${TABLE}.ip ;;
  }

  dimension: live_candidate_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_candidate_id ;;
  }

  dimension: live_employer_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_employer_id ;;
  }

  dimension: live_interview_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.live_interview_id ;;
  }

  dimension_group: timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.timestamp ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_agent {
    type: string
    sql: ${TABLE}.user_agent ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      live_candidates.first_name,
      live_candidates.id,
      live_candidates.last_name,
      live_employers.first_name,
      live_employers.id,
      live_employers.last_name,
      live_interviews.id
    ]
  }
}
