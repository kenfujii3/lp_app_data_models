view: screening_library_questions {
  sql_table_name: willh.screening_library_questions ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: answer_type {
    type: string
    sql: ${TABLE}.answer_type ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: creator_id {
    type: number
    sql: ${TABLE}.creator_id ;;
  }

  dimension: delete_flag {
    type: yesno
    sql: ${TABLE}.delete_flag ;;
  }

  dimension: multiple_selection {
    type: yesno
    sql: ${TABLE}.multiple_selection ;;
  }

  dimension: question {
    type: string
    sql: ${TABLE}.question ;;
  }

  dimension: screening_library_category_id {
    type: number
    sql: ${TABLE}.screening_library_category_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      candidate_screening_questions.count,
      screening_library_options.count,
      screening_questions.count,
      screening_session_questions.count
    ]
  }
}
