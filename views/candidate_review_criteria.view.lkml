view: candidate_review_criteria {
  sql_table_name: willh.candidate_review_criteria ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: assessment_session_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.assessment_session_uuid ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: review_criterion_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.review_criterion_uuid ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  dimension: value {
    type: string
    sql: ${TABLE}.value ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name,
      question_candidate_review_criteria.count,
      stage_candidate_review_criteria.count
    ]
  }
}
