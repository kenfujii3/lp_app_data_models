view: admin_role_permissions {
  sql_table_name: willh.admin_role_permissions ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: admin_role_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.admin_role_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: has_access {
    type: string
    sql: ${TABLE}.has_access ;;
  }

  dimension: section {
    type: string
    sql: ${TABLE}.section ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, admin_roles.id, admin_roles.name]
  }
}
