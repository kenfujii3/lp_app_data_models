view: interview_requirements {
  sql_table_name: willh.interview_requirements ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: awards {
    type: string
    sql: ${TABLE}.awards ;;
  }

  dimension: blog {
    type: string
    sql: ${TABLE}.blog ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: custom_fields {
    type: string
    sql: ${TABLE}.custom_fields ;;
  }

  dimension: cv {
    type: string
    sql: ${TABLE}.cv ;;
  }

  dimension: docs {
    type: string
    sql: ${TABLE}.docs ;;
  }

  dimension: education {
    type: string
    sql: ${TABLE}.education ;;
  }

  dimension: filter_questions {
    type: string
    sql: ${TABLE}.filter_questions ;;
  }

  dimension: interested_industries {
    type: string
    sql: ${TABLE}.interested_industries ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: linkedin {
    type: string
    sql: ${TABLE}.linkedin ;;
  }

  dimension: open_to_working_remotely {
    type: string
    sql: ${TABLE}.open_to_working_remotely ;;
  }

  dimension: personal_description {
    type: string
    sql: ${TABLE}.personal_description ;;
  }

  dimension: personal_tag_line {
    type: string
    sql: ${TABLE}.personal_tag_line ;;
  }

  dimension: photo {
    type: string
    sql: ${TABLE}.photo ;;
  }

  dimension: preferred_city {
    type: string
    sql: ${TABLE}.preferred_city ;;
  }

  dimension: roles {
    type: string
    sql: ${TABLE}.roles ;;
  }

  dimension: schedule {
    type: string
    sql: ${TABLE}.schedule ;;
  }

  dimension: strict_filter {
    type: yesno
    sql: ${TABLE}.strict_filter ;;
  }

  dimension: twitter {
    type: string
    sql: ${TABLE}.twitter ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: willing_to_relocate {
    type: string
    sql: ${TABLE}.willing_to_relocate ;;
  }

  dimension: work_experience {
    type: string
    sql: ${TABLE}.work_experience ;;
  }

  measure: count {
    type: count
    drill_fields: [id, jobs.country_name, jobs.industry_name, jobs.location_name, jobs.root_job_id]
  }
}
