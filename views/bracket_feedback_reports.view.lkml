view: bracket_feedback_reports {
  sql_table_name: willh.bracket_feedback_reports ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: document_content_type {
    type: string
    sql: ${TABLE}.document_content_type ;;
  }

  dimension: document_file_name {
    type: string
    sql: ${TABLE}.document_file_name ;;
  }

  dimension: document_file_size {
    type: number
    sql: ${TABLE}.document_file_size ;;
  }

  dimension_group: document_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.document_updated_at ;;
  }

  dimension: score_bracket_id {
    type: number
    sql: ${TABLE}.score_bracket_id ;;
  }

  dimension: uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [id, document_file_name]
  }
}
