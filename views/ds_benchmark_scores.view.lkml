view: ds_benchmark_scores {
  sql_table_name: willh.ds_benchmark_scores ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: criteria_review_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.criteria_review_id ;;
  }

  dimension: ds_review_group_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_review_group_id ;;
  }

  dimension: ds_reviewer_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_reviewer_id ;;
  }

  dimension: score {
    type: string
    sql: ${TABLE}.score ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      criteria_reviews.id,
      ds_review_groups.id,
      ds_review_groups.name,
      ds_reviewers.id
    ]
  }
}
