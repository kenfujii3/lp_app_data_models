view: review_lists {
  sql_table_name: willh.review_lists ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: cached_slug {
    type: string
    sql: ${TABLE}.cached_slug ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: delete_flag {
    type: yesno
    sql: ${TABLE}.delete_flag ;;
  }

  dimension: hide_video {
    type: yesno
    sql: ${TABLE}.hide_video ;;
  }

  dimension: internal {
    type: yesno
    sql: ${TABLE}.internal ;;
  }

  dimension: internal_type {
    type: string
    sql: ${TABLE}.internal_type ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: reviewer_notification {
    type: yesno
    sql: ${TABLE}.reviewer_notification ;;
  }

  dimension: shared {
    type: yesno
    sql: ${TABLE}.shared ;;
  }

  dimension: show_candidate_details {
    type: yesno
    sql: ${TABLE}.show_candidate_details ;;
  }

  dimension: show_custom_cv {
    type: yesno
    sql: ${TABLE}.show_custom_cv ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      name,
      job_application_lists.count,
      job_application_reviews.count,
      job_applications.count,
      job_review_lists.count,
      review_list_shares.count
    ]
  }
}
