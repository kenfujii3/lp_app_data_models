view: documents {
  sql_table_name: willh.documents ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: attachment_content_type {
    type: string
    sql: ${TABLE}.attachment_content_type ;;
  }

  dimension: attachment_file_name {
    type: string
    sql: ${TABLE}.attachment_file_name ;;
  }

  dimension: attachment_file_size {
    type: number
    sql: ${TABLE}.attachment_file_size ;;
  }

  dimension_group: attachment_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.attachment_updated_at ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      attachment_file_name,
      name,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name
    ]
  }
}
