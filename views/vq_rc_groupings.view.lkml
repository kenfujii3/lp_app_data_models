view: vq_rc_groupings {
  sql_table_name: willh.vq_rc_groupings ;;

  dimension: review_criterion_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.review_criterion_uuid ;;
  }

  dimension: video_question_uuid {
    type: number
    value_format_name: id
    sql: ${TABLE}.video_question_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
