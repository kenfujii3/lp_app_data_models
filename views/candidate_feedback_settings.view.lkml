view: candidate_feedback_settings {
  sql_table_name: willh.candidate_feedback_settings ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: competency_text {
    type: string
    sql: ${TABLE}.competency_text ;;
  }

  dimension: competency_title {
    type: string
    sql: ${TABLE}.competency_title ;;
  }

  dimension: include_average {
    type: yesno
    sql: ${TABLE}.include_average ;;
  }

  dimension: introduction {
    type: string
    sql: ${TABLE}.introduction ;;
  }

  dimension: job_candidate_feedback_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_candidate_feedback_id ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }

  measure: count {
    type: count
    drill_fields: [id, job_candidate_feedbacks.id, feedback_category_frameworks.count, feedback_competencies.count]
  }
}
