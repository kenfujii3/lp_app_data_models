view: job_application_question_versions {
  sql_table_name: willh.job_application_question_versions ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: answer_time {
    type: number
    sql: ${TABLE}.answer_time ;;
  }

  dimension_group: application {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.application_time ;;
  }

  dimension: bandwidth {
    type: string
    sql: ${TABLE}.bandwidth ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: ip_address {
    type: string
    sql: ${TABLE}.ip_address ;;
  }

  dimension: ip_location {
    type: string
    sql: ${TABLE}.ip_location ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: number_of_records {
    type: number
    sql: ${TABLE}.number_of_records ;;
  }

  dimension: platform {
    type: string
    sql: ${TABLE}.platform ;;
  }

  dimension: preparation_time {
    type: number
    sql: ${TABLE}.preparation_time ;;
  }

  dimension: question_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.question_id ;;
  }

  dimension: session_id {
    type: string
    sql: ${TABLE}.session_id ;;
  }

  dimension: speech_filename {
    type: string
    sql: ${TABLE}.speech_filename ;;
  }

  dimension: speech_text {
    type: string
    sql: ${TABLE}.speech_text ;;
  }

  dimension: time_before_record {
    type: number
    sql: ${TABLE}.time_before_record ;;
  }

  dimension: unreviewable {
    type: yesno
    sql: ${TABLE}.unreviewable ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  dimension: version_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.version_id ;;
  }

  dimension: video_length {
    type: number
    sql: ${TABLE}.video_length ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      speech_filename,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      questions.base_video_name,
      questions.id,
      users.demo_video_name,
      users.first_name,
      users.id,
      users.integration_username,
      users.last_name,
      versions.id,
      ds_review_applicants.count,
      videos.count
    ]
  }
}
