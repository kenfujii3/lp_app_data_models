view: screening_sessions {
  sql_table_name: willh.screening_sessions ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: aasm_state {
    type: string
    sql: ${TABLE}.aasm_state ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: apply_code {
    type: string
    sql: ${TABLE}.apply_code ;;
  }

  dimension: assessment_id {
    type: number
    sql: ${TABLE}.assessment_id ;;
  }

  dimension: candidate_id {
    type: number
    sql: ${TABLE}.candidate_id ;;
  }

  dimension: candidate_record_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.candidate_record_id ;;
  }

  dimension_group: completed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.completed_at ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: elapsed_time {
    type: number
    sql: ${TABLE}.elapsed_time ;;
  }

  dimension_group: expiration {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.expiration_date ;;
  }

  dimension: extend_time {
    type: number
    sql: ${TABLE}.extend_time ;;
  }

  dimension: external_requisition_id {
    type: string
    sql: ${TABLE}.external_requisition_id ;;
  }

  dimension: integration_transaction_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.integration_transaction_id ;;
  }

  dimension: result {
    type: string
    sql: ${TABLE}.result ;;
  }

  dimension: score {
    type: number
    sql: ${TABLE}.score ;;
  }

  dimension_group: started {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.started_at ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: url {
    type: string
    sql: ${TABLE}.url ;;
  }

  dimension: uuid {
    type: string
    sql: ${TABLE}.uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      candidate_records.base_video_name,
      candidate_records.cv_file_name,
      candidate_records.first_name,
      candidate_records.id,
      candidate_records.last_name,
      candidate_records.photo_file_name,
      integration_transactions.candidate_first_name,
      integration_transactions.candidate_last_name,
      integration_transactions.id,
      integration_transactions.requester_first_name,
      integration_transactions.requester_last_name,
      candidate_sessions.count,
      screening_session_options.count,
      screening_session_questions.count
    ]
  }
}
