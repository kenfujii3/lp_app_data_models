view: ds_question_answers {
  sql_table_name: willh.ds_question_answers ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: ds_job_info_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_job_info_id ;;
  }

  dimension: ds_question_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_question_id ;;
  }

  dimension: ds_question_option_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_question_option_id ;;
  }

  dimension: ds_reviewer_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ds_reviewer_id ;;
  }

  dimension: other {
    type: string
    sql: ${TABLE}.other ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [id, ds_job_infos.id, ds_questions.id, ds_question_options.id, ds_reviewers.id]
  }
}
