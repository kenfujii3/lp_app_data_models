view: screening_feedback_frameworks {
  sql_table_name: willh.screening_feedback_frameworks ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: competencies_intro {
    type: string
    sql: ${TABLE}.competencies_intro ;;
  }

  dimension: competencies_title {
    type: string
    sql: ${TABLE}.competencies_title ;;
  }

  dimension: feedback_enabled_screening {
    type: yesno
    sql: ${TABLE}.feedback_enabled_screening ;;
  }

  dimension: intro {
    type: string
    sql: ${TABLE}.intro ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: strength_qty {
    type: number
    sql: ${TABLE}.strength_qty ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }

  dimension: weakness_qty {
    type: number
    sql: ${TABLE}.weakness_qty ;;
  }

  measure: count {
    type: count
    drill_fields: [id, jobs.country_name, jobs.industry_name, jobs.location_name, jobs.root_job_id]
  }
}
