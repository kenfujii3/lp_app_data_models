view: integration_transactions {
  sql_table_name: willh.integration_transactions ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: account_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_id ;;
  }

  dimension: account_integration_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.account_integration_id ;;
  }

  dimension: action {
    type: string
    sql: ${TABLE}.action ;;
  }

  dimension: callback_attempts {
    type: number
    sql: ${TABLE}.callback_attempts ;;
  }

  dimension: callback_url {
    type: string
    sql: ${TABLE}.callback_url ;;
  }

  dimension: candidate_email {
    type: string
    sql: ${TABLE}.candidate_email ;;
  }

  dimension: candidate_first_name {
    type: string
    sql: ${TABLE}.candidate_first_name ;;
  }

  dimension: candidate_id {
    type: string
    sql: ${TABLE}.candidate_id ;;
  }

  dimension: candidate_last_name {
    type: string
    sql: ${TABLE}.candidate_last_name ;;
  }

  dimension: candidate_mobile {
    type: string
    sql: ${TABLE}.candidate_mobile ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: external_transaction_id {
    type: string
    sql: ${TABLE}.external_transaction_id ;;
  }

  dimension: job_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.job_id ;;
  }

  dimension: package_id {
    type: string
    # hidden: yes
    sql: ${TABLE}.package_id ;;
  }

  dimension: package_title {
    type: string
    sql: ${TABLE}.package_title ;;
  }

  dimension: processed {
    type: yesno
    sql: ${TABLE}.processed ;;
  }

  dimension: requester_email {
    type: string
    sql: ${TABLE}.requester_email ;;
  }

  dimension: requester_first_name {
    type: string
    sql: ${TABLE}.requester_first_name ;;
  }

  dimension: requester_last_name {
    type: string
    sql: ${TABLE}.requester_last_name ;;
  }

  dimension: requisition_id {
    type: string
    sql: ${TABLE}.requisition_id ;;
  }

  dimension: requisition_title {
    type: string
    sql: ${TABLE}.requisition_title ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: template_id {
    type: string
    sql: ${TABLE}.template_id ;;
  }

  dimension: template_title {
    type: string
    sql: ${TABLE}.template_title ;;
  }

  dimension: transaction_id {
    type: string
    sql: ${TABLE}.transaction_id ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      candidate_first_name,
      candidate_last_name,
      requester_first_name,
      requester_last_name,
      accounts.maybe_field_name,
      accounts.name,
      accounts.provisioner_account_id,
      accounts.subdomain_name,
      account_integrations.endpoint_username,
      account_integrations.id,
      account_integrations.name,
      account_integrations.username,
      jobs.country_name,
      jobs.industry_name,
      jobs.location_name,
      jobs.root_job_id,
      packages.id,
      packages.name,
      integration_transaction_fields.count,
      integration_transaction_tokens.count,
      job_integration_mapping_notifications.count,
      screening_sessions.count
    ]
  }
}
